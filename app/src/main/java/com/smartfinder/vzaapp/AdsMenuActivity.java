package com.smartfinder.vzaapp;

import control.MenuView.MenuViewAdapter;
import control.SwipeBack.app.SwipeBackActivity;
import util.BroadcastUtil;

public abstract class AdsMenuActivity extends SwipeBackActivity {
    public MenuViewAdapter menuViewAdapter;
    public BroadcastUtil broadcastUtil;
}
