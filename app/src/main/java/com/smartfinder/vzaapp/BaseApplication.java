package com.smartfinder.vzaapp;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.content.IntentCompat;
import android.view.Display;
import android.view.WindowManager;

//import java.beans.PropertyChangeSupport;
import java.text.SimpleDateFormat;
//import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
//import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

//import control.Dialog.ChooseDialog;
//import control.Dialog.DeferDialog;
import entity.BaseEntity;
//import entity.Host.Cart.MenuOrderData;
import entity.Host.Cart.MenuOrderEntity;

public class BaseApplication extends Application {
    public static class GlobalVariable{
        private static HashMap<Class,Object> globalVariable = new HashMap<>();
        public static <T extends BaseEntity> T get(Context context,Class<T> entity){
            if(globalVariable.containsKey(entity)){
                return (T)globalVariable.get(entity);
            }else{
                //try {
                    /*Intent intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
                    intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                    return entity.newInstance();*/
                    return null;
                /*} catch (InstantiationException | IllegalAccessException e) {
                    //throw new NullPointerException();
                    return  null;
                }*/
            }
        }
        public static <T extends BaseEntity> void set(T entity){
            globalVariable.put(((Object)entity).getClass(),entity);
        }
    };

    public static class CartStateVariable{
        private static List<MenuOrderEntity> menuOrder = new CopyOnWriteArrayList<>();
        private static OnChange changeListener;
        public interface OnChange{  public void OnChange(Integer length); }
        public static void add(MenuOrderEntity ORDER){
            menuOrder.add(ORDER);
            changeListener.OnChange(length());
        }
        public static List<MenuOrderEntity> getAll(){
         return menuOrder;
        }
        public static void clearAll(){
            menuOrder.clear();
            if(changeListener != null) {
                changeListener.OnChange(length());
            }
        };
        public static void clear(Integer position){
            menuOrder.remove(menuOrder.get(position));
            changeListener.OnChange(length());
        }
        public static void clear(MenuOrderEntity entity){
            menuOrder.remove(entity);
            changeListener.OnChange(length());
        }
        public static Integer length(){
            Integer length = 0;
            for(MenuOrderEntity orderData : menuOrder){
               length = length + orderData.ORDER_QRY;
            }
            return length;
        };
        public static Double getAllPrice(){
            Double allPrice = 0.00;
            for(MenuOrderEntity menuOrderEntity : menuOrder){
                Double unitPrice = menuOrderEntity.getUnitPrice();
                Double addPrice = menuOrderEntity.getAdditionPrice();
                if(menuOrderEntity.isMenuSet) {
                    allPrice = allPrice + ((unitPrice+addPrice) * menuOrderEntity.ORDER_QRY) + menuOrderEntity.ORDER_PRICE;
                }else{
                    allPrice = allPrice + ((unitPrice+addPrice) * menuOrderEntity.ORDER_QRY);
                }
            }
            //-- Add
            return allPrice;
        }
        public static void setOnChangeListener(OnChange event){
            changeListener = event;
        };
    }
    public static class LoginRecordVariable{
        public static void setVariable(Context context,String username,String password){
            SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.LOGIN_RECORD),Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(context.getString(R.string.LOGIN_RECORD_USERNAME),username);
            editor.putString(context.getString(R.string.LOGIN_RECORD_PASSWORD), password);
            editor.commit();
        }
        public static String getUsername(Context context){
            SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.LOGIN_RECORD),Context.MODE_PRIVATE);
            return preferences.getString(context.getString(R.string.LOGIN_RECORD_USERNAME),"");
        };
        public static String getPassword(Context context){
            SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.LOGIN_RECORD),Context.MODE_PRIVATE);
            return preferences.getString(context.getString(R.string.LOGIN_RECORD_PASSWORD),"");
        };
    };
    public static class RequestCheckVariable {
        private static boolean isCheck = false;
        public static void set(boolean isCheck){
            RequestCheckVariable.isCheck = isCheck;
        }
        public static boolean get(){
            return isCheck;
        }
    };
    public static class MyDocRecordVariable{
        private static String _status = "";
        public static void setVariable(Context context, String status){
            _status = status;
            /*SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.MYDOC_RECORD),Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(context.getString(R.string.MYDOC_RECORD_STATUS),status);
            editor.commit();*/
        }
        public static String getStatus(Context context){
            //SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.MYDOC_RECORD),Context.MODE_PRIVATE);
            //return preferences.getString(context.getString(R.string.MYDOC_RECORD_STATUS),"");
            return _status;
        };
    };
    public static class MyLikeRecordVariable{
        private static String _status = "";
        public static void setVariable(Context context, String status){
            _status = status;
        }
        public static String getStatus(Context context){
            //SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.MYDOC_RECORD),Context.MODE_PRIVATE);
            //return preferences.getString(context.getString(R.string.MYDOC_RECORD_STATUS),"");
            return _status;
        };
    };
    public static class MyFavRecordVariable{
        private static String _status = "";
        public static void setVariable(Context context, String status){
            _status = status;
        }
        public static String getStatus(Context context){
            return _status;
        };
    };
    public static class NotificationRecordVariable{
        public static String _key = "";
        public static void setVariable(Context context, String key){
            _key = key;
        }
        public static void setVariable(Context context, int totalRecord, int lastRecord){
            SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.NOTIFICATION_RECORD)+ "_" + _key, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(context.getString(R.string.NOTIFICATION_RECORD_TOTALRECORDS)+ "_" + _key, totalRecord);
            editor.putInt(context.getString(R.string.NOTIFICATION_RECORD_LASTRECORDS)+ "_" + _key, lastRecord);
            editor.commit();
        }
        public static int getTotalRecord(Context context){
            SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.NOTIFICATION_RECORD)+ "_" + _key, Context.MODE_PRIVATE);
            return preferences.getInt(context.getString(R.string.NOTIFICATION_RECORD_TOTALRECORDS)+ "_" + _key,0);
        };
        public static int getLastRecord(Context context){
            SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.NOTIFICATION_RECORD)+ "_" + _key, Context.MODE_PRIVATE);
            return preferences.getInt(context.getString(R.string.NOTIFICATION_RECORD_LASTRECORDS)+ "_" + _key,0);
        };
    };
    public static class ConfigRecordVariable{
        public static void setVariable(Context context, String url){
            SharedPreferences preferences = context.getSharedPreferences("SITE", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("SITE", url);
            editor.commit();
        }
        public static String getVariable(Context context){
            String url = "";
            SharedPreferences preferences = context.getSharedPreferences("SITE", Context.MODE_PRIVATE);
            url = preferences.getString("SITE","");
            /*if(url.length() == 0){
                url = context.getResources().getString(R.string.MAIN_SITE);
            }*/
            return url;
        };
    };
    public static class DisplayVariable {
        public static int getWidth(Context mContext){
            int width=0;
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            if(Build.VERSION.SDK_INT > 12){
                Point size = new Point();
                display.getSize(size);
                width = size.x;
            }
            else{
                width = display.getWidth();  // Deprecated
            }
            return width;
        }
        public static int getHeight(Context mContext){
            int height=0;
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            if(Build.VERSION.SDK_INT > 12){
                Point size = new Point();
                display.getSize(size);
                height = size.y;
            }
            else{
                height = display.getHeight();  // Deprecated
            }
            return height;
        }
    };

    public static class DateTimeVariable {
        private static Calendar cal = Calendar.getInstance();
        public static String getTimeHHMMS(){
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            return sdf.format(cal.getTime());
        }
    }

}
