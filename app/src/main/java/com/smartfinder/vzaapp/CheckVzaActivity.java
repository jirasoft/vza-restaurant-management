package com.smartfinder.vzaapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import java.util.ArrayList;
import java.util.List;
import control.Dialog.ChooseDialog;
import emuntype.MainOperate;
import emuntype.SiteOperate;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.Master.MasterEntity;
import util.ServiceUtil;

public class CheckVzaActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkvza);

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_onClick);
        findViewById(R.id.btnSubmit).setOnClickListener(btnSubmit_onClick);
        ((CheckBox)findViewById(R.id.chkAccept)).setOnCheckedChangeListener(chkAccept_onChange);
        ((EditText)findViewById(R.id.txtCode)).setOnEditorActionListener(txtCode_onEditorAction);
    }

    public Button.OnClickListener btnClose_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public Button.OnClickListener btnSubmit_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(((EditText)findViewById(R.id.txtCode)).getText().length() > 0
                    && ((CheckBox)findViewById(R.id.chkAccept)).isChecked()){
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("HOTELCODEPara",((EditText) findViewById(R.id.txtCode)).getText().toString()));
                ServiceUtil hotelInfoService = new ServiceUtil(CheckVzaActivity.this, MainOperate.HotelInformationbyCodeJSON,param, HotelInfoEntity.class);
                hotelInfoService.setFinishedListener(hotelInfoService_OnFinished);
                hotelInfoService.execute();
            }
        }
    };

    public ServiceUtil.OnFinished hotelInfoService_OnFinished = new ServiceUtil.OnFinished() {
        @Override
        public void OnFinished(Object results) {
            HotelInfoEntity hotelInfoEntity = (HotelInfoEntity) results;
            if(hotelInfoEntity.DATA.HOTEL_ID == 0){
                ChooseDialog dialog = ChooseDialog.newInstance(CheckVzaActivity.this,hotelInfoEntity.DATA.ErrorMsg, false);
                dialog.show(getFragmentManager(), "tag");
                return;
            }else{
                //Keep hotelInfo To AppVariable
                BaseApplication.GlobalVariable.set(hotelInfoEntity);
                UserInfoEntity userInfo = BaseApplication.GlobalVariable.get(getBaseContext(),UserInfoEntity.class);
                HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getBaseContext(),HotelInfoEntity.class);

                //Init Service
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("GUEST_TYPEPara", userInfo.Data.GUEST_TYPE.toString()));
                param.add(new BasicNameValuePair("STAFF_CODEPara", userInfo.Data.STAFF_CODE.toString()));

                String site = hotelInfo.DATA.HOTEL_URL;
                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                ServiceUtil masterService = new ServiceUtil(CheckVzaActivity.this,site,keycode, SiteOperate.VZAgetMasterDataJSON,param, MasterEntity.class);
                masterService.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        //Keep masterData To AppVariable
                        BaseApplication.GlobalVariable.set((MasterEntity)results);

                        //New Intent With Clear
                        Intent intent = new Intent(CheckVzaActivity.this,MasterVzaActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
                masterService.execute();
            }
        }
    };

    public EditText.OnEditorActionListener txtCode_onEditorAction = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(((CheckBox)CheckVzaActivity.this.findViewById(R.id.chkAccept)).isChecked()){
                CheckVzaActivity.this.findViewById(R.id.btnSubmit).performClick();
                if(((EditText)findViewById(R.id.txtCode)).getText().length() > 0) {
                    return false;
                }else{
                    return true;
                }
            }else{
                return true;
            }

        }
    };
    public CheckBox.OnCheckedChangeListener chkAccept_onChange = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked){
                //CheckVzaActivity.this.findViewById(R.id.btnSubmit).setVisibility(View.VISIBLE);
                findViewById(R.id.txtCode).requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(findViewById(R.id.txtCode), InputMethodManager.SHOW_IMPLICIT);
            }else{
                //CheckVzaActivity.this.findViewById(R.id.btnSubmit).setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        }
    };

}
