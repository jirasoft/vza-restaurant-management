package com.smartfinder.vzaapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ConfigActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        //Init Event
        findViewById(R.id.btnSave).setOnClickListener(btnSave_onClick);
        findViewById(R.id.btnCancel).setOnClickListener(btnCancel_onClick);
        findViewById(R.id.btnClose).setOnClickListener(btnClose_onClick);

        //Receive Data
        if(BaseApplication.ConfigRecordVariable.getVariable(getBaseContext()).length()>0)
            ((EditText) findViewById(R.id.txtUrl)).setText(BaseApplication.ConfigRecordVariable.getVariable(getBaseContext()));

    }

    private Button.OnClickListener btnSave_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Init Control
            String txtUrl = ((EditText)findViewById(R.id.txtUrl)).getText().toString();

            //Save Url
            BaseApplication.ConfigRecordVariable.setVariable(getBaseContext(),txtUrl);

            finish();

        }
    };

    private Button.OnClickListener btnCancel_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public Button.OnClickListener btnClose_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
