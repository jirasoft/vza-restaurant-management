package com.smartfinder.vzaapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import emuntype.MainOperate;
import entity.BaseEntity;
import util.ServiceUtil;

public class ForgotActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        //Init Extra
        ((EditText)findViewById(R.id.txtEmail)).setText(getIntent().getStringExtra("email"));

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);
        findViewById(R.id.btnSubmit).setOnClickListener(btnSubmit_OnClick);
    }

    private Button.OnClickListener btnSubmit_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String txtEmail = ((EditText)findViewById(R.id.txtEmail)).getText().toString();
            if(!Patterns.EMAIL_ADDRESS.matcher(txtEmail).matches()){
                ChooseDialog dialog = ChooseDialog.newInstance(ForgotActivity.this,getResources().getString(R.string.alert_email_invalid),false);
                dialog.show(getFragmentManager(),"tag");
            }else{
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("EMAILPara", txtEmail));
                ServiceUtil serviceUtil = new ServiceUtil(ForgotActivity.this, MainOperate.UserInfoforgotpasswordJSON,param, BaseEntity.class);
                serviceUtil.setFailedListener(new ServiceUtil.OnFailed() {
                    @Override
                    public void OnFailed() {
                        ForgotActivity.this.finish();
                    }
                });
                serviceUtil.execute();

                //Clear Text
                ((EditText)findViewById(R.id.txtEmail)).setText("");
            }
        }
    };

    private Button.OnClickListener btnClose_OnClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            ForgotActivity.this.finish();
        }
    };

}
