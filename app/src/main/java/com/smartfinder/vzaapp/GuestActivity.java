package com.smartfinder.vzaapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.Menu.GroupFragment;
import com.smartfinder.vzaapp.Menu.MainFragment;
import com.smartfinder.vzaapp.SideBar.MyDocActivity;
import com.smartfinder.vzaapp.SideBar.MyCartActivity;
import com.smartfinder.vzaapp.SideBar.MyFavActivity;
import com.smartfinder.vzaapp.SideBar.MyLikeActivity;
import com.smartfinder.vzaapp.SideBar.MyNotiActivity;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.ExpandableList.MyDocAdapter;
import control.MenuList.LikeUnlikeAdapter;
import control.MenuList.MenuList;
import control.MenuView.MenuView;
import control.MenuView.MenuViewAdapter;
import emuntype.MainOperate;
import emuntype.SiteOperate;
import entity.BaseEntity;
import entity.Host.Notification.NotificationData;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.QRCodeInfo.QRCodeInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.BillOrder.BillOrderData;
import entity.Service.BillOrder.BillOrderDataAddition;
import entity.Service.BillOrder.BillOrderEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.CheckLogin.CheckLoginEntity;
import entity.Service.Favorite.FavoriteEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.VZACode.VZACodeEntity;
import entity.Service.VZAOpen.VZAOpenEntity;
import util.BroadcastUtil;
import util.ServiceUtil;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class GuestActivity extends AdsMenuActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        //Custom Anim
        overridePendingTransition(R.anim.slidein, 0);

        //Init Fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        MenuView menuView = (MenuView) findViewById(R.id.frmFragment);
        menuViewAdapter = new MenuViewAdapter(fragmentManager);
        menuViewAdapter.addItem(menuView, MainFragment.newInstance());
        menuView.setAdapter(menuViewAdapter);

        //Init Event
        findViewById(R.id.btnLocation).setOnClickListener(btnLocation_OnClick);
        findViewById(R.id.btnBill).setOnClickListener(btnBill_OnClick);
        findViewById(R.id.btnLike).setOnClickListener(btnLike_OnClick);
        findViewById(R.id.btnFav).setOnClickListener(btnFav_OnClick);
        menuView.setOnPageChangeListener(menuView_OnPageChange);

        //Get Entity
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getBaseContext(), MasterEntity.class);
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getBaseContext(), HotelInfoEntity.class);
        CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(getBaseContext(), CheckLocationEntity.class);
        VZACodeEntity vzaEntity = BaseApplication.GlobalVariable.get(getBaseContext(), VZACodeEntity.class);
        //QRCodeInfoEntity qrEntity = BaseApplication.GlobalVariable.get(getBaseContext(), QRCodeInfoEntity.class);
        Integer LOCATION_ID = locationEntity.LOCATION_ID; //Integer.parseInt(qrEntity.data.Location_ID); //locationEntity.LOCATION_ID;
        String TABLE_NO = locationEntity.TABLE_NO; //qrEntity.data.Table_No; //locationEntity.TABLE_NO;
        String VZACODE = vzaEntity.VZACODE == "" ? locationEntity.VZACODE : vzaEntity.VZACODE;
        /*if(LOCATION_ID.equals(0)){
            LOCATION_ID = Integer.parseInt(qrEntity.data.Location_ID);
            TABLE_NO = qrEntity.data.Table_No;
        }*/

        //String SystemID = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(LOCATION_ID).SYSTEMID.toString();
        String SystemID = masterData.FindByLocationID(LOCATION_ID).SYSTEMID.toString();
        String hotelID = hotelInfo.DATA.HOTEL_ID.toString();
        String hotelKeyCode = hotelInfo.DATA.HOTEL_KEYCODE;
        String site = hotelInfo.DATA.HOTEL_URL;

        //Init Top Bar
        /*((TextView) findViewById(R.id.txtHeader)).setText(masterData.FindByLocationID(LOCATION_ID).DisplayText +
                " - Table No - " + TABLE_NO);*/

        //-- Set Notification Key (SystemID_HotelKey_LocationID_VZACode)
        String key = SystemID + "_" + hotelKeyCode + "_" + LOCATION_ID + "_" + VZACODE;
        BaseApplication.NotificationRecordVariable.setVariable(getBaseContext(), key);

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("KEYCODEPara", hotelKeyCode));
        param.add(new BasicNameValuePair("HOTEL_IDPara", hotelID));
        param.add(new BasicNameValuePair("SYSTEMIDPara", SystemID));
        param.add(new BasicNameValuePair("SENDORRECEIVECODEPara", VZACODE));
        param.add(new BasicNameValuePair("LOCATIONIDPara", LOCATION_ID.toString()));
        broadcastUtil = new BroadcastUtil(this, param, site);
        broadcastUtil.setCastListener(broadcastUtil_OnCast);
        broadcastUtil.Start();

        //locationid , vzacode , user_email
        if(BaseApplication.MyDocRecordVariable.getStatus(GuestActivity.this).equals("") ||
                BaseApplication.MyLikeRecordVariable.getStatus(GuestActivity.this).equals("") ||
                BaseApplication.MyFavRecordVariable.getStatus(GuestActivity.this).equals("")) {

            String Email = BaseApplication.GlobalVariable.get(GuestActivity.this, UserInfoEntity.class).Data.EMAIL;

            List<NameValuePair> paramStatus = new ArrayList<>();
            paramStatus.add(new BasicNameValuePair("LOCATIONIDPara", LOCATION_ID.toString()));
            paramStatus.add(new BasicNameValuePair("VZACODEPara", VZACODE));
            paramStatus.add(new BasicNameValuePair("USER_EMAIL", Email));

            ServiceUtil serviceUtil = new ServiceUtil(GuestActivity.this, site, hotelKeyCode,
                    SiteOperate.VZAgetStatusMasterDataJSON, paramStatus, BaseEntity.class);
            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    //Init Adapter
                    BaseEntity entity = (BaseEntity) results;

                    //-- If Data is entry
                    if (entity.STATUS_MYBILL) {
                        //-- SET STATUS
                        BaseApplication.MyDocRecordVariable.setVariable(getBaseContext(), "Y");
                        //-- SET Background
                        findViewById(R.id.btnBill).setBackground(getResources().getDrawable(R.drawable.pic_bar_doc_red));
                    } else {
                        //-- SET STATUS
                        BaseApplication.MyDocRecordVariable.setVariable(getBaseContext(), "N");
                    }

                    //-- Like and Unlike
                    if (entity.LIKE_AND_UNLIKE) {
                        //-- SET STATUS
                        BaseApplication.MyLikeRecordVariable.setVariable(getBaseContext(), "Y");
                        //-- SET Background
                        findViewById(R.id.btnLike).setBackground(getResources().getDrawable(R.drawable.pic_bar_like_red));
                    } else {
                        //-- SET STATUS
                        BaseApplication.MyLikeRecordVariable.setVariable(getBaseContext(), "N");
                    }

                    //-- Favorite
                    if (entity.FAVORITE) {
                        //-- SET STATUS
                        BaseApplication.MyFavRecordVariable.setVariable(getBaseContext(), "Y");
                        //-- SET Background
                        findViewById(R.id.btnFav).setBackground(getResources().getDrawable(R.drawable.pic_bar_fav_red));
                    } else {
                        //-- SET STATUS
                        BaseApplication.MyFavRecordVariable.setVariable(getBaseContext(), "N");
                    }
                }
            });
            serviceUtil.execute();
        }

        if(BaseApplication.MyDocRecordVariable.getStatus(GuestActivity.this).equals("Y"))
            findViewById(R.id.btnBill).setBackground(getResources().getDrawable(R.drawable.pic_bar_doc_red));

        if(BaseApplication.MyLikeRecordVariable.getStatus(GuestActivity.this).equals("Y"))
            findViewById(R.id.btnLike).setBackground(getResources().getDrawable(R.drawable.pic_bar_like_red));

        if(BaseApplication.MyFavRecordVariable.getStatus(GuestActivity.this).equals("Y"))
            findViewById(R.id.btnFav).setBackground(getResources().getDrawable(R.drawable.pic_bar_fav_red));
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Get Entity
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getBaseContext(), MasterEntity.class);
        CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(getBaseContext(), CheckLocationEntity.class);
        //QRCodeInfoEntity qrEntity = BaseApplication.GlobalVariable.get(getBaseContext(), QRCodeInfoEntity.class);
        Integer LOCATION_ID = locationEntity.LOCATION_ID;//Integer.parseInt(qrEntity.data.Location_ID); //locationEntity.LOCATION_ID;
        String TABLE_NO = locationEntity.TABLE_NO; //qrEntity.data.Table_No; //locationEntity.TABLE_NO;
        /*if(LOCATION_ID.equals(0)){
            LOCATION_ID = Integer.parseInt(qrEntity.data.Location_ID);
            TABLE_NO = qrEntity.data.Table_No;
        }*/

        //Init Top Bar
        ((TextView) findViewById(R.id.txtHeader)).setText(masterData.FindByLocationID(LOCATION_ID).DisplayText +
                " - Table No - " + TABLE_NO);

        //Init Cart Notification
        BaseApplication.CartStateVariable.setOnChangeListener(OnCart_OnChange);
        OnCart_OnChange.OnChange(BaseApplication.CartStateVariable.length());

        //Interrupt Notification
        broadcastUtil.Execute();
    }

    @Override
    protected void onDestroy() {
        //Un-Init Notification
        broadcastUtil.Shutdown();

        super.onDestroy();
    }

    public BroadcastUtil.OnCast broadcastUtil_OnCast = new BroadcastUtil.OnCast() {
        @Override
        public void OnCast(Integer totalRecord) {
            String displayRecord = "";
            if (totalRecord > 0) {
                if (totalRecord >= 100) {
                    displayRecord = "99+";
                } else {
                    displayRecord = totalRecord.toString();
                }
                findViewById(R.id.btnNoti).setBackground(getResources().getDrawable(R.drawable.pic_bar_noti_red));
            } else {
                findViewById(R.id.btnNoti).setBackground(getResources().getDrawable(R.drawable.pic_bar_noti));
            }
            if (BroadcastUtil.NotificationData.size() > 0) {
                findViewById(R.id.btnNoti).setOnClickListener(btnNoti_OnClick);
            } else {
                findViewById(R.id.btnNoti).setOnClickListener(null);
            }
            ((TextView) findViewById(R.id.txtNoti)).setText(displayRecord);
        }
    };

    public MenuView.OnPageChangeListener menuView_OnPageChange = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int form, float value, int to) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        @Override
        public void onPageSelected(final int position) {
            //Set Swipe Page
            GuestActivity.this.setSwipeBackEnable(position == 0 ? true : false);
        }
    };

    public BaseApplication.CartStateVariable.OnChange OnCart_OnChange = new BaseApplication.CartStateVariable.OnChange() {
        @Override
        public void OnChange(Integer length) {
            if (length > 0) {
                (findViewById(R.id.btnCart)).setBackgroundResource(R.drawable.bar_cart_used);
                (findViewById(R.id.btnCart)).setOnClickListener(btnCart_OnClick);
                ((TextView) findViewById(R.id.txtCart)).setText(length.toString());
            } else {
                (findViewById(R.id.btnCart)).setBackgroundResource(R.drawable.bar_cart);
                (findViewById(R.id.btnCart)).setOnClickListener(null);
                ((TextView) findViewById(R.id.txtCart)).setText("");
            }
        }
    };

    public Button.OnClickListener btnCart_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(GuestActivity.this, MyCartActivity.class);
            startActivity(intent);
        }
    };

    public Button.OnClickListener btnBill_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getBaseContext(), HotelInfoEntity.class);
            CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(getBaseContext(), CheckLocationEntity.class);
            VZACodeEntity vzaEntity = BaseApplication.GlobalVariable.get(getBaseContext(), VZACodeEntity.class);
            QRCodeInfoEntity qrEntity = BaseApplication.GlobalVariable.get(getBaseContext(), QRCodeInfoEntity.class);
            Integer LOCATION_ID = Integer.parseInt(qrEntity.data.Location_ID); //locationEntity.LOCATION_ID;
            String TABLE_NO = qrEntity.data.Table_No; //locationEntity.TABLE_NO;
            String VZACODE = vzaEntity.VZACODE == "" ? locationEntity.VZACODE : vzaEntity.VZACODE;
            String hotelKeyCode = hotelInfo.DATA.HOTEL_KEYCODE;
            String site = hotelInfo.DATA.HOTEL_URL;

            String Email = BaseApplication.GlobalVariable.get(GuestActivity.this, UserInfoEntity.class).Data.EMAIL;

            List<NameValuePair> paramStatus = new ArrayList<>();
            paramStatus.add(new BasicNameValuePair("LOCATIONIDPara", LOCATION_ID.toString()));
            paramStatus.add(new BasicNameValuePair("VZACODEPara", VZACODE));
            paramStatus.add(new BasicNameValuePair("USER_EMAIL", Email));

            ServiceUtil serviceUtil = new ServiceUtil(GuestActivity.this, site, hotelKeyCode,
                    SiteOperate.VZAgetStatusMasterDataJSON, paramStatus, BaseEntity.class);
            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    //Init Adapter
                    BaseEntity entity = (BaseEntity) results;

                    //-- If Data is entry
                    if (entity.STATUS_MYBILL) {
                        //-- SET STATUS
                        BaseApplication.MyDocRecordVariable.setVariable(getBaseContext(), "Y");
                        //-- SET Background
                        findViewById(R.id.btnBill).setBackground(getResources().getDrawable(R.drawable.pic_bar_doc_red));
                    } else {
                        //-- SET STATUS
                        BaseApplication.MyDocRecordVariable.setVariable(getBaseContext(), "N");
                    }

                    //-- Like and Unlike
                    if (entity.LIKE_AND_UNLIKE) {
                        //-- SET STATUS
                        BaseApplication.MyLikeRecordVariable.setVariable(getBaseContext(), "Y");
                        //-- SET Background
                        findViewById(R.id.btnLike).setBackground(getResources().getDrawable(R.drawable.pic_bar_like_red));
                    } else {
                        //-- SET STATUS
                        BaseApplication.MyLikeRecordVariable.setVariable(getBaseContext(), "N");
                    }

                    //-- Favorite
                    if (entity.FAVORITE) {
                        //-- SET STATUS
                        BaseApplication.MyFavRecordVariable.setVariable(getBaseContext(), "Y");
                        //-- SET Background
                        findViewById(R.id.btnFav).setBackground(getResources().getDrawable(R.drawable.pic_bar_fav_red));
                    } else {
                        //-- SET STATUS
                        BaseApplication.MyFavRecordVariable.setVariable(getBaseContext(), "N");
                    }

                    //-- Check Order
                    //if(BaseApplication.MyDocRecordVariable.getStatus(GuestActivity.this).equals("Y")) {
                        Intent intent = new Intent(GuestActivity.this, MyDocActivity.class);
                        startActivity(intent);
                    /*}else{
                        ChooseDialog dialog = ChooseDialog.newInstance(GuestActivity.this,getResources().getString(R.string.alert_mydocument_empty), false);
                        dialog.show(getFragmentManager(), "tag");
                    }*/
                }
            });
            serviceUtil.execute();

        }
    };

    public Button.OnClickListener btnLike_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(GuestActivity.this, MyLikeActivity.class);
            startActivity(intent);
        }
    };

    public Button.OnClickListener btnNoti_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Init Notification
            broadcastUtil.ReadRecord();
            Intent intent = new Intent(GuestActivity.this, MyNotiActivity.class);
            startActivity(intent);
        }
    };

    public Button.OnClickListener btnFav_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(GuestActivity.this, MyFavActivity.class);
            startActivity(intent);
        }
    };

    public Button.OnClickListener btnLocation_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (BaseApplication.CartStateVariable.length() > 0) {
                ChooseDialog dialog = ChooseDialog.newInstance(GuestActivity.this, getString(R.string.alert_check_mycart), false);
                dialog.show(GuestActivity.this.getFragmentManager(), "tag");
                return;
            }

            //-- Dialog Confirm Change Location
            ChooseDialog dialog = ChooseDialog.newInstance(GuestActivity.this, getResources().getString(R.string.alert_change_location), true);
            dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                @Override
                public void OnSubmit(View view) {
                    Intent intent = new Intent(GuestActivity.this, QRCodeActivity.class);
                    intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            });
            dialog.show(getFragmentManager(), "tag");

        }
    };

    /*public ImageButton.OnClickListener imgConfig_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(GuestActivity.this, MyCartActivity.class);
            startActivity(intent);
        }
    };*/


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        MenuView menuView = (MenuView) findViewById(R.id.frmFragment);
        if (menuViewAdapter.getCount() > 0) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            menuViewAdapter = new MenuViewAdapter(fragmentManager);
            menuViewAdapter.addItem(menuView, MainFragment.newInstance());
            //menuViewAdapter.addItem(menuView, fragmentManager.getFragments().get(fragmentManager.getFragments().size()-1));
            menuView.setAdapter(menuViewAdapter);
        }
    }

    @Override
    public void onBackPressed() {
        //Custom Anim
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slideout);
    }


}
