package com.smartfinder.vzaapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.smartfinder.vzaapp.Tutorial.TutorialActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import control.Dialog.ChooseDialog;
import emuntype.MainOperate;
import emuntype.UserType;
import entity.Main.QRCodeInfo.QRCodeEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Main.VerifyUser.VerifyUserEntity;
import util.ServiceUtil;

public class LoginActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Get Login Record
        ((EditText) findViewById(R.id.txtEmail)).setText(BaseApplication.LoginRecordVariable.getUsername(this));
        ((EditText) findViewById(R.id.txtPassword)).setText(BaseApplication.LoginRecordVariable.getPassword(this));

        //Init Event
        (findViewById(R.id.btnLogin)).setOnClickListener(btnLogin_onClick);
        (findViewById(R.id.btnForgot)).setOnClickListener(btnForgot_onClick);
        (findViewById(R.id.btnRegister)).setOnClickListener(btnRegister_onClick);
        (findViewById(R.id.imgConfig)).setOnClickListener(imgConfig_onClick);
        ((EditText) findViewById(R.id.txtPassword)).setOnEditorActionListener(txtPassword_onEditorAction);

        //Init Tutorial
        if (savedInstanceState == null && (BaseApplication.LoginRecordVariable.getUsername(this).isEmpty() && BaseApplication.LoginRecordVariable.getPassword(this).isEmpty())) {
            startActivity(new Intent(this, TutorialActivity.class));
        }

    }

    private Button.OnClickListener btnLogin_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (((EditText) findViewById(R.id.txtEmail)).getText().toString().length() == 0 ||
                    ((EditText) findViewById(R.id.txtPassword)).getText().toString().length() == 0) {
                ChooseDialog dialog = ChooseDialog.newInstance(LoginActivity.this, getResources().getString(R.string.alert_fill_login), false);
                dialog.show(getFragmentManager(), "tag");
                return;
            }
            try {
                //Call Login Service
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("EMAILPara", ((EditText) findViewById(R.id.txtEmail)).getText().toString()));
                param.add(new BasicNameValuePair("PASSCODEPara", ((EditText) findViewById(R.id.txtPassword)).getText().toString()));

                //New Service
                ServiceUtil verifyService = new ServiceUtil(LoginActivity.this, MainOperate.VerifyUserJSON, param, VerifyUserEntity.class);
                verifyService.setFinishedListener(verifyService_onFinished);
                verifyService.execute();
            }catch(Exception ex){
                ChooseDialog dialog = ChooseDialog.newInstance(LoginActivity.this, getResources().getString(R.string.alert_fill_login), false);
                dialog.show(getFragmentManager(), "tag");
                return;
            }
        }
    };

    private Button.OnClickListener btnForgot_onClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Keep Email
            String strEmail = ((EditText) findViewById(R.id.txtEmail)).getText().toString();
            Intent intent = new Intent(LoginActivity.this, ForgotActivity.class);
            if (strEmail.length() > 0) {
                intent.putExtra("email", strEmail);
            }
            startActivity(intent);
        }
    };

    private Button.OnClickListener btnRegister_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
    };

    private ImageButton.OnClickListener imgConfig_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, ConfigActivity.class);
            startActivity(intent);
        }
    };

    private EditText.OnEditorActionListener txtPassword_onEditorAction = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            (findViewById(R.id.btnLogin)).performClick();
            return false;
        }
    };

    private ServiceUtil.OnFinished verifyService_onFinished = new ServiceUtil.OnFinished() {
        @Override
        public void OnFinished(Object results) {
            final VerifyUserEntity verifyUser = (VerifyUserEntity) results;
            if (verifyUser.RESULT != true) {
                ChooseDialog dialog = ChooseDialog.newInstance(LoginActivity.this, getResources().getString(R.string.alert_auth_failed), false);
                dialog.show(getFragmentManager(), "tag");
                ((EditText) findViewById(R.id.txtPassword)).setText("");
                return;
            } else {
                //Keep verifyUser To AppVariable
                BaseApplication.GlobalVariable.set(verifyUser);

                //Call Login Service
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("EMAILPara", verifyUser.EMAIL));

                //new Service
                ServiceUtil userInfoService = new ServiceUtil(LoginActivity.this, MainOperate.UserInfoJSON, param, UserInfoEntity.class);
                userInfoService.setFinishedListener(userInfoService_onFinished);
                userInfoService.execute();
            }
        }
    };

    private ServiceUtil.OnFinished userInfoService_onFinished = new ServiceUtil.OnFinished() {
        @Override
        public void OnFinished(Object results) {
            UserInfoEntity userInfo = (UserInfoEntity) results;
            //Keep userInfo To AppVariable
            BaseApplication.GlobalVariable.set(userInfo);

            //Keep To Record
            BaseApplication.LoginRecordVariable.setVariable(LoginActivity.this, userInfo.Data.EMAIL, userInfo.Data.PASSCODE);

            //Check UserType
            if (BaseApplication.GlobalVariable.get(getBaseContext(), VerifyUserEntity.class).GUEST_TYPE == UserType.Staff.getValue()) {
                Intent intent = new Intent(LoginActivity.this, CheckVzaActivity.class);
                intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                Intent intent = new Intent(LoginActivity.this, QRCodeActivity.class);
                //intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
    };

}
