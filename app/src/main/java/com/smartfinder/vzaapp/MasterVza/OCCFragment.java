package com.smartfinder.vzaapp.MasterVza;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.ViewList.OccAdapter;
import emuntype.MainOperate;
import entity.BaseEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.OccInfo.OccInfoEntity;
import util.ServiceUtil;

public class OCCFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mastervza_occ,container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        //Init Data
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("HOTEL_IDPara",BaseApplication.GlobalVariable.get(getActivity(),HotelInfoEntity.class).DATA.HOTEL_ID.toString()));
        ServiceUtil serviceUtil = new ServiceUtil(getActivity(), MainOperate.HotelHmsOccListListJSON,param, OccInfoEntity.class);
        serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                OccInfoEntity entity = (OccInfoEntity)results;
                ListView listView = ((ListView)getActivity().findViewById(R.id.lisOcc));
                listView.setAdapter(new OccAdapter(getActivity(),entity.HMSOCCList));
            }
        });
        serviceUtil.execute();
    }
}
