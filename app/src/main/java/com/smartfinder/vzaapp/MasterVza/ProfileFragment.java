package com.smartfinder.vzaapp.MasterVza;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.LoginActivity;
import com.smartfinder.vzaapp.MasterVzaActivity;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.Dialog.ProfileDialog;
import emuntype.MainOperate;
import entity.BaseEntity;
import entity.Main.UserInfo.UserInfoEntity;
import util.ServiceUtil;
import util.ViewUtil;

public class ProfileFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mastervza_profile, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        //Init Data
        UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(getActivity(), UserInfoEntity.class);

        //Init Control
        if (userInfoEntity.Data.TITLE.equals("Male")) {
            getActivity().findViewById(R.id.rdoMale).performClick();
        } else {
            getActivity().findViewById(R.id.rdoFemale).performClick();
        }

        //Receive Data
        ((EditText) getActivity().findViewById(R.id.txtFirstName)).setText(userInfoEntity.Data.FIRSTNAME);
        ((EditText) getActivity().findViewById(R.id.txtLastName)).setText(userInfoEntity.Data.LASTNAME);
        ((EditText) getActivity().findViewById(R.id.txtEmail)).setText(userInfoEntity.Data.EMAIL);
        ((EditText) getActivity().findViewById(R.id.txtResidence)).setText(userInfoEntity.Data.RESIDENCE_COUNTRY);
        ((EditText) getActivity().findViewById(R.id.txtPhone)).setText(userInfoEntity.Data.TELEPHONE);
        ((EditText) getActivity().findViewById(R.id.txtPassword)).setText(userInfoEntity.Data.PASSCODE);

        //Hidden Edit Mode
        ViewUtil.isEnableView(getActivity().findViewById(R.id.frmInfo), false);
        getActivity().findViewById(R.id.btnEdit).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.btnLogout).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.btnSubmit).setVisibility(View.GONE);
        getActivity().findViewById(R.id.btnCancel).setVisibility(View.GONE);

        //Init Event
        getActivity().findViewById(R.id.btnSubmit).setOnClickListener(btnSubmit_OnClick);
        getActivity().findViewById(R.id.btnCancel).setOnClickListener(btnCancel_OnClick);
        getActivity().findViewById(R.id.btnEdit).setOnClickListener(btnEdit_OnClick);
        getActivity().findViewById(R.id.btnLogout).setOnClickListener(btnLogout_OnClick);

        getActivity().findViewById(R.id.frmInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //-- Keyboard Hide
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        });
    }

    public Button.OnClickListener btnEdit_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Show Edit Mode
            ViewUtil.isEnableView(getActivity().findViewById(R.id.frmInfo), true);
            getActivity().findViewById(R.id.btnSubmit).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.btnEdit).setVisibility(View.GONE);
            getActivity().findViewById(R.id.btnLogout).setVisibility(View.GONE);
        }
    };

    public Button.OnClickListener btnLogout_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity().onBackPressed();
        }
    };

    public Button.OnClickListener btnSubmit_OnClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            //Init Data
            final UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(getActivity(), UserInfoEntity.class);

            //Init Control
            final String txtEmail = ((EditText) getActivity().findViewById(R.id.txtEmail)).getText().toString();
            Boolean rdoMale = ((RadioButton) getActivity().findViewById(R.id.rdoMale)).isChecked();
            String txtFirstName = ((EditText) getActivity().findViewById(R.id.txtFirstName)).getText().toString();
            String txtLastName = ((EditText) getActivity().findViewById(R.id.txtLastName)).getText().toString();
            String txtResidence = ((EditText) getActivity().findViewById(R.id.txtResidence)).getText().toString();
            String txtPhone = ((EditText) getActivity().findViewById(R.id.txtPhone)).getText().toString();
            String txtPassword = ((EditText) getActivity().findViewById(R.id.txtPassword)).getText().toString();

            final boolean result = false;

            if (txtFirstName.length() == 0 || txtLastName.length() == 0 || txtEmail.length() == 0 || txtPhone.length() == 0 || txtPassword.length() == 0) {
                ChooseDialog dialog = ChooseDialog.newInstance(getActivity(), getResources().getString(R.string.alert_register_fill), false);
                dialog.show(getActivity().getFragmentManager(), "tag");
            } else if (!Patterns.EMAIL_ADDRESS.matcher(txtEmail).matches()) {
                ChooseDialog dialog = ChooseDialog.newInstance(getActivity(), getResources().getString(R.string.alert_email_invalid), false);
                dialog.show(getActivity().getFragmentManager(), "tag");
            } else {
                if (txtPhone.length() < 9) {
                    ChooseDialog dialog = ChooseDialog.newInstance(getActivity(), getResources().getString(R.string.alert_phone_invalid), false);
                    dialog.show(getActivity().getFragmentManager(), "tag");
                } else {
                    List<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("IDPara", userInfoEntity.Data.ID.toString()));
                    if (rdoMale) {
                        param.add(new BasicNameValuePair("TITLENAMEPara", "Male"));
                    } else {
                        param.add(new BasicNameValuePair("TITLENAMEPara", "Famale"));
                    }
                    param.add(new BasicNameValuePair("FIRSTNAMEPara", txtFirstName));
                    param.add(new BasicNameValuePair("LASTNAMEPara", txtLastName));
                    param.add(new BasicNameValuePair("EMAILPara", txtEmail));
                    param.add(new BasicNameValuePair("RESIDENCECOUNTRYPara", txtResidence));
                    param.add(new BasicNameValuePair("TELEPHONEPara", txtPhone));
                    param.add(new BasicNameValuePair("PASSCODEPara", txtPassword));

                    ServiceUtil serviceUtil = new ServiceUtil(getActivity(),
                            MainOperate.UpdateRegisterJSON, param,
                            BaseEntity.class);

                    serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {

                            //Call Login Service
                            List<NameValuePair> param = new ArrayList<NameValuePair>();
                            param.add(new BasicNameValuePair("EMAILPara", txtEmail));
                            ServiceUtil userInfoService = new ServiceUtil(getActivity(), MainOperate.UserInfoJSON,
                                    param, UserInfoEntity.class);

                            userInfoService.setFinishedListener(userInfoService_onFinished);
                            userInfoService.execute();
                        }
                    });
                    serviceUtil.execute();
                }
            }

        }
    };

    private ServiceUtil.OnFinished userInfoService_onFinished = new ServiceUtil.OnFinished() {
        @Override
        public void OnFinished(Object results) {
            //Keep userInfo To AppVariable
            UserInfoEntity userInfo = (UserInfoEntity) results;
            BaseApplication.GlobalVariable.set(userInfo);

            ProfileDialog dialog = ProfileDialog.newInstance(getActivity(), getResources().getString(R.string.alert_user_update_success), false);
            dialog.setOnSubmitListener(new ProfileDialog.OnSubmit() {
                @Override
                public void OnSubmit(View view) {
                    ProfileFragment.this.onResume();
                }
            });
            dialog.show(getActivity().getFragmentManager(), "tag");

        }
    };

    public Button.OnClickListener btnCancel_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ProfileFragment.this.onResume();
        }
    };

}
