package com.smartfinder.vzaapp.MasterVza;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.GuestActivity;
import com.smartfinder.vzaapp.MasterVzaActivity;
import com.smartfinder.vzaapp.R;
import com.smartfinder.vzaapp.StaffActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.Dialog.OpenTableDialog;
import control.View.AnimateView;
import control.ViewList.ServiceAdapter;
import control.ViewList.ServiceModel;
import control.ViewList.TableListAdapter;
import control.ViewList.ViewList;
import emuntype.SiteOperate;
import emuntype.UserType;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Main.VerifyUser.VerifyUserEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.OpenTable.OpenTableEntity;
import entity.Service.TableList.TableListEntity;
import entity.Service.VZACode.VZACodeEntity;
import entity.Service.Master.MasterData;
import entity.Service.VZAOpen.VZAOpenEntity;
import util.ServiceUtil;

public class ServiceFragment extends Fragment {

    private static Integer locationID = Integer.MIN_VALUE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_mastervza_service, container, false);

        //Set Header Image Service
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), HotelInfoEntity.class);
        AnimateView animateView = (AnimateView) view.findViewById(R.id.imgHeader);
        animateView.setImageService(hotelInfo.DATA.HOTEL_IMAGE);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //Init Master List
        List<ServiceModel> lisModel = new ArrayList<>();
        for (MasterData data : BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), MasterEntity.class).LocationList) {
            lisModel.add(new ServiceModel(data.IMAGENAME, data.DisplayText));
        }

        ServiceAdapter serviceAdapter = new ServiceAdapter(getActivity(), lisModel);
        final ViewList lisView = (ViewList) getActivity().findViewById(R.id.lisService);
        lisView.setAdapter(serviceAdapter);
        lisView.setOnItemClickListener(lisView_onItemClick);

        //Get Dialog
        android.app.Fragment tagFragment = getActivity().getFragmentManager().findFragmentByTag("tag");
        if (tagFragment != null) {
            getActivity().getFragmentManager().beginTransaction().remove(tagFragment).commit();
        }

        //Check Auto Check Location
        if (!((MasterVzaActivity) getActivity()).isShown) {
            if (lisModel.size() == 1) {
                lisView.performItemClick(lisView, 0, lisView.getItemIdAtPosition(0));
                ((MasterVzaActivity) getActivity()).isShown = true;
            }
        }
    }

    public ListView.OnItemClickListener lisView_onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Check UserType
            VerifyUserEntity verifyUser = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), VerifyUserEntity.class);
            MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), MasterEntity.class);
            UserInfoEntity userInfo = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), UserInfoEntity.class);
            final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), HotelInfoEntity.class);
            MasterData[] aryMasterData = masterData.LocationList.toArray(new MasterData[masterData.LocationList.size()]);

            //Check ToCartState
            if (!locationID.equals(aryMasterData[position].LOCATION_ID)) {
                if (BaseApplication.CartStateVariable.length() > 0) {
                    ChooseDialog dialog = ChooseDialog.newInstance(getActivity(), getString(R.string.alert_check_mycart), false);
                    dialog.show(getActivity().getFragmentManager(), "tag");
                    return;
                }
            }

            //Keep Location ID
            locationID = aryMasterData[position].LOCATION_ID;

            if (verifyUser.GUEST_TYPE != UserType.Staff.getValue()) {

                //Check GetShowVZA
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                param.add(new BasicNameValuePair("TELEPHONNOPara", userInfo.Data.TELEPHONE));
                param.add(new BasicNameValuePair("LOCATIONIDPara", locationID.toString()));
                param.add(new BasicNameValuePair("GUEST_TYPEPara", userInfo.Data.GUEST_TYPE.toString()));

                String site = hotelInfo.DATA.HOTEL_URL;
                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                ServiceUtil vzaCodeService = new ServiceUtil(getActivity(), site, keycode, SiteOperate.VZAGetShowVZACODEbyTelephonJSON, param, VZACodeEntity.class);
                vzaCodeService.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        //Keep vzaCode To AppVariable
                        VZACodeEntity vzaCode = (VZACodeEntity) results;

                        BaseApplication.GlobalVariable.set(vzaCode);

                        //VZACodeEntity vzaCode = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), VZACodeEntity.class);
                        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), HotelInfoEntity.class);

                        List<NameValuePair> param = new ArrayList<NameValuePair>();
                        param.add(new BasicNameValuePair("LOCATION_IDPara", locationID.toString()));
                        param.add(new BasicNameValuePair("VZA_CODEPara", vzaCode.VZACODE));
                        String site = hotelInfo.DATA.HOTEL_URL;
                        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                        ServiceUtil checkLocationService = new ServiceUtil(getActivity(), site, keycode, SiteOperate.VZACheckLocationOpenedJSON, param, CheckLocationEntity.class);
                        checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
                            @Override
                            public void OnFinished(Object results) {
                                //Keep checkLocation To AppVariable
                                BaseApplication.GlobalVariable.set((CheckLocationEntity) results);
                                Intent intent = new Intent(getActivity(), GuestActivity.class);
                                BaseApplication.RequestCheckVariable.set(false);
                                startActivity(intent);
                            }
                        });
                        checkLocationService.execute();
                    }
                });
                vzaCodeService.execute();

            } else {
                //Check Location
                String site = hotelInfo.DATA.HOTEL_URL;
                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                final ServiceUtil checkLocationService = new ServiceUtil(getActivity(), site, keycode, SiteOperate.VZACheckLocationOpenedJSON, CheckLocationEntity.class);
                final ServiceUtil openTableService = new ServiceUtil(getActivity(), site, keycode, SiteOperate.VZACheckTableOpenedJSON, OpenTableEntity.class);
                final ServiceUtil vzaOpenService = new ServiceUtil(getActivity(), site, keycode, SiteOperate.VZAOpenJSON, VZAOpenEntity.class);

                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("SYSTEMIDPara", BaseApplication.GlobalVariable.get(getActivity(), MasterEntity.class).FindByLocationID(locationID).SYSTEMID.toString()));
                param.add(new BasicNameValuePair("LOCATION_IDPara", locationID.toString()));

                ServiceUtil serviceUtil = new ServiceUtil(getActivity(), site, keycode, SiteOperate.POSgetTableListForStaffJSON, param, TableListEntity.class);
                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        TableListEntity entity = (TableListEntity) results;
                        //Open Table Dialog
                        OpenTableDialog dialog = OpenTableDialog.newInstance(getActivity(), entity);
                        dialog.setOnSubmitListener(new OpenTableDialog.OnSubmit() {
                            @Override
                            public void OnSubmit(final String table, final Integer pax, final String telephone) {

                                List<NameValuePair> param = new ArrayList<>();
                                param.add(new BasicNameValuePair("LOCATION_IDPara", locationID.toString()));
                                param.add(new BasicNameValuePair("VZA_CODEPara", "T" + table));
                                checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
                                    @Override
                                    public void OnFinished(Object results) {
                                        //Keep checkLocation To AppVariable
                                        CheckLocationEntity entity = (CheckLocationEntity) results;
                                        entity.VZACODE = "T" + entity.TABLE_NO;
                                        BaseApplication.GlobalVariable.set(entity);

                                        //Keep vzaCode To AppVariable
                                        VZACodeEntity vzaEntity = new VZACodeEntity();
                                        vzaEntity.VZACODE = "T" + entity.TABLE_NO;
                                        BaseApplication.GlobalVariable.set(vzaEntity);


                                        //-- Call Table Open
                                        List<NameValuePair> param = new ArrayList<>();
                                        param.add(new BasicNameValuePair("LOCATION_IDPara", locationID.toString()));
                                        param.add(new BasicNameValuePair("TABLE_NOPara", entity.TABLE_NO));
                                        param.add(new BasicNameValuePair("ACCEPT_COVERPara", pax.toString()));
                                        param.add(new BasicNameValuePair("ACTION_PROCRESSPara", "N"));
                                        openTableService.setFinishedListener(new ServiceUtil.OnFinished() {
                                            @Override
                                            public void OnFinished(Object results) {
                                                OpenTableEntity entity = (OpenTableEntity) results;
                                                if (entity.RESULT) {
                                                    //-- If telephone is null
                                                    if (telephone.equals("")) {
                                                        Intent intent = new Intent(getActivity(), StaffActivity.class);
                                                        intent.putExtra("VZACODE", "");
                                                        BaseApplication.RequestCheckVariable.set(false);
                                                        startActivity(intent);
                                                    } else {
                                                        //-- VZAOpenJSON
                                                        List<NameValuePair> param = new ArrayList<>();
                                                        param.add(new BasicNameValuePair("LOCATION_IDPara", locationID.toString()));
                                                        param.add(new BasicNameValuePair("TABLE_NOPara", table));
                                                        param.add(new BasicNameValuePair("TELEPHONE_NOPara", telephone));
                                                        vzaOpenService.setFinishedListener(new ServiceUtil.OnFinished() {
                                                            @Override
                                                            public void OnFinished(Object results) {
                                                                VZAOpenEntity vzaOpenEntity = (VZAOpenEntity) results;
                                                                if (vzaOpenEntity.ERRORMSG.equals("")) {
                                                                    Intent intent = new Intent(getActivity(), StaffActivity.class);
                                                                    intent.putExtra("VZACODE", vzaOpenEntity.VZACODE);
                                                                    BaseApplication.RequestCheckVariable.set(false);
                                                                    startActivity(intent);
                                                                } else {
                                                                    //-- ERROR
                                                                    ChooseDialog errorDialog = ChooseDialog.newInstance(getActivity(), vzaOpenEntity.ERRORMSG, false);
                                                                    errorDialog.show(getActivity().getFragmentManager(), "tag");
                                                                }
                                                            }
                                                        });
                                                        vzaOpenService.execute(param);
                                                    }
                                                } else {
                                                    ChooseDialog chooseDialog = ChooseDialog.newInstance(getActivity(), getResources().getString(R.string.alert_table_not_exist), false);
                                                    chooseDialog.show(getActivity().getFragmentManager(), "tag");
                                                }
                                            }
                                        });
                                        openTableService.execute(param);
                                    }
                                });
                                checkLocationService.execute(param);
                            }
                        });
                        dialog.show(getActivity().getFragmentManager(), "tag");
                    }
                });
                serviceUtil.execute();
            }
        }
    };

}
