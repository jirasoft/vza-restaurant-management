package com.smartfinder.vzaapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.IntentCompat;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import com.smartfinder.vzaapp.MasterVza.AboutFragment;
import com.smartfinder.vzaapp.MasterVza.OCCFragment;
import com.smartfinder.vzaapp.MasterVza.ProfileFragment;
import com.smartfinder.vzaapp.MasterVza.ServiceFragment;
import control.Dialog.ChooseDialog;
import emuntype.UserType;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Main.VerifyUser.VerifyUserEntity;

public class MasterVzaActivity extends FragmentActivity {

    public static View isView = null; // Fix issue No 24.
    //public static boolean isEdit = false; // Fix issue No 24.
    public boolean isShown = false;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mastervza);

        ((RadioGroup)findViewById(R.id.rgpMenu)).setOnCheckedChangeListener(rgpMenu_OnCheckedChange);
        findViewById(R.id.btnLocation).setOnClickListener(btnLocation_OnClick);
        findViewById(R.id.btnService).setOnClickListener(grpMenu_OnClick);
        findViewById(R.id.btnProfile).setOnClickListener(grpMenu_OnClick);
        findViewById(R.id.btnAbout).setOnClickListener(grpMenu_OnClick);
        findViewById(R.id.btnOCC).setOnClickListener(grpMenu_OnClick);

        //Check UserType
        if(BaseApplication.GlobalVariable.get(getBaseContext(),VerifyUserEntity.class).GUEST_TYPE == UserType.Staff.getValue()){
            if(!BaseApplication.GlobalVariable.get(getBaseContext(),UserInfoEntity.class).Data.GET_HMSOCC){
                findViewById(R.id.btnOCC).setVisibility(View.GONE);
            }else{
                findViewById(R.id.btnOCC).setVisibility(View.VISIBLE);
            }
        }else{
            findViewById(R.id.btnOCC).setVisibility(View.GONE);
        }

       //Init Fragment
       if(savedInstanceState == null) {
           //Init Animate
           FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
           fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
           fragmentTransaction.commit();

           //Init Cart
           BaseApplication.CartStateVariable.clearAll();
       }

        //Init Event
       if(isView == null || isView.getId() == R.id.btnService) {
           findViewById(R.id.btnService).performClick();
       }else if (isView.getId() == R.id.btnProfile){
               findViewById(R.id.btnProfile).performClick();
       }else{
           findViewById(R.id.btnAbout).performClick();
       }

       //Check isShown
       isShown = savedInstanceState != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public RadioGroup.OnCheckedChangeListener rgpMenu_OnCheckedChange = new RadioGroup.OnCheckedChangeListener() {
       @Override
       public void onCheckedChanged(RadioGroup group, int checkedId) {
           for (int j = 0; j < group.getChildCount(); j++) {
               final Button view = (Button) group.getChildAt(j);
               view.setSelected(view.getId() == checkedId);
           }
       }
   };

    public Button.OnClickListener grpMenu_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toggle Button
            ((RadioGroup)view.getParent()).check(view.getId());

            //-- Set View
            isView = view;

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            if(getSupportFragmentManager().findFragmentByTag("SERVICE" ) != null) {
                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("SERVICE"));
            }
            if(getSupportFragmentManager().findFragmentByTag("PROFILE") != null) {
                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("PROFILE"));
            }
            if(getSupportFragmentManager().findFragmentByTag("ABOUT") != null) {
                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("ABOUT"));
            }
            if(getSupportFragmentManager().findFragmentByTag("OCC") != null) {
                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("OCC"));
            }

            if(view.getId() == R.id.btnService){
                if(getSupportFragmentManager().findFragmentByTag("SERVICE") == null){
                    fragmentTransaction.add(R.id.frmFragment, new ServiceFragment(), "SERVICE");
                }else{
                    isShown = false; // Fix issue No 10.
                    //getSupportFragmentManager().findFragmentByTag("SERVICE").onResume(); // Fix issue No 10.
                    fragmentTransaction.show(getSupportFragmentManager().findFragmentByTag("SERVICE"));
                }
            }else if(view.getId() == R.id.btnProfile){
                if(getSupportFragmentManager().findFragmentByTag("PROFILE") == null){
                    fragmentTransaction.add(R.id.frmFragment, new ProfileFragment(), "PROFILE");
                }else{
                    //getSupportFragmentManager().findFragmentByTag("PROFILE").onResume();
                    fragmentTransaction.show(getSupportFragmentManager().findFragmentByTag("PROFILE"));
                }
            }else if(view.getId() == R.id.btnAbout){
                if(getSupportFragmentManager().findFragmentByTag("ABOUT") == null){
                    fragmentTransaction.add(R.id.frmFragment, new AboutFragment(), "ABOUT");
                }else{
                    fragmentTransaction.show(getSupportFragmentManager().findFragmentByTag("ABOUT"));
                }
            }else if(view.getId() == R.id.btnOCC){
                if(getSupportFragmentManager().findFragmentByTag("OCC") == null){
                    fragmentTransaction.add(R.id.frmFragment, new OCCFragment(), "OCC");
                }else{
                    //getSupportFragmentManager().findFragmentByTag("OCC").onResume();
                    fragmentTransaction.show(getSupportFragmentManager().findFragmentByTag("OCC"));
                }
            }
            fragmentTransaction.commit();
        }
    };

    public Button.OnClickListener btnLocation_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(BaseApplication.CartStateVariable.length() > 0){
                ChooseDialog dialog = ChooseDialog.newInstance(MasterVzaActivity.this,getString(R.string.alert_check_mycart),false);
                dialog.show(MasterVzaActivity.this.getFragmentManager(),"tag");
                return;
            }

            //-- Dialog Confirm Change Location
            ChooseDialog dialog = ChooseDialog.newInstance(MasterVzaActivity.this,getResources().getString(R.string.alert_change_location), true);
            dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                @Override
                public void OnSubmit(View view) {
                    Intent intent = new Intent(MasterVzaActivity.this,CheckVzaActivity.class);
                    intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            });
            dialog.show(getFragmentManager(), "tag");
        }
    };

    @Override
    public void onBackPressed() {
        ChooseDialog dialog = ChooseDialog.newInstance(MasterVzaActivity.this,getResources().getString(R.string.alert_logout), true);
        dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
            @Override
            public void OnSubmit(View view) {
                //Intent intent = getBaseContext().getPackageManager().getLaunchIntentForPackage( getBaseContext().getPackageName() );
                BaseApplication.LoginRecordVariable.setVariable(MasterVzaActivity.this, "", "");
                Intent intent = new Intent(MasterVzaActivity.this, LoginActivity.class);  //
                intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

                //-- Clear Notification 16/03/2015
                //BaseApplication.NotificationRecordVariable.setVariable(MasterVzaActivity.this,0,0);
            }
        });
        dialog.show(getFragmentManager(), "tag");
    }

}
