package com.smartfinder.vzaapp.Menu;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.smartfinder.vzaapp.AdsMenuActivity;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.GuestActivity;
import com.smartfinder.vzaapp.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.ViewList.MenuGroupAdapter;
import control.ViewList.MenuGroupModel;
import control.MenuView.MenuView;
import control.ViewList.ViewList;
import emuntype.MenuLookType;
import emuntype.SiteOperate;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterData;
import entity.Service.Master.MasterDetailList;
import entity.Service.Master.MasterEntity;
import entity.Service.MasterItem.MasterItemData;
import entity.Service.MasterItem.MasterItemEntity;
import util.ServiceUtil;

public class GroupFragment extends Fragment {

    public static final GroupFragment newInstance(){
        GroupFragment fragment = new GroupFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_group,container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        //Init Group List

        MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),MasterEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),CheckLocationEntity.class);

        final List<MasterDetailList> detailLists = (List<MasterDetailList>) masterData.FindByLocationID(checkLocation.LOCATION_ID).MenuDetail.MenugroupList;
        Collections.sort(detailLists,new Comparator<MasterDetailList>() {
            @Override
            public int compare(MasterDetailList lhs, MasterDetailList rhs) {
                return lhs.MENU_GROUP_SEQ.compareTo(rhs.MENU_GROUP_SEQ);
            }
        });

        //Add MenuGroupList
        List<MenuGroupModel> lisModel = new ArrayList<>();
        for(MasterDetailList detailList : detailLists ){
            lisModel.add(new MenuGroupModel(detailList.MENU_GROUP_IMAGENAME,detailList.DisplayText,detailList.MENU_GROUP_ID+"-"+detailList.GROUP_LOOKFOR));
        }

        MenuGroupAdapter serviceAdapter = new MenuGroupAdapter(getActivity(),lisModel);
        ViewList lisView = (ViewList) getActivity().findViewById(R.id.lisMenu);
        lisView.setAdapter(serviceAdapter);

        //Init Event
        lisView.setOnItemClickListener(lisView_OnItemClick);
    }

    public ViewList.OnItemClickListener lisView_OnItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Set Bundle Position Param
            Bundle bundle = new Bundle();
            bundle.putString("item", (view.findViewById(R.id.txtMessage)).toString());

            MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), MasterEntity.class);
            CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), CheckLocationEntity.class);

            //Init Position
            MasterData masterDetail = masterData.FindByLocationID(checkLocation.LOCATION_ID);

            //Get Value
            //Integer menuID = masterDetail.MenuDetail.MenugroupList.get(position).MENUSUBGROEP_DETAIL.get(0).MENU_SUBMENU_ID;
            Integer menuID = masterDetail.MenuDetail.MenugroupList.get(position).MENU_GROUP_ID;
            String lookFor = masterDetail.MenuDetail.MenugroupList.get(position).GROUP_LOOKFOR;
            MasterDetailList masterDatailList = masterDetail.MenuDetail.FindByID(menuID,lookFor);

            //Check MenuLook Type
            MenuView menuView = (MenuView)getActivity().findViewById(R.id.frmFragment);
            if(MenuLookType.NORMAL.getValue().equals(masterDatailList.GROUP_LOOKFOR)){
                ((AdsMenuActivity)getActivity()).menuViewAdapter.addItem(menuView, ItemGroupFragment.newInstance(menuID,lookFor));
            }else if(MenuLookType.SUBGROUP.getValue().equals(masterDatailList.GROUP_LOOKFOR)){
                ((AdsMenuActivity)getActivity()).menuViewAdapter.addItem(menuView, SubGroupFragment.newInstance(menuID,lookFor));
            }else if(MenuLookType.MENU_ITEM.getValue().equals(masterDatailList.GROUP_LOOKFOR) || MenuLookType.OPENFOOD.getValue().equals(masterDatailList.GROUP_LOOKFOR)){
                //Init Data
                UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),UserInfoEntity.class);
                HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),HotelInfoEntity.class);

                //Get MasterItem
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("GUEST_TYPEPara",userInfoEntity.Data.GUEST_TYPE.toString()));
                param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
                param.add(new BasicNameValuePair("MENU_GROUP_CODEPara",masterDatailList.MENU_GROUP_CODE));
                //param.add(new BasicNameValuePair("MENU_SUBMENU_IDPara",masterDatailList.FindByID(menuID).MENU_SUBMENU_ID.toString()));
                //param.add(new BasicNameValuePair("LOOKFORPara",masterDatailList.FindByID(menuID).PROMOTIONSET_LOOKFOR_ID.toString()));
                param.add(new BasicNameValuePair("MENU_SUBMENU_IDPara",masterDatailList.MENUSUBGROEP_DETAIL.get(0).MENU_SUBMENU_ID.toString()));
                param.add(new BasicNameValuePair("LOOKFORPara",masterDatailList.MENUSUBGROEP_DETAIL.get(0).PROMOTIONSET_LOOKFOR_ID.toString()));

                String site = hotelInfo.DATA.HOTEL_URL;
                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                final String groupLookFor = masterDatailList.GROUP_LOOKFOR;
                final ServiceUtil masterItemEntity = new ServiceUtil(getActivity(),site,keycode, SiteOperate.VZAgetMasterItemDataJSON,param, MasterItemEntity.class);
                masterItemEntity.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        BaseApplication.GlobalVariable.set((MasterItemEntity)results);
                        //Results Data
                        if(((MasterItemEntity)results).MenuList.size() == 1){
                            //Init Menu
                            Intent intent;
                            Integer itemID = ((List<MasterItemData>)((MasterItemEntity) results).MenuList).get(0).ID;
                            Bundle bundle = new Bundle();
                            bundle.putInt("itemID", itemID); //bundle.putInt("itemID",((List<MasterItemData>)((MasterItemEntity) results).MenuList).get(0).ID);
                            if(MenuLookType.MENU_ITEM.getValue().equals(groupLookFor)) {
                                intent = new Intent(getActivity(), ItemActivity.class);
                                bundle.putBoolean("isMenuSet", true); //bundle.putBoolean("isMenuSet", false);
                            }else{
                                intent = new Intent(getActivity(), OpenFoodActivity.class);
                                bundle.putBoolean("isMenuSet", false); //bundle.putBoolean("isMenuSet", false);
                            }
                            //bundle.putBoolean("isMenuSet", true); //bundle.putBoolean("isMenuSet", false);
                            bundle.putString("lookFor", groupLookFor);
                            bundle.putString("favItem","");
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }else{
                            ChooseDialog dialog = ChooseDialog.newInstance(getActivity(),getString(R.string.alert_data_empty), false);
                            dialog.show(getActivity().getFragmentManager(), "tag");
                        }
                    }
                });
                masterItemEntity.execute();
            }
        }
    };
}

