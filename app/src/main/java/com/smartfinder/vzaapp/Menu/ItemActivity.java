package com.smartfinder.vzaapp.Menu;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.View.AnimateView;
import control.ViewList.ItemExtraAdapter;
import control.ViewList.ItemExtraModel;
import control.ViewList.ViewList;
import emuntype.SiteOperate;
import entity.Host.CanOrder.CanOrderData;
import entity.Host.Cart.CheckOrderData;
import entity.Host.Cart.MenuOrderDataAddition;
import entity.Host.Cart.MenuOrderData;
import entity.Host.Cart.MenuOrderEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CanOrder.CanOrderEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.CheckLogin.CheckLoginEntity;
import entity.Service.CheckOrder.CheckOrderEntity;
import entity.Service.Master.MasterData;
import entity.Service.Master.MasterEntity;
import entity.Service.MasterItem.MasterItemData;
import entity.Service.MasterItem.MasterItemEntity;
import entity.Service.Notification.NotificationEntity;
import entity.Service.Ordering.OrderingEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.BroadcastUtil;
import util.ServiceUtil;

public class ItemActivity extends Activity {

    private Integer itemAmount = 1;
    //private Double itemTotalPrice = 0.00;
    private Double itemPrice = 0.00;
    private String itemCurrency = "$";
    private String itemName = "";
    private Integer id = 0;
    private Integer itemID = 0;
    private String lookFor = "";
    private Boolean isMenuSet = false;
    private MasterItemData menuItem;
    private ItemExtraAdapter.OnSelectedChange serviceAdapter_onSelectedChange = new ItemExtraAdapter.OnSelectedChange() {
        @Override
        public void OnSelectedChange() {
            onCalculate();
        }
    };
    private Button.OnClickListener btnAmount_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.btnPlus && itemAmount < 9) {
                itemAmount = itemAmount + 1;
            } else if (view.getId() == R.id.btnMinus && itemAmount > 1) {
                itemAmount = itemAmount - 1;
            }
            onCalculate();
        }
    };
    private Button.OnClickListener btnClose_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ItemActivity.this.finish();
        }
    };
    private Button.OnClickListener btnZoom_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if ((findViewById(R.id.imgView)).getBackground() != null) {
                try {
                    Bitmap bitmap = ((BitmapDrawable) (findViewById(R.id.imgView)).getBackground()).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.WEBP, 80, stream);
                    Intent intent = new Intent(ItemActivity.this, ItemZoomActivity.class);
                    intent.putExtra("image", stream.toByteArray());
                    startActivity(intent);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    };

    ;
    private Button.OnClickListener toOrder_Event = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            //Init Data
            final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(ItemActivity.this, HotelInfoEntity.class);
            final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(ItemActivity.this, CheckLocationEntity.class);
            final VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(ItemActivity.this, VZACodeEntity.class);
            MasterEntity masterEntity = BaseApplication.GlobalVariable.get(ItemActivity.this, MasterEntity.class);

            final String site = hotelInfo.DATA.HOTEL_URL;
            final String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
            /*String vzacode = "";
            if (vzaCodeEntity != null) {
                vzacode = vzaCodeEntity.VZACODE.toString();
            } else {
                vzacode = BaseApplication.GlobalVariable.get(ItemActivity.this, CheckLoginEntity.class).VZACODE;
            }*/

            final String vzaCode = vzaCodeEntity.VZACODE =="" ? checkLocation.VZACODE : vzaCodeEntity.VZACODE;

            try {
                //-- Check Order Now or Add to cart
                if (view.getId() == R.id.btnOrder) {
                    //-- Check VZA Closed or Not
                    List<NameValuePair> closeParam = new ArrayList<>();
                    closeParam.add(new BasicNameValuePair("HOTEL_IDPara", hotelInfo.DATA.HOTEL_ID.toString()));
                    closeParam.add(new BasicNameValuePair("SYSTEMIDPara", String.valueOf(masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SYSTEMID)));
                    closeParam.add(new BasicNameValuePair("SENDORRECEIVECODEPara", vzaCode));
                    closeParam.add(new BasicNameValuePair("LOCATIONIDPara", checkLocation.LOCATION_ID.toString()));
                    closeParam.add(new BasicNameValuePair("LASTRECORDIDPara", "0"));

                    ServiceUtil closeSeviceUtil = new ServiceUtil(ItemActivity.this, site, keycode,
                            SiteOperate.NTFGetTransactionNotificationJSON, closeParam, NotificationEntity.class);
                    closeSeviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                            NotificationEntity notificationEntity = (NotificationEntity) results;
                            if (!notificationEntity.VZACLOSE) {
                                //Init CanOrder
                                final List<entity.Host.CanOrder.CanOrderData> canOrderData = new ArrayList<>();
                                canOrderData.add(new CanOrderData(menuItem.MENU_MENU_ID));

                                List<NameValuePair> canParam = new ArrayList<>();
                                canParam.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                                canParam.add(new BasicNameValuePair("TABLE_NOPara", checkLocation.TABLE_NO.toString()));
                                canParam.add(new BasicNameValuePair("VZA_CODEPara", vzaCode));
                                canParam.add(new BasicNameValuePair("ACCEPT_COVERPara", "0"));
                                canParam.add(new BasicNameValuePair("MENU_IDPara", new Gson().toJson(canOrderData)));

                                final ServiceUtil canSeviceUtil = new ServiceUtil(ItemActivity.this, site, keycode,
                                        SiteOperate.VZACheckCanOrderJSON, canParam, CanOrderEntity.class);
                                canSeviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                    @Override
                                    public void OnFinished(Object results) {
                                        final CanOrderEntity entity = (CanOrderEntity) results;
                                        String vzacode = "";
                                        if (entity.BILL_DISCOUNT) {
                                            ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getString(R.string.alert_order_discount), false);
                                            dialog.show(ItemActivity.this.getFragmentManager(), "tag");
                                            return;
                                        } else if (!entity.TABLE_OPEN) {
                                            ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getString(R.string.alert_table_open), false);
                                            dialog.show(ItemActivity.this.getFragmentManager(), "tag");
                                            return;
                                        } else if (entity.ISTABLE_TF) {
                                            checkLocation.TABLE_NO = entity.TABLE_NO_TF;
                                            vzacode = "T" + entity.TABLE_NO_TF;
                                            vzaCodeEntity.VZACODE = vzacode;
                                            checkLocation.VZACODE = vzacode;

                                            //-- Move Tables and change key notification
                                            String[] arrKey = BaseApplication.NotificationRecordVariable._key.split("_");
                                            String key = arrKey[0]+"_"+arrKey[1]+"_"+arrKey[2]+"_"+vzacode;
                                            BaseApplication.NotificationRecordVariable.setVariable(getBaseContext(), key);

                                            //-- Set value is change to global variable
                                            BaseApplication.GlobalVariable.set(checkLocation);
                                            BaseApplication.GlobalVariable.set(vzaCodeEntity);
                                        } else {
                                            vzacode = vzaCode;
                                        }
                                        final String VZACode = vzacode;
                                        if (entity.LOCATION_OPEN) {
                                            //Init Order Data
                                        /*final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, lisOrderData, isMenuSet);
                                        final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(ItemActivity.this, HotelInfoEntity.class);
                                        final VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(ItemActivity.this, VZACodeEntity.class);
                                        final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(ItemActivity.this, CheckLocationEntity.class);*/
                                            final UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(ItemActivity.this, UserInfoEntity.class);

                                            //Int Alert Order Box
                                            ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getString(R.string.alert_order_now), true);
                                            dialog.show(ItemActivity.this.getFragmentManager(), "this");
                                            dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                                                @Override
                                                public void OnSubmit(View view) {
                                                    if (entity.ITEM_SOLDOUTList.size() > 0) {
                                                        if (entity.ITEM_SOLDOUTList.get(0).CAN_ORDER) {
                                                            //Init Order Check
                                                            entity.Host.Cart.CheckOrderEntity menuOrders = new entity.Host.Cart.CheckOrderEntity();
                                                            for (int i = 0; i < itemAmount; i++) {
                                                                //menuOrders.CHECKORDER.add(new CheckOrderData(itemID));
                                                                menuOrders.CHECKORDER.add(new CheckOrderData(menuItem.MENU_MENU_ID));
                                                            }

                                                            //Call Service Check Order
                                                            List<NameValuePair> param = new ArrayList<>();
                                                            param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                                                            param.add(new BasicNameValuePair("MENU_IDPara", new Gson().toJson(menuOrders.CHECKORDER)));

                                                            final ServiceUtil serviceUtil = new ServiceUtil(ItemActivity.this, site, keycode, SiteOperate.VZAGetCheckOrderSTOPSOLDJSON,
                                                                    param, CheckOrderEntity.class);
                                                            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                                                @Override
                                                                public void OnFinished(Object results) {
                                                                    CheckOrderEntity orderEntity = (CheckOrderEntity) results;
                                                                    final List<entity.Service.CheckOrder.CheckOrderData> listCheck = new ArrayList<>(orderEntity.MENUList);
                                                                    Iterator<entity.Service.CheckOrder.CheckOrderData> menuList = listCheck.iterator();
                                                                    while (menuList.hasNext()) {
                                                                        if (menuList.next().CAN_ORDER)
                                                                            menuList.remove();
                                                                    }

                                                                    //Init Order
                                                                    List<MenuOrderData> lisOrderData = new ArrayList<>();
                                                                    List<ItemExtraModel> itemModels = ((ItemExtraAdapter) ((ViewList) findViewById(R.id.lisExtra)).getAdapter()).getItems();
                                                                    //Init Order Change

                                                                    if (!isMenuSet) {
                                                                        //Keep Comment && Addition
                                                                        List<String> lisComment = new ArrayList<>();
                                                                        List<MenuOrderDataAddition> lisAddition = new ArrayList<>();

                                                                        for (ItemExtraModel model : itemModels) {
                                                                            if (model.getGroup() == ItemExtraModel.GroupType.ADDITIONAL && model.getIsCheck()) {
                                                                                MenuOrderDataAddition orderAddition = new MenuOrderDataAddition();
                                                                                orderAddition.ADDITIONAL_ID = model.getId();
                                                                                orderAddition.ADDITIONAL_UNITPRICE = model.getUnitPrice();
                                                                                orderAddition.ORDER_ADDITIONAL_QTY = itemAmount;
                                                                                orderAddition.ADDITIONAL_NAME = model.getMessage();
                                                                                lisAddition.add(orderAddition);
                                                                            }
                                                                            if (model.getGroup() == ItemExtraModel.GroupType.CONDIMENT && model.getIsCheck()) {
                                                                                lisComment.add(model.getMessage());
                                                                            }
                                                                        }

                                                                        if (lookFor.equals("SubGroup") || isMenuSet) {
                                                                            lisOrderData.add(new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, menuItem.DisplayText, menuItem.MENU_SIZE, itemAmount.doubleValue(),
                                                                                    menuItem.CHARGE_UNITPIRCE, TextUtils.join(",", lisComment), lisAddition));
                                                                        } else {
                                                                            lisOrderData.add(new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, menuItem.DisplayText, menuItem.MENU_SIZE, itemAmount.doubleValue(),
                                                                                    menuItem.MENU_UNITPRICE, TextUtils.join(",", lisComment), lisAddition));
                                                                        }

                                                                    } else {
                                                                        //Check is Up Size
                                                                        Double priceUpSize = 0.00;
                                                                        Boolean keepUpSizePrize = false;
                                                                        List<MasterItemData.ItemUpSizeDetail> lisUpSizeItem = new ArrayList<>();
                                                                        for (ItemExtraModel model : itemModels) {
                                                                            if (model.getGroup() == ItemExtraModel.GroupType.UPSIZE && model.getType() == ItemExtraModel.ItemType.HEADER && model.getIsCheck()) {
                                                                                priceUpSize = model.getUnitPrice();
                                                                                for (ItemExtraModel itemModel : itemModels) {
                                                                                    if (itemModel.getGroup() == ItemExtraModel.GroupType.UPSIZE && itemModel.getType() == ItemExtraModel.ItemType.DETAIL) {
                                                                                        lisUpSizeItem.add((MasterItemData.ItemUpSizeDetail) itemModel.getEntity());
                                                                                    }
                                                                                }
                                                                                break;
                                                                            }
                                                                        }

                                                                        for (ItemExtraModel model : itemModels) {
                                                                            if (model.getType() == ItemExtraModel.ItemType.DETAIL && model.getIsCheck()) {
                                                                                MenuOrderData orderData;
                                                                                MasterItemData.MenuPromotionDetail menuDetail = ((MasterItemData.MenuPromotionDetail) model.getEntity());
                                                                                orderData = new MenuOrderData(menuItem.MENU_MENU_ID, menuDetail.MENU_CODE, menuDetail.MENU_NAME1, menuDetail.MENU_SIZE,
                                                                                        itemAmount.doubleValue(), menuDetail.CHARGE_UNITPIRCE, "", new ArrayList<MenuOrderDataAddition>());
                                                                                if (model.getGroup() == ItemExtraModel.GroupType.SELECT) {
                                                                                    for (MasterItemData.ItemUpSizeDetail itemUpSizeDetail : lisUpSizeItem) {
                                                                                        if (itemUpSizeDetail.ITEM_UPSIZE_CODE.equals(menuDetail.ITEM_UPSIZE_CODE)) {
                                                                                            orderData.MENU_CODE = menuDetail.ITEM_UPSIZE_CODE;
                                                                                            orderData.MENU_NAME = menuDetail.ITEM_UPSIZE_NAME1;
                                                                                            if (!keepUpSizePrize) {
                                                                                                keepUpSizePrize = true;
                                                                                                orderData.MENU_UNITPRICE = orderData.MENU_UNITPRICE + priceUpSize;  //orderData.MENU_UNITPRICE = priceUpSize;
                                                                                            }
                                                                                            break;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                lisOrderData.add(orderData);
                                                                            }
                                                                        }
                                                                    }

                                                                    //Init Order Data
                                                                    final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, itemPrice, lisOrderData, isMenuSet);
                                                                    final UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(ItemActivity.this, UserInfoEntity.class);

                                                                    //Int Alert Order Box
                                                                    if (!listCheck.isEmpty()) {
                                                                        ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getString(R.string.alert_cant_order), false);
                                                                        dialog.show(ItemActivity.this.getFragmentManager(), "tag");
                                                                        return;
                                                                    } else {
                                                                        List<NameValuePair> param = new ArrayList<>();
                                                                        param.add(new BasicNameValuePair("VZACODEPara", VZACode));
                                                                        param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                                                                        param.add(new BasicNameValuePair("TELPara", userInfoEntity.Data.TELEPHONE));
                                                                        param.add(new BasicNameValuePair("MENUORDERITEMWITHJSONFORMATPara", new Gson().toJson(orderDataEntity.ORDER)));

                                                                        String site = hotelInfo.DATA.HOTEL_URL;
                                                                        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                                                                        ServiceUtil serviceUtil = new ServiceUtil(ItemActivity.this, site, keycode,
                                                                                SiteOperate.POSSaveTrnOrderingJSON, param, OrderingEntity.class);
                                                                        serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                                                            @Override
                                                                            public void OnFinished(Object results) {
                                                                                BaseApplication.RequestCheckVariable.set(false);
                                                                                ItemActivity.this.finish();
                                                                            }
                                                                        });
                                                                        serviceUtil.execute();
                                                                    }
                                                                }
                                                            });
                                                            serviceUtil.execute();
                                                        } else {
                                                            ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getString(R.string.alert_cant_order), false);
                                                            dialog.show(ItemActivity.this.getFragmentManager(), "tag");
                                                            return;
                                                        }

                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                                canSeviceUtil.execute();
                            } else {
                                ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getString(R.string.alert_vzacode_closed), false);
                                dialog.show(ItemActivity.this.getFragmentManager(), "tag");
                                return;
                            }
                        }
                    });
                    closeSeviceUtil.execute();
                } else {
                    //Init Add to cart
                    entity.Host.Cart.CheckOrderEntity menuOrders = new entity.Host.Cart.CheckOrderEntity();
                    for (int i = 0; i < itemAmount; i++) {
                        menuOrders.CHECKORDER.add(new CheckOrderData(itemID));
                    }

                    //Call Service Check Order
                    List<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                    param.add(new BasicNameValuePair("MENU_IDPara", new Gson().toJson(menuOrders.CHECKORDER)));

                    final ServiceUtil serviceUtil = new ServiceUtil(ItemActivity.this, site, keycode, SiteOperate.VZAGetCheckOrderSTOPSOLDJSON,
                            param, CheckOrderEntity.class);
                    serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                            CheckOrderEntity orderEntity = (CheckOrderEntity) results;
                            List<entity.Service.CheckOrder.CheckOrderData> listCheck = new ArrayList<>(orderEntity.MENUList);
                            Iterator<entity.Service.CheckOrder.CheckOrderData> menuList = listCheck.iterator();
                            while (menuList.hasNext()) {
                                if (menuList.next().CAN_ORDER)
                                    menuList.remove();
                            }

                            //Init Order
                            List<MenuOrderData> lisOrderData = new ArrayList<>();
                            List<ItemExtraModel> itemModels = ((ItemExtraAdapter) ((ViewList) findViewById(R.id.lisExtra)).getAdapter()).getItems();

                            //Init Order Change
                            MenuOrderData orderChange;
                            List<MenuOrderData> lisOrderChange = new ArrayList<>();
                            String nameChange = "";
                            Double priceChange = 0.00;

                            if (!listCheck.isEmpty()) {
                                ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getString(R.string.alert_cant_order), false);
                                dialog.show(ItemActivity.this.getFragmentManager(), "tag");
                                return;
                            } else {
                                if (!isMenuSet) {
                                    //Keep Comment && Addition
                                    List<String> lisComment = new ArrayList<>();
                                    List<MenuOrderDataAddition> lisAddition = new ArrayList<>();

                                    for (ItemExtraModel model : itemModels) {
                                        if (model.getGroup() == ItemExtraModel.GroupType.ADDITIONAL && model.getIsCheck()) {
                                            MenuOrderDataAddition orderAddition = new MenuOrderDataAddition();
                                            orderAddition.ADDITIONAL_ID = model.getId();
                                            orderAddition.ADDITIONAL_UNITPRICE = model.getUnitPrice();
                                            orderAddition.ORDER_ADDITIONAL_QTY = itemAmount;
                                            orderAddition.ADDITIONAL_NAME = model.getMessage();
                                            lisAddition.add(orderAddition);
                                        }
                                        if (model.getGroup() == ItemExtraModel.GroupType.CONDIMENT && model.getIsCheck()) {
                                            lisComment.add(model.getMessage());
                                        }
                                    }

                                    if (lookFor.equals("SubGroup") || isMenuSet) {
                                        lisOrderData.add(new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, menuItem.DisplayText, menuItem.MENU_SIZE, itemAmount.doubleValue(),
                                                menuItem.CHARGE_UNITPIRCE, TextUtils.join(",", lisComment), lisAddition));
                                    } else {
                                        lisOrderData.add(new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, menuItem.DisplayText, menuItem.MENU_SIZE, itemAmount.doubleValue(),
                                                menuItem.MENU_UNITPRICE, TextUtils.join(",", lisComment), lisAddition));
                                    }

                                } else {
                                    //Check is Up Size
                                    Double priceUpSize = 0.00;
                                    Boolean keepUpSizePrize = false;
                                    List<MasterItemData.ItemUpSizeDetail> lisUpSizeItem = new ArrayList<>();
                                    for (ItemExtraModel model : itemModels) {
                                        if (model.getGroup() == ItemExtraModel.GroupType.UPSIZE && model.getType() == ItemExtraModel.ItemType.HEADER && model.getIsCheck()) {
                                            priceUpSize = model.getUnitPrice();
                                            for (ItemExtraModel itemModel : itemModels) {
                                                if (itemModel.getGroup() == ItemExtraModel.GroupType.UPSIZE && itemModel.getType() == ItemExtraModel.ItemType.DETAIL) {
                                                    lisUpSizeItem.add((MasterItemData.ItemUpSizeDetail) itemModel.getEntity());
                                                }
                                            }
                                            break;
                                        }
                                    }

                                    for (ItemExtraModel model : itemModels) {
                                        if (model.getType() == ItemExtraModel.ItemType.DETAIL && model.getIsCheck()) {
                                            MenuOrderData orderData;
                                            MasterItemData.MenuPromotionDetail menuDetail = ((MasterItemData.MenuPromotionDetail) model.getEntity());
                                            orderData = new MenuOrderData(menuItem.MENU_MENU_ID, menuDetail.MENU_CODE, menuDetail.MENU_NAME1,
                                                    menuDetail.MENU_SIZE, itemAmount.doubleValue(), menuDetail.CHARGE_UNITPIRCE, "",
                                                    new ArrayList<MenuOrderDataAddition>());
                                            if (model.getGroup() == ItemExtraModel.GroupType.SELECT) {
                                                for (MasterItemData.ItemUpSizeDetail itemUpSizeDetail : lisUpSizeItem) {
                                                    if (itemUpSizeDetail.ITEM_UPSIZE_CODE.equals(menuDetail.ITEM_UPSIZE_CODE)) {
                                                        orderData.MENU_CODE = menuDetail.ITEM_UPSIZE_CODE;
                                                        orderData.MENU_NAME = menuDetail.ITEM_UPSIZE_NAME1;
                                                        if (!keepUpSizePrize) {
                                                            keepUpSizePrize = true;
                                                            orderData.MENU_UNITPRICE = orderData.MENU_UNITPRICE + priceUpSize; //orderData.MENU_UNITPRICE = priceUpSize;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }

                                            //-- CHANGE ORDER
                                            if (model.getGroup() == ItemExtraModel.GroupType.CHANGE) {
                                                nameChange = menuDetail.MENU_NAME1;
                                                priceChange = menuDetail.CHARGE_UNITPIRCE;
                                                /*orderChange = new MenuOrderData(menuItem.MENU_MENU_ID, menuDetail.MENU_CODE, menuDetail.MENU_NAME1,
                                                        menuDetail.MENU_SIZE, itemAmount.doubleValue(), menuDetail.CHARGE_UNITPIRCE, "",
                                                        new ArrayList<MenuOrderDataAddition>());*/
                                                lisOrderChange.add(orderData);
                                                break;
                                            }else{
                                                lisOrderData.add(orderData);
                                            }
                                        }
                                    }
                                }

                                //Init Order Data
                                //final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, lisOrderData, isMenuSet);
                                final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, itemPrice, lisOrderData, isMenuSet);

                                /*
                                final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(ItemActivity.this, HotelInfoEntity.class);
                                final VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(ItemActivity.this, VZACodeEntity.class);
                                final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(ItemActivity.this, CheckLocationEntity.class);
                                final UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(ItemActivity.this, UserInfoEntity.class);
                                */

                                //-- Add to cart
                                //BaseApplication.CartStateVariable.add(new MenuOrderEntity(itemName, orderDataEntity.ORDER, isMenuSet));
                                BaseApplication.CartStateVariable.add(new MenuOrderEntity(itemName, itemPrice, orderDataEntity.ORDER, isMenuSet));
                                if(nameChange != ""){
                                    BaseApplication.CartStateVariable.add(new MenuOrderEntity(nameChange, priceChange, lisOrderChange, false));
                                }
                                ItemActivity.this.finish();
                            }
                        }
                    });
                    serviceUtil.execute();
                }
            }catch (Exception ex){
                ChooseDialog dialog = ChooseDialog.newInstance(ItemActivity.this, getResources().getString(R.string.alert_default), false);
                dialog.show(getFragmentManager(), "tag");
                return;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item);

        //Init Extras
        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("itemID"); //itemID = bundle.getInt("itemID");
        isMenuSet = bundle.getBoolean("isMenuSet");
        lookFor = bundle.getString("lookFor") != null ? bundle.getString("lookFor") : "";

        if (!bundle.getString("favItem").isEmpty()) {
            menuItem = new Gson().fromJson(bundle.getString("favItem"), MasterItemData.class);
        } else {
            menuItem = BaseApplication.GlobalVariable.get(this, MasterItemEntity.class).FindByID(id); //menuItem = BaseApplication.GlobalVariable.get(this, MasterItemEntity.class).FindByID(itemID);
        }

        if (lookFor.equals("SubGroup")) {  //if (lookFor.equals("SubGroup") || isMenuSet) {
            itemPrice = menuItem.CHARGE_UNITPIRCE;
        } else {
            itemPrice = menuItem.MENU_UNITPRICE;
        }
        itemID = menuItem.MENU_MENU_ID;
        itemCurrency = menuItem.CURRENCY_DEP;
        itemName = menuItem.MENU_NAME1;

        //Set View
        ((AnimateView) findViewById(R.id.imgView)).setImageService(menuItem.MENU_IMAGENAME);

        //Set To Adapter
        List<ItemExtraModel> itemExtra = new ArrayList<>();

        //Add CONDIMENT
        if (!menuItem.CONDIMENT_DETAIL.isEmpty()) {
            itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.CONDIMENT).setHeader(getResources().getString(R.string.txt_condiments), false));
            for (MasterItemData.CondimentDetail conDetail : menuItem.CONDIMENT_DETAIL) {
                itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.CONDIMENT).setDetail(conDetail.CONDIMENT_ID,
                        conDetail.CONDIMENT_NAME1,
                        true,
                        false,
                        conDetail));
            }
        }

        //Add ADDITIONAL
        if (!menuItem.ADDITIONAL_DETAIL.isEmpty()) {
            itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.ADDITIONAL).setHeader(getResources().getString(R.string.txt_additional), false));
            for (MasterItemData.AdditionalDetail addDetail : menuItem.ADDITIONAL_DETAIL) {
                itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.ADDITIONAL).setDetail(addDetail.ADDITIONAL_ID,
                        addDetail.ADDITIONAL_NAME1,
                        menuItem.CURRENCY_DEP,
                        addDetail.ADDITIONAL_UNITPRICE,
                        true,
                        false,
                        addDetail));
            }
        }

        //Add SELECT
        if (!menuItem.SELECT_DETAIL.isEmpty()) {
            for (MasterItemData.SelectDetail selDetail : menuItem.SELECT_DETAIL) {
                itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.SELECT).setHeader(selDetail.GROUPSELECT_NAME, false));
                //Select Quota
                Integer Quota = selDetail.ITEM_LIMITQTY.intValue();
                for (MasterItemData.MenuPromotionDetail promotionDetail : selDetail.MENUPROMOTION_DETAIL) {
                    boolean selQuota = false;
                    if (Quota > 0) {
                        selQuota = true;
                        Quota--;
                    }
                    itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.SELECT, selDetail.hashCode(), selDetail.ITEM_LIMITQTY.intValue()).setDetail(promotionDetail.MENU_MENU_ID,
                            promotionDetail.MENU_NAME1,
                            promotionDetail.CURRENCY_DEP,
                            promotionDetail.CHARGE_UNITPIRCE,
                            true,
                            selQuota,
                            promotionDetail));
                }
            }
        }

        //Add FIX
        if (!menuItem.FIX_DETAIL.isEmpty()) {
            for (MasterItemData.FixDetail fixDetail : menuItem.FIX_DETAIL) {
                itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.FIX).setHeader(fixDetail.GROUPSELECT_NAME, false));
                for (MasterItemData.MenuPromotionDetail promotionDetail : fixDetail.MENUPROMOTION_DETAIL) {
                    itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.FIX, fixDetail.hashCode(), fixDetail.ITEM_LIMITQTY.intValue()).setDetail(promotionDetail.MENU_MENU_ID,
                            promotionDetail.MENU_NAME1,
                            promotionDetail.CURRENCY_DEP,
                            promotionDetail.CHARGE_UNITPIRCE,
                            true,
                            true,
                            promotionDetail));
                }
            }
        }

        //Add CHANGE
        if (!menuItem.CHANGE_DETAIL.isEmpty()) {
            for (MasterItemData.ChangeDetail changeDetail : menuItem.CHANGE_DETAIL) {
                itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.CHANGE).setHeader(changeDetail.GROUPSELECT_NAME, false));
                for (MasterItemData.MenuPromotionDetail promotionDetail : changeDetail.MENUPROMOTION_DETAIL) {
                    itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.CHANGE, changeDetail.hashCode(), changeDetail.ITEM_LIMITQTY.intValue()).setDetail(promotionDetail.MENU_MENU_ID,
                            promotionDetail.MENU_NAME1,
                            promotionDetail.CURRENCY_DEP,
                            promotionDetail.CHARGE_UNITPIRCE,
                            true,
                            false,
                            promotionDetail));
                }
            }
        }

        //Add UPSIZE
        if (!menuItem.UPSIZE_DETAIL.isEmpty()) {
            for (MasterItemData.UpSizeDetail upSizeDetail : menuItem.UPSIZE_DETAIL) {
                itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.UPSIZE).setHeader(getResources().getString(R.string.txt_upsize), menuItem.CURRENCY_DEP, upSizeDetail.UPSIZE_CHARGE, true));
                for (MasterItemData.ItemUpSizeDetail itemUpSizeDetail : upSizeDetail.ITEMUPSIZE_DETAIL) {
                    itemExtra.add(new ItemExtraModel(ItemExtraModel.GroupType.UPSIZE).setDetail(itemUpSizeDetail.ITEM_UPSIZE_ID, itemUpSizeDetail.ITEM_UPSIZE_NAME1, false, false, itemUpSizeDetail));
                }
            }
        }

        if (itemExtra.isEmpty()) {
            //Hidden View
            LinearLayout frmMode = (LinearLayout) findViewById(R.id.frmMode);
            frmMode.setVisibility(View.GONE);
        }

        ItemExtraAdapter serviceAdapter = new ItemExtraAdapter(this, R.id.txtMessage, itemExtra);
        serviceAdapter.setOnSelectedChangeListener(serviceAdapter_onSelectedChange);
        ViewList lisView = (ViewList) findViewById(R.id.lisExtra);
        lisView.setAdapter(serviceAdapter);

        //On Calculate
        ((TextView) findViewById(R.id.txtTitle)).setText(itemName);
        ((TextView) findViewById(R.id.txtAmount)).setText(itemAmount.toString());
        onCalculate();

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);
        findViewById(R.id.btnPlus).setOnClickListener(btnAmount_OnClick);
        findViewById(R.id.btnMinus).setOnClickListener(btnAmount_OnClick);
        findViewById(R.id.btnZoom).setOnClickListener(btnZoom_OnClick);
        findViewById(R.id.btnOrder).setOnClickListener(toOrder_Event);
        findViewById(R.id.btnTocart).setOnClickListener(toOrder_Event);
    }

    private void onCalculate() {
        double additionPrice = 0.00;
        List<ItemExtraModel> itemModels = ((ItemExtraAdapter) ((ViewList) findViewById(R.id.lisExtra)).getAdapter()).getItems();
        for (ItemExtraModel model : itemModels) {
            //Init Addition
            if (model.getHasCheck() && model.getIsCheck()) {
                if (model.getGroup() == ItemExtraModel.GroupType.ADDITIONAL ||
                        model.getGroup() == ItemExtraModel.GroupType.FIX ||
                        model.getGroup() == ItemExtraModel.GroupType.CHANGE ||
                        model.getGroup() == ItemExtraModel.GroupType.UPSIZE ||
                        model.getGroup() == ItemExtraModel.GroupType.SELECT) {
                    additionPrice = additionPrice + model.getUnitPrice();
                }
            }
        }
        //itemTotalPrice = additionPrice + itemPrice;
        ((TextView) findViewById(R.id.txtPrice)).setText(itemCurrency + new DecimalFormat("#,##0.00").format((additionPrice + itemPrice) * itemAmount));
        ((TextView) findViewById(R.id.txtAmount)).setText(itemAmount.toString());
    }
}
