package com.smartfinder.vzaapp.Menu;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.MenuList.MenuItemAdapter;
import control.MenuList.MenuList;
import control.MenuView.MenuView;
import control.ViewList.ViewList;
import emuntype.SiteOperate;
import entity.Host.Cart.CheckOrderData;
import entity.Host.Cart.MenuOrderData;
import entity.Host.Cart.MenuOrderDataAddition;
import entity.Host.Cart.MenuOrderEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.CheckOrder.CheckOrderEntity;
import entity.Service.Master.MasterDetailList;
import entity.Service.Master.MasterEntity;
import entity.Service.MasterItem.MasterItemData;
import entity.Service.MasterItem.MasterItemEntity;
import entity.Service.Ordering.OrderingEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

public class ItemGroupFragment extends Fragment {

    // Declare Variables
    MenuItemAdapter serviceAdapter;
    MenuList lisMenu;

    public static final ItemGroupFragment newInstance(Integer groupID,String lookFor){
        ItemGroupFragment fragment = new ItemGroupFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("groupID", groupID);
        bundle.putString("lookFor", lookFor);
        bundle.putInt("subGroupID", groupID);
        bundle.putBoolean("isInstance", true);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static final ItemGroupFragment newInstance(Integer groupID,String lookFor,Integer subGroupID){
        ItemGroupFragment fragment = new ItemGroupFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("groupID", groupID);
        bundle.putString("lookFor", lookFor);
        bundle.putInt("subGroupID", subGroupID);
        bundle.putBoolean("isInstance",true);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        //Set Default
        return inflater.inflate(R.layout.fragment_menu_itemgroup,container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        //--Init
        final EditText txtSearch = (EditText)getActivity().findViewById(R.id.txtSearch);

        //Init Event
        ((RadioGroup) getActivity().findViewById(R.id.rgpList)).setOnCheckedChangeListener(rgpList_OnCheckedChange);
        getActivity().findViewById(R.id.btnListView).setOnClickListener(grpList_OnClick);
        getActivity().findViewById(R.id.btnGridView).setOnClickListener(grpList_OnClick);

        //--Event Search text
        txtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.e(getTag(), s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                ((LinearLayout)getActivity().findViewById(R.id.frmTopPanel)).setVisibility(View.GONE);
                serviceAdapter.getFilter().filter(s);
            }
        });
        txtSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Button) getActivity().findViewById(R.id.btn_cancel_search)).setVisibility(View.VISIBLE);

                //-- Keyboard Showing
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_IMPLICIT);

                /*View vLayout =  (View) getActivity().findViewById(R.id.vLayout);
                //vLayout.setBackgroundColor(Color.TRANSPARENT);
                vLayout.setAlpha(0.5f);
                vLayout.setTop(lisMenu.getTop());
                vLayout.setVisibility(View.VISIBLE);*/

                ((LinearLayout)getActivity().findViewById(R.id.frmTopPanel)).setVisibility(View.VISIBLE);
                //((LinearLayout)getActivity().findViewById(R.id.frmTopPanel)).setBackgroundColor(Color.argb (0, 0, 255, 0));

            }
        });
        txtSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ((Button) getActivity().findViewById(R.id.btn_cancel_search)).setVisibility(View.VISIBLE);

                    //-- Keyboard Showing
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_IMPLICIT);

                    /*View vLayout =  (View) getActivity().findViewById(R.id.vLayout);
                    //vLayout.setBackgroundColor(Color.TRANSPARENT);
                    vLayout.setAlpha(0.5f);
                    vLayout.setTop(lisMenu.getTop());
                    vLayout.setVisibility(View.VISIBLE);*/

                    ((LinearLayout)getActivity().findViewById(R.id.frmTopPanel)).setVisibility(View.VISIBLE);
                    //((LinearLayout)getActivity().findViewById(R.id.frmTopPanel)).setBackgroundColor(Color.argb(0, 0, 255, 0));
                }
                else
                {
                    ((Button) getActivity().findViewById(R.id.btn_cancel_search)).setVisibility(View.GONE);
                }
            }
        });
        //-- Event Click Cancel
        ((Button)getActivity().findViewById(R.id.btn_cancel_search)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSearch.setText("");
                ((Button) getActivity().findViewById(R.id.btn_cancel_search)).setVisibility(View.GONE);
                //-- Keyboard Hide
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                //((View) getActivity().findViewById(R.id.vLayout)).setVisibility(View.GONE);
                ((LinearLayout)getActivity().findViewById(R.id.frmTopPanel)).setVisibility(View.GONE);

            }
        });

         //Init Data
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),MasterEntity.class);
        UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),UserInfoEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),CheckLocationEntity.class);
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),HotelInfoEntity.class);
        MasterDetailList masterDetailList = masterData.FindByLocationID(checkLocation.LOCATION_ID).MenuDetail.FindByID(getArguments().getInt("groupID"),
                getArguments().getString("lookFor"));

        //Get MasterItem
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("GUEST_TYPEPara",userInfoEntity.Data.GUEST_TYPE.toString()));
        param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
        param.add(new BasicNameValuePair("MENU_GROUP_CODEPara",masterDetailList.MENU_GROUP_CODE));
        param.add(new BasicNameValuePair("MENU_SUBMENU_IDPara",masterDetailList.FindByID(getArguments().getInt("subGroupID")).MENU_SUBMENU_ID.toString()));
        param.add(new BasicNameValuePair("LOOKFORPara",masterDetailList.FindByID(getArguments().getInt("subGroupID")).PROMOTIONSET_LOOKFOR_ID.toString()));

        String site = hotelInfo.DATA.HOTEL_URL;
        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
        ServiceUtil masterItemEntity = new ServiceUtil(ItemGroupFragment.this.getActivity(), site, keycode,
                SiteOperate.VZAgetMasterItemDataJSON, param, MasterItemEntity.class);

        if(getArguments().getBoolean("isInstance")){
            getArguments().putBoolean("isInstance",false);
            masterItemEntity.execute();
        }else{
            if(((RadioGroup)getActivity().findViewById(R.id.rgpList)).getCheckedRadioButtonId() < 0){
                getActivity().findViewById(R.id.btnListView).performClick();
            }
        }

        masterItemEntity.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                BaseApplication.GlobalVariable.set((MasterItemEntity)results);
                try {
                    getActivity().findViewById(R.id.btnListView).performClick();

                    //Get Current Page
                    MenuView menuView = (MenuView) getActivity().findViewById(R.id.frmFragment);
                    menuView.setCurrentItem(menuView.getAdapter().getCount() - 1, true);
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        });

    }

    public Button.OnClickListener grpList_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toggle Button
            ((RadioGroup)view.getParent()).check(view.getId());

            //set To Adapter
            Collection<MasterItemData> menuItem = BaseApplication.GlobalVariable.get(getActivity(),MasterItemEntity.class).MenuList;

            //MenuItemAdapter serviceAdapter;
            //MenuList lisMenu;
            if(((RadioGroup)view.getParent()).getCheckedRadioButtonId() == R.id.btnListView){
                serviceAdapter = new MenuItemAdapter(getActivity(),new ArrayList<>(menuItem),R.layout.adapter_menu_itemgroup_list,getArguments().getString("lookFor"));
                lisMenu = (MenuList) getActivity().findViewById(R.id.lisItem);
                lisMenu.setNumColumns(getResources().getInteger(R.integer.MENULIST_LISTMODE_COLSIZE));
                lisMenu.setRowSize(getResources().getDimension(R.dimen.MENULIST_LISTMODE_ROWSIZE));
                lisMenu.setAdapter(serviceAdapter);
            }else{
                serviceAdapter = new MenuItemAdapter(getActivity(),new ArrayList<>(menuItem),R.layout.adapter_menu_itemgroup_grid,getArguments().getString("lookFor"));
                lisMenu = (MenuList) getActivity().findViewById(R.id.lisItem);
                lisMenu.setNumColumns(getResources().getInteger(R.integer.MENULIST_GRIDMODE_COLSIZE));
                lisMenu.setRowSize(getResources().getDimension(R.dimen.MENULIST_GRIDMODE_ROWSIZE));
                lisMenu.setAdapter(serviceAdapter);
            }
            lisMenu.setTextFilterEnabled(true);
            serviceAdapter.setOnItemClickListener(onToCartClick);
            serviceAdapter.setOnItemClickListener(onItemClick);
        }
    };

    private MenuItemAdapter.OnItemClick onItemClick = new MenuItemAdapter.OnItemClick() {
        @Override
        public void OnItemClick(MasterItemData model) {
            //Init Menu
            Intent intent = new Intent(getActivity(),ItemActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("itemID", model.ID);
            bundle.putBoolean("isMenuSet",false);
            bundle.putString("lookFor",getArguments().getString("lookFor"));
            bundle.putString("favItem","");
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    private MenuItemAdapter.OnToCartClick onToCartClick = new MenuItemAdapter.OnToCartClick() {
        @Override
        public void OnToCartClick(final MasterItemData model) {
            //Init Order Check
            entity.Host.Cart.CheckOrderEntity menuOrders = new entity.Host.Cart.CheckOrderEntity();
            menuOrders.CHECKORDER.add(new CheckOrderData(model.MENU_MENU_ID)); //menuOrders.CHECKORDER.add(new CheckOrderData(model.ID));

            //Init Data
            HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity(),HotelInfoEntity.class);
            CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity(),CheckLocationEntity.class);

            //Call Service Check Order
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
            param.add(new BasicNameValuePair("MENU_IDPara",new Gson().toJson(menuOrders.CHECKORDER)));

            String site = hotelInfo.DATA.HOTEL_URL;
            String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
            final ServiceUtil serviceUtil = new ServiceUtil(getActivity(),site,keycode, SiteOperate.VZAGetCheckOrderSTOPSOLDJSON,
                    param, CheckOrderEntity.class);
            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    CheckOrderEntity orderEntity = (CheckOrderEntity) results;
                    List<entity.Service.CheckOrder.CheckOrderData> listCheck = new ArrayList<>(orderEntity.MENUList);
                    Iterator<entity.Service.CheckOrder.CheckOrderData> menuList = listCheck.iterator();
                    while (menuList.hasNext()){
                        if(menuList.next().CAN_ORDER)
                            menuList.remove();
                    }

                    if(!listCheck.isEmpty()){
                        ChooseDialog dialog = ChooseDialog.newInstance(getActivity(),getString(R.string.alert_cant_order), false);
                        dialog.show(getActivity().getFragmentManager(), "tag");
                        return;
                    }else{
                        MasterItemData menuItem = BaseApplication.GlobalVariable.get(getActivity(),MasterItemEntity.class).FindByID(model.ID);
                        MenuOrderData OrderItem;
                        Double itemPrice = 0.0;
                        if(getArguments().getString("lookFor").equals("SubGroup")) {
                            itemPrice = menuItem.CHARGE_UNITPIRCE;
                            OrderItem = new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, menuItem.MENU_NAME1, menuItem.MENU_SIZE, 1.00,
                                    menuItem.CHARGE_UNITPIRCE, "", new ArrayList<MenuOrderDataAddition>());
                        }else{
                            itemPrice = menuItem.MENU_UNITPRICE;
                            OrderItem = new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, menuItem.MENU_NAME1, menuItem.MENU_SIZE, 1.00,
                                    menuItem.MENU_UNITPRICE, "", new ArrayList<MenuOrderDataAddition>());
                        }
                        //BaseApplication.CartStateVariable.add(new MenuOrderEntity(menuItem.MENU_NAME1,OrderItem));
                        BaseApplication.CartStateVariable.add(new MenuOrderEntity(menuItem.MENU_NAME1, itemPrice, OrderItem));
                    }
                }
            });
            serviceUtil.execute();
        }
    };

    private RadioGroup.OnCheckedChangeListener rgpList_OnCheckedChange = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            for (int j = 0; j < group.getChildCount(); j++) {
                final Button view = (Button) group.getChildAt(j);
                view.setSelected(view.getId() == checkedId);
            }
        }
    };

}
