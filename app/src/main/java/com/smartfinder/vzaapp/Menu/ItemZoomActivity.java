package com.smartfinder.vzaapp.Menu;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.smartfinder.vzaapp.R;

import control.ZoomImageView.ZoomImageView;

public class ItemZoomActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_itemzoom);

        ZoomImageView view = ((ZoomImageView)findViewById(R.id.imgView));
        try {
            view.setImageBitmap(BitmapFactory.decodeByteArray(this.getIntent().getByteArrayExtra("image"), 0, this.getIntent().getByteArrayExtra("image").length));
        }catch (Exception e){
            e.printStackTrace();
        }

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);
    }

    Button.OnClickListener btnClose_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ItemZoomActivity.this.finish();
        }
    };
}
