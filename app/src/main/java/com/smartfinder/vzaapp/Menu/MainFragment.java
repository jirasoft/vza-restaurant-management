package com.smartfinder.vzaapp.Menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.AdsMenuActivity;
import com.smartfinder.vzaapp.BaseApplication;
//import com.smartfinder.vzaapp.GuestActivity;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.MenuView.MenuView;
import control.View.AnimateView;
import emuntype.SiteOperate;
import entity.Host.Request.RequestData;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.Master.ServiceDetailList;
import entity.Service.Request.RequestEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

public class MainFragment extends Fragment {

    public static final MainFragment newInstance()
    {
        MainFragment fragment = new MainFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_menu_main,container, false);

        //Set Location Image Service
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),MasterEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),CheckLocationEntity.class);

        //Set View
        String urlLocationImg = masterData.FindByLocationID(checkLocation.LOCATION_ID).IMAGENAME;
        ((AnimateView)view.findViewById(R.id.imgLocation)).setImageService(urlLocationImg);

        //Init Event
        view.findViewById(R.id.btnMenu).setOnClickListener(btnMenu_OnClick);
        view.findViewById(R.id.btnRequest).setOnClickListener(btnRequest_OnClick);
        view.findViewById(R.id.btnCheck).setOnClickListener(btnCheck_OnClick);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //Set Check Button
        if(BaseApplication.RequestCheckVariable.get()){
            ((Button)getActivity().findViewById(R.id.btnCheck)).setText(getActivity().getString(R.string.btn_wait));
            getActivity().findViewById(R.id.btnCheck).setClickable(false);
        }else{
            ((Button)getActivity().findViewById(R.id.btnCheck)).setText(getActivity().getString(R.string.btn_check));
            getActivity().findViewById(R.id.btnCheck).setClickable(true);
        }
    }

    public Button.OnClickListener btnMenu_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Init Menu
            MenuView menuView = (MenuView)getActivity().findViewById(R.id.frmFragment);
            ((AdsMenuActivity)getActivity()).menuViewAdapter.addItem(menuView, GroupFragment.newInstance());
        }
    };

    public Button.OnClickListener btnRequest_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Init Data
            MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), MasterEntity.class);
            CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),CheckLocationEntity.class);
            List<ServiceDetailList> serviceDetailList = new ArrayList<>(masterData.FindByLocationID(checkLocation.LOCATION_ID).ServiceDetail.ServiceItemList);

            if(serviceDetailList.size() > 0) {
                MenuView menuView = (MenuView) getActivity().findViewById(R.id.frmFragment);
                ((AdsMenuActivity) getActivity()).menuViewAdapter.addItem(menuView, RequestFragment.newInstance());
            }else{
                ChooseDialog dialog = ChooseDialog.newInstance(getActivity(),getResources().getString(R.string.alert_request_empty), false);
                dialog.show(getActivity().getFragmentManager(), "tag");
                return;
            }
        }
    };

    public Button.OnClickListener btnCheck_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ChooseDialog dialog = ChooseDialog.newInstance(getActivity(),getResources().getString(R.string.alert_check_bill), true);
            dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                @Override
                public void OnSubmit(View view) {

                    final VZACodeEntity vzaCode = BaseApplication.GlobalVariable.get(getActivity(),VZACodeEntity.class);
                    final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity(),CheckLocationEntity.class);
                    final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity(),HotelInfoEntity.class);

                    List<NameValuePair> param = new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
                    param.add(new BasicNameValuePair("VZA_CODEPara",vzaCode.VZACODE));
                    String site = hotelInfo.DATA.HOTEL_URL;
                    String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                    ServiceUtil checkLocationService = new ServiceUtil(getActivity(),site,keycode, SiteOperate.VZACheckLocationOpenedJSON,param, CheckLocationEntity.class);
                    checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                            if(((CheckLocationEntity) results).RESULT){
                                //Keep checkLocation To AppVariable
                                BaseApplication.GlobalVariable.set((CheckLocationEntity) results);
                                MasterEntity masterEntity = BaseApplication.GlobalVariable.get(getActivity(),MasterEntity.class);
                                List<NameValuePair> param = new ArrayList<>();
                                param.add(new BasicNameValuePair("LOCATIONPara",checkLocation.LOCATION_ID.toString()));
                                param.add(new BasicNameValuePair("SYSTEMID",masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SYSTEMID.toString()));
                                param.add(new BasicNameValuePair("VZACODEPara",vzaCode.VZACODE));
                                param.add(new BasicNameValuePair("REFNO",""));
                                param.add(new BasicNameValuePair("TEXTMSG",""));

                                ArrayList<RequestData> requestData = new ArrayList<>();
                                requestData.add(new RequestData(masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SERVICE_ID));
                                param.add(new BasicNameValuePair("REQTYPEIDJSON", new Gson().toJson(requestData)));

                                String site = hotelInfo.DATA.HOTEL_URL;
                                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                                ServiceUtil serviceUtil = new ServiceUtil(getActivity(),site,keycode,SiteOperate.VZASaveRequestJSON,param, RequestEntity.class);
                                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                    @Override
                                    public void OnFinished(Object results) {
                                        BaseApplication.RequestCheckVariable.set(true);
                                        ((Button)getActivity().findViewById(R.id.btnCheck)).setText(getActivity().getString(R.string.btn_wait));
                                        getActivity().findViewById(R.id.btnCheck).setClickable(false);
                                        ((AdsMenuActivity)getActivity()).broadcastUtil.Execute();
                                    }
                                });
                                serviceUtil.execute();
                            }
                        }
                    });
                    checkLocationService.execute();
                }
            });
            dialog.show(getActivity().getFragmentManager(), "tag");

        }
    };
}
