package com.smartfinder.vzaapp.Menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import emuntype.SiteOperate;
import entity.Host.Cart.MenuOrderData;
import entity.Host.Cart.MenuOrderDataAddition;
import entity.Host.Cart.MenuOrderEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.MasterItem.MasterItemData;
import entity.Service.MasterItem.MasterItemEntity;
import entity.Service.Ordering.OrderingEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

public class OpenFoodActivity extends Activity {

    private Integer itemAmount = 1;
    private Double itemPrice = 0.00;
    private String itemCurrency = "$";
    private String itemName = "";
    private Integer itemID = 0;
    private String lookFor = "";
    private Boolean isMenuSet = false;
    private MasterItemData menuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_openfood_item);

        //Init Extras
        Bundle bundle = getIntent().getExtras();
        itemID = bundle.getInt("itemID");
        isMenuSet = bundle.getBoolean("isMenuSet");
        lookFor = bundle.getString("lookFor") != null ? bundle.getString("lookFor") : "";

        if (!bundle.getString("favItem").isEmpty()) {
            menuItem = new Gson().fromJson(bundle.getString("favItem"), MasterItemData.class);
        } else {
            menuItem = BaseApplication.GlobalVariable.get(this, MasterItemEntity.class).FindByID(itemID);
        }

        itemPrice = menuItem.MENU_UNITPRICE;
        itemCurrency = menuItem.CURRENCY_DEP;
        itemName = menuItem.MENU_NAME1;

        ((TextView) findViewById(R.id.txtAmount)).setText(itemAmount.toString());

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);
        findViewById(R.id.btnPlus).setOnClickListener(btnAmount_OnClick);
        findViewById(R.id.btnMinus).setOnClickListener(btnAmount_OnClick);
        findViewById(R.id.btnOrder).setOnClickListener(toOrder_Event);
        findViewById(R.id.btnTocart).setOnClickListener(toOrder_Event);
    }

    private Button.OnClickListener btnAmount_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.btnPlus && itemAmount < 9) {
                itemAmount = itemAmount + 1;
            } else if (view.getId() == R.id.btnMinus && itemAmount > 1) {
                itemAmount = itemAmount - 1;
            }
            ((TextView) findViewById(R.id.txtAmount)).setText(itemAmount.toString());
        }
    };

    private Button.OnClickListener btnClose_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            OpenFoodActivity.this.finish();
        }
    };

    private Button.OnClickListener toOrder_Event = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {

            if (view.getId() == R.id.btnOrder) {
                //Init Order Data
                List<MenuOrderData> lisOrderData = new ArrayList<>();

                itemName = ((EditText) findViewById(R.id.txtItemName)).getText().toString();
                itemPrice = Double.valueOf(((EditText) findViewById(R.id.txtItemPrice)).getText().toString());

                lisOrderData.add(new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, itemName, menuItem.MENU_SIZE, itemAmount.doubleValue(),
                        itemPrice, "", new ArrayList<MenuOrderDataAddition>()));

                //final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, lisOrderData, isMenuSet);
                final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, itemPrice, lisOrderData, isMenuSet);
                final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, HotelInfoEntity.class);
                final VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, VZACodeEntity.class);
                final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, CheckLocationEntity.class);
                final UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, UserInfoEntity.class);

                //Int Alert Order Box
                ChooseDialog dialog = ChooseDialog.newInstance(OpenFoodActivity.this, getString(R.string.alert_order_now), true);
                dialog.show(OpenFoodActivity.this.getFragmentManager(), "this");
                dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                    @Override
                    public void OnSubmit(View view) {
                        List<NameValuePair> param = new ArrayList<>();
                        param.add(new BasicNameValuePair("VZACODEPara", vzaCodeEntity.VZACODE));
                        param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                        param.add(new BasicNameValuePair("TELPara", userInfoEntity.Data.TELEPHONE));
                        param.add(new BasicNameValuePair("MENUORDERITEMWITHJSONFORMATPara", new Gson().toJson(orderDataEntity.ORDER)));

                        String site = hotelInfo.DATA.HOTEL_URL;
                        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                        ServiceUtil serviceUtil = new ServiceUtil(OpenFoodActivity.this, site, keycode,
                                SiteOperate.POSSaveTrnOrderingJSON, param, OrderingEntity.class);
                        serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                            @Override
                            public void OnFinished(Object results) {
                                BaseApplication.RequestCheckVariable.set(false);
                                OpenFoodActivity.this.finish();
                            }
                        });
                        serviceUtil.execute();
                    }
                });
            } else {
                //Init Order Data
                List<MenuOrderData> lisOrderData = new ArrayList<>();

                itemName = ((EditText) findViewById(R.id.txtItemName)).getText().toString();
                itemPrice = Double.valueOf(((EditText) findViewById(R.id.txtItemPrice)).getText().toString());

                lisOrderData.add(new MenuOrderData(menuItem.MENU_MENU_ID, menuItem.MENU_CODE, itemName, menuItem.MENU_SIZE, itemAmount.doubleValue(),
                        itemPrice, "", new ArrayList<MenuOrderDataAddition>()));

                //final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, lisOrderData, isMenuSet);
                final MenuOrderEntity orderDataEntity = new MenuOrderEntity(itemName, itemPrice, lisOrderData, isMenuSet);

                /*final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, HotelInfoEntity.class);
                final VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, VZACodeEntity.class);
                final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, CheckLocationEntity.class);
                final UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(OpenFoodActivity.this, UserInfoEntity.class);*/

                //BaseApplication.CartStateVariable.add(new MenuOrderEntity(itemName, orderDataEntity.ORDER, isMenuSet));
                BaseApplication.CartStateVariable.add(new MenuOrderEntity(itemName, itemPrice, orderDataEntity.ORDER, isMenuSet));
                OpenFoodActivity.this.finish();
            }
        }

    };
}
