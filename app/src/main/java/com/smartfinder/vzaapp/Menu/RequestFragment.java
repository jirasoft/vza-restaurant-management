package com.smartfinder.vzaapp.Menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.AdsMenuActivity;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.GuestActivity;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.MenuView.MenuView;
import control.ViewList.RequestAdapter;
import control.ViewList.RequestModel;
import control.ViewList.ViewList;
import emuntype.SiteOperate;
import entity.Host.Request.RequestData;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.Master.ServiceDetailList;
import entity.Service.Master.ServiceDetailSubList;
import entity.Service.Request.RequestEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

public class RequestFragment extends Fragment {

    public static final RequestFragment newInstance(){
        RequestFragment fragment = new RequestFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        //Set Default
        return inflater.inflate(R.layout.fragment_menu_request,container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        //Init Data
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), MasterEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),CheckLocationEntity.class);
        List<ServiceDetailList> serviceDetailList = new ArrayList<>(masterData.FindByLocationID(checkLocation.LOCATION_ID).ServiceDetail.ServiceItemList);

        //set To Adapter
        List<RequestModel> requestModel = new ArrayList<>();
        for(ServiceDetailList serviceDetail : serviceDetailList){
            requestModel.add(new RequestModel().setHeader(serviceDetail.SERVICEGRP_ID,serviceDetail.SYSTEMID,serviceDetail.SERVICEGRP_NAME));
            for(ServiceDetailSubList serviceSubList : serviceDetail.SERVICE_DETAIL){
                requestModel.add(new RequestModel().setDetail(serviceSubList.SERVICE_ID,serviceSubList.SYSTEMID,serviceSubList.SERVICE_NAME1,serviceSubList.IMAGENAME));
           }
        }

        if(requestModel.size()> 0) {
            RequestAdapter requestAdapter = new RequestAdapter(getActivity(), requestModel);
            ViewList lisView = (ViewList) getActivity().findViewById(R.id.lisItem);
            lisView.setOnItemClickListener(lisView_OnClick);
            lisView.setAdapter(requestAdapter);
        }
    }

    public ListView.OnItemClickListener lisView_OnClick  = new ListView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final RequestModel requestModel = ((RequestAdapter)parent.getAdapter()).getItem(position);
            ChooseDialog dialog = ChooseDialog.newInstance(getActivity(),String.format(getResources().getString(R.string.alert_request),requestModel.getMessage()), true);
            dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                @Override
                public void OnSubmit(View view) {

                    final VZACodeEntity vzaCode = BaseApplication.GlobalVariable.get(getActivity(),VZACodeEntity.class);
                    final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity(),CheckLocationEntity.class);
                    final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getActivity(),HotelInfoEntity.class);

                    List<NameValuePair> param = new ArrayList<NameValuePair>();
                    param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
                    param.add(new BasicNameValuePair("VZA_CODEPara",vzaCode.VZACODE));
                    String site = hotelInfo.DATA.HOTEL_URL;
                    String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                    ServiceUtil checkLocationService = new ServiceUtil(getActivity(),site,keycode, SiteOperate.VZACheckLocationOpenedJSON,param, CheckLocationEntity.class);
                    checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                            if(((CheckLocationEntity) results).RESULT){
                                //Keep checkLocation To AppVariable
                                BaseApplication.GlobalVariable.set((CheckLocationEntity) results);

                                List<NameValuePair> param = new ArrayList<>();
                                param.add(new BasicNameValuePair("LOCATIONPara",checkLocation.LOCATION_ID.toString()));
                                param.add(new BasicNameValuePair("SYSTEMID",requestModel.getSystemid().toString()));
                                param.add(new BasicNameValuePair("VZACODEPara",vzaCode.VZACODE));
                                param.add(new BasicNameValuePair("REFNO",""));
                                param.add(new BasicNameValuePair("TEXTMSG",""));

                                ArrayList<RequestData> requestData = new ArrayList<>();
                                requestData.add(new RequestData(requestModel.getId()));
                                param.add(new BasicNameValuePair("REQTYPEIDJSON", new Gson().toJson(requestData)));

                                String site = hotelInfo.DATA.HOTEL_URL;
                                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                                ServiceUtil serviceUtil = new ServiceUtil(getActivity(),site,keycode,SiteOperate.VZASaveRequestJSON,param, RequestEntity.class);
                                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                    @Override
                                    public void OnFinished(Object results) {
                                        MenuView menuView = (MenuView) getActivity().findViewById(R.id.frmFragment);
                                        //Go To First Page
                                        menuView.setCurrentItem(0,true);
                                        //Call Notification
                                        ((AdsMenuActivity)getActivity()).broadcastUtil.Execute();
                                    }
                                });
                                serviceUtil.execute();
                            }
                        }
                    });
                    checkLocationService.execute();
                }
            });
            dialog.show(getActivity().getFragmentManager(), "tag");

        }
    };

}
