package com.smartfinder.vzaapp.Menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.smartfinder.vzaapp.AdsMenuActivity;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.GuestActivity;
import com.smartfinder.vzaapp.R;
import java.util.ArrayList;
import java.util.List;
import control.MenuView.MenuView;
import control.ViewList.MenuGroupAdapter;
import control.ViewList.MenuGroupModel;
import control.ViewList.ViewList;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterDetail;
import entity.Service.Master.MasterDetailList;
import entity.Service.Master.MasterDetailSubList;
import entity.Service.Master.MasterEntity;

public class SubGroupFragment extends Fragment {

    public static final SubGroupFragment newInstance(Integer groupID,String lookFor)
    {
        SubGroupFragment fragment = new SubGroupFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("groupID", groupID);
        bundle.putString("lookFor", lookFor);
        fragment.setArguments(bundle);
        return fragment ;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_subgroup, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        //Init Group List
        //Init Bundle
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(), MasterEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getActivity().getBaseContext(),CheckLocationEntity.class);
        MasterDetail masterDetail =  masterData.FindByLocationID(checkLocation.LOCATION_ID).MenuDetail;
        MasterDetailList masterDetailList = masterDetail.FindByID(getArguments().getInt("groupID"),getArguments().getString("lookFor"));
        List<MasterDetailSubList> lisMasterDetail = masterDetailList.MENUSUBGROEP_DETAIL;

        //Add MenuGroupList
        List<MenuGroupModel> lisModel = new ArrayList<>();
        for(MasterDetailSubList detailList : lisMasterDetail ){
            lisModel.add(new MenuGroupModel(detailList.MENU_SUBGROUP_IMAGENAME,detailList.DisplayText,detailList.MENU_SUBMENU_ID.toString()));
        }

        MenuGroupAdapter serviceAdapter = new MenuGroupAdapter(getActivity(),lisModel);
        ViewList lisView = (ViewList) getActivity().findViewById(R.id.lisSubMenu);
        lisView.setAdapter(serviceAdapter);

        //Init Event
        lisView.setOnItemClickListener(lisView_OnItemClick);
    }

    public ListView.OnItemClickListener lisView_OnItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            MenuView menuView = (MenuView)getActivity().findViewById(R.id.frmFragment);
            ((AdsMenuActivity)getActivity()).menuViewAdapter.addItem(menuView,
                                           ItemGroupFragment.newInstance(getArguments().getInt("groupID"),
                                           getArguments().getString("lookFor"),
                                           (Integer.parseInt((String)view.findViewById(R.id.txtMessage).getTag()))));
        }
    };
}
