package com.smartfinder.vzaapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.zxing.Result;
import com.google.zxing.client.android.CaptureActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.Dialog.ConfirmDialog;
import control.Dialog.CoverDialog;
import emuntype.MainOperate;
import emuntype.SiteOperate;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.QRCodeInfo.QRCodeInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.CheckLogin.CheckLoginEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.OpenTable.OpenTableEntity;
import entity.Service.VZACode.VZACodeEntity;
import entity.Service.VZAOpen.VZAOpenEntity;
import util.ServiceUtil;
import util.ViewUtil;


public class QRCodeActivity extends CaptureActivity {

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    private static final long BULK_MODE_SCAN_DELAY_MS = 1000L;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);

        //Init Event
        (findViewById(R.id.btnUseInfo)).setOnClickListener(btnUseInfo_onClick);
        findViewById(R.id.btnClose).setOnClickListener(btnClose_onClick);
    }

    @Override
    public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
        // TODO Auto-generated method stub
        super.handleDecode(rawResult, barcode, scaleFactor);
        String contents = rawResult.getText();
        Log.d("code", contents);
        try {
            Gson gson = new Gson();
            QRCodeInfoEntity qrcodeInfo = gson.fromJson(contents, QRCodeInfoEntity.class);

            //Keep QR Code To AppVariable
            BaseApplication.GlobalVariable.set(qrcodeInfo);

            VZACheckLoginJSON();

        } catch (Exception ex) {
            Log.d("Convert JSON", ex.getMessage());
        }
    }

    private Button.OnClickListener btnUseInfo_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String strJSON = "{'smartscan':'1', 'data':{'Location_ID':'1','Hotel_Code':'1616','Table_No':'11'}}";
            Gson gson = new Gson();
            QRCodeInfoEntity qrcodeInfo = gson.fromJson(strJSON, QRCodeInfoEntity.class);
            //Keep QR Code To AppVariable
            BaseApplication.GlobalVariable.set(qrcodeInfo);

            VZACheckLoginJSON();
        }
    };

    public Button.OnClickListener btnClose_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public void ScanQRCode() {
        View view = ((Activity) QRCodeActivity.this).getWindow().getDecorView().findViewById(android.R.id.content);
        ViewUtil.isEnableView(view, true);
        restartPreviewAfterDelay(BULK_MODE_SCAN_DELAY_MS);
    }

    public void LoginAgain() {
        finish();
        Intent intent = new Intent(QRCodeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void VZACheckLoginJSON() {
        //-- Get QRCode
        QRCodeInfoEntity qrcodeInfo = BaseApplication.GlobalVariable.get(this, QRCodeInfoEntity.class);

        List<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("HOTELCODEPara", qrcodeInfo.data.Hotel_Code));
        ServiceUtil hotelInfoService = new ServiceUtil(QRCodeActivity.this, MainOperate.HotelInformationbyCodeJSON, param, HotelInfoEntity.class);
        hotelInfoService.setFinishedListener(hotelInfoService_OnFinished);
        hotelInfoService.execute();
    }

    public ServiceUtil.OnFinished hotelInfoService_OnFinished = new ServiceUtil.OnFinished() {
        @Override
        public void OnFinished(Object results) {
            HotelInfoEntity hotelInfoEntity = (HotelInfoEntity) results;
            if (hotelInfoEntity.DATA.HOTEL_ID == 0) {
                ChooseDialog dialog = ChooseDialog.newInstance(QRCodeActivity.this, hotelInfoEntity.DATA.ErrorMsg, false);
                dialog.show(getFragmentManager(), "tag");
                return;
            } else {
                //Keep hotelInfo To AppVariable
                BaseApplication.GlobalVariable.set(hotelInfoEntity);
                UserInfoEntity userInfo = BaseApplication.GlobalVariable.get(getBaseContext(), UserInfoEntity.class);
                HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getBaseContext(), HotelInfoEntity.class);
                QRCodeInfoEntity qrcodeInfo = BaseApplication.GlobalVariable.get(getBaseContext(), QRCodeInfoEntity.class);

                //-- VZACheckLoginJSON
                List<NameValuePair> param = new ArrayList<NameValuePair>();
                //param.add(new BasicNameValuePair("KEYCODEPara", hotelInfo.DATA.HOTEL_KEYCODE));
                param.add(new BasicNameValuePair("LOCATION_IDPara", qrcodeInfo.data.Location_ID));
                param.add(new BasicNameValuePair("TABLE_NOPara", qrcodeInfo.data.Table_No));
                param.add(new BasicNameValuePair("TIME_CHECKINPara", BaseApplication.DateTimeVariable.getTimeHHMMS()));
                param.add(new BasicNameValuePair("TELEPHONE_NOPara", userInfo.Data.TELEPHONE));
                //ServiceUtil(Context context, String site, String keycode, Enum operate, List<NameValuePair> param, Class entityClass)
                //ServiceUtil vzaInfoService = new ServiceUtil(QRCodeActivity.this, hotelInfo.DATA.HOTEL_URL, hotelInfo.DATA.HOTEL_KEYCODE,
                //        SiteOperate.VZACheckLoginJSON, param, CheckLoginEntity.class);
                ServiceUtil vzaInfoService = new ServiceUtil(QRCodeActivity.this, hotelInfo.DATA.HOTEL_URL, hotelInfo.DATA.HOTEL_KEYCODE,
                        SiteOperate.VZACheckLoginJSON, param, VZACodeEntity.class);
                vzaInfoService.setFinishedListener(vzaInfoService_OnFinished);
                vzaInfoService.execute();
            }
        }
    };

    public ServiceUtil.OnFinished vzaInfoService_OnFinished = new ServiceUtil.OnFinished() {
        @Override
        public void OnFinished(Object results) {
            //final CheckLoginEntity checkLoginEntity = (CheckLoginEntity) results;
            final VZACodeEntity checkLoginEntity = (VZACodeEntity) results;

            //-- Get Global Variable
            final QRCodeInfoEntity qrcodeInfo = BaseApplication.GlobalVariable.get(getBaseContext(), QRCodeInfoEntity.class);

            if (checkLoginEntity.ERRORMSG.equals("")) {
                //-- Keep CheckLogin To AppVariable
                BaseApplication.GlobalVariable.set(checkLoginEntity);
                //--
                if (checkLoginEntity.LOCATION_OPEN.equals(true)) {
                    if (checkLoginEntity.TABLE_USE.equals(0)) {
                        //-- Popup Cover
                        final CoverDialog coverDialog = CoverDialog.newInstance(QRCodeActivity.this);
                        coverDialog.setOnSubmitListener(new CoverDialog.OnSubmit() {
                            @Override
                            public void OnSubmit(final Integer pax) {
                                OpenTableAndOpenVZA(pax);
                                coverDialog.dismiss();
                            }
                        });
                        //-- On Click Cancel
                        coverDialog.setOnDismissListener(new CoverDialog.OnDismiss() {
                            @Override
                            public void OnDismiss() {
                                if(BaseApplication.GlobalVariable.get(getBaseContext(),OpenTableEntity.class) == null) {
                                    doScanAgain();
                                }
                            }
                        });
                        coverDialog.show(QRCodeActivity.this.getFragmentManager(), "tag");
                    } else {
                        //-- Do you want to join table number?
                        String message = "";
                        //if (!qrcodeInfo.data.Table_No.equals("")) {
                        if (checkLoginEntity.TABLE_NO.equals("")) {
                            message = getResources().getString(R.string.alert_join_table).replace("{TABLENO}", qrcodeInfo.data.Table_No);
                        } else {
                            message = getResources().getString(R.string.alert_join_table).replace("{TABLENO}", checkLoginEntity.TABLE_NO);
                        }
                        ConfirmDialog tableJoinDialog = ConfirmDialog.newInstance(QRCodeActivity.this, message);
                        tableJoinDialog.setOnSubmitListener(new ConfirmDialog.OnSubmit() {
                            @Override
                            public void OnSubmit(View view) {
                                //-- IF VZACODE = ""
                                if (checkLoginEntity.VZACODE.equals("")) {
                                    //-- Get Global Variable
                                    UserInfoEntity userInfo = BaseApplication.GlobalVariable.get(getBaseContext(), UserInfoEntity.class);
                                    HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getBaseContext(), HotelInfoEntity.class);
                                    //CheckLoginEntity checkLoginEntity = BaseApplication.GlobalVariable.get(getBaseContext(), CheckLoginEntity.class);

                                    //-- VZAOpenJSON
                                    List<NameValuePair> param = new ArrayList<NameValuePair>();
                                    param.add(new BasicNameValuePair("LOCATION_IDPara", qrcodeInfo.data.Location_ID));
                                    if (!qrcodeInfo.data.Table_No.equals("")) {
                                        param.add(new BasicNameValuePair("TABLE_NOPara", qrcodeInfo.data.Table_No));
                                    } else {
                                        param.add(new BasicNameValuePair("TABLE_NOPara", checkLoginEntity.TABLE_NO));
                                    }
                                    param.add(new BasicNameValuePair("TELEPHONE_NOPara", userInfo.Data.TELEPHONE));
                                    ServiceUtil vzaOpenService = new ServiceUtil(QRCodeActivity.this, hotelInfo.DATA.HOTEL_URL, hotelInfo.DATA.HOTEL_KEYCODE,
                                            SiteOperate.VZAOpenJSON, param, VZAOpenEntity.class);
                                    vzaOpenService.setFinishedListener(new ServiceUtil.OnFinished() {
                                        @Override
                                        public void OnFinished(Object results) {
                                            VZAOpenEntity vzaOpenEntity = (VZAOpenEntity) results;
                                            //-- Keep VZAOpen To AppVariable
                                            BaseApplication.GlobalVariable.set(vzaOpenEntity);
                                            if (vzaOpenEntity.ERRORMSG.equals("")) {
                                                //-- Main Menu
                                                doMainMenu();
                                            } else {
                                                //-- ERROR
                                                ChooseDialog errorDialog = ChooseDialog.newInstance(QRCodeActivity.this, vzaOpenEntity.ERRORMSG, false);
                                                errorDialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                                                    @Override
                                                    public void OnSubmit(View view) {
                                                        ScanQRCode();
                                                    }
                                                });
                                                errorDialog.show(QRCodeActivity.this.getFragmentManager(), "tag");
                                            }
                                        }
                                    });
                                    vzaOpenService.execute();
                                } else {
                                    //-- Main Menu
                                    doMainMenu();
                                }
                            }
                        });
                        tableJoinDialog.setOnCancelListener(new ConfirmDialog.OnCancel() {
                            @Override
                            public void OnCancel() {
                                doScanAgain();
                            }
                        });
                        tableJoinDialog.show(getFragmentManager(), "tag");
                    }
                } else {
                    ChooseDialog dialog = ChooseDialog.newInstance(QRCodeActivity.this, getResources().getString(R.string.alert_location_close), false);
                    dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                        @Override
                        public void OnSubmit(View view) {
                            //-- Scan QR Code
                            ScanQRCode();
                        }
                    });
                    dialog.show(getFragmentManager(), "tag");
                }
            } else {
                ChooseDialog dialog = ChooseDialog.newInstance(QRCodeActivity.this, checkLoginEntity.ERRORMSG, false);
                dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                    @Override
                    public void OnSubmit(View view) {
                        //-- Scan QR Code
                        ScanQRCode();
                    }
                });
                dialog.show(getFragmentManager(), "tag");
            }
        }
    };

    public void doMainMenu() {
        //-- Get Global Variable
        UserInfoEntity userInfo = BaseApplication.GlobalVariable.get(getBaseContext(), UserInfoEntity.class);
        final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getBaseContext(), HotelInfoEntity.class);

        //Init Service
        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("GUEST_TYPEPara", userInfo.Data.GUEST_TYPE.toString()));
        param.add(new BasicNameValuePair("STAFF_CODEPara", userInfo.Data.STAFF_CODE.toString()));

        String site = hotelInfo.DATA.HOTEL_URL;
        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
        ServiceUtil masterService = new ServiceUtil(QRCodeActivity.this, site, keycode, SiteOperate.VZAgetMasterDataJSON, param, MasterEntity.class);
        masterService.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                MasterEntity masterEntity = (MasterEntity) results;

                QRCodeInfoEntity qrcodeInfo = (QRCodeInfoEntity) BaseApplication.GlobalVariable.get(getBaseContext(), QRCodeInfoEntity.class);
                VZACodeEntity vzaCode = (VZACodeEntity) BaseApplication.GlobalVariable.get(getBaseContext(), VZACodeEntity.class);
                OpenTableEntity openTable = (OpenTableEntity) BaseApplication.GlobalVariable.get(getBaseContext(), OpenTableEntity.class);

                //-- New
                CheckLocationEntity entity = new CheckLocationEntity();
                entity.RESULT = true;
                entity.VZAID = 0;
                entity.VZACODE = vzaCode.VZACODE != "" ? vzaCode.VZACODE : openTable.VZACODE;
                entity.LOCATION_ID= Integer.parseInt(qrcodeInfo.data.Location_ID);
                entity.ROOM_ID = "";
                entity.TABLE_NO = vzaCode.TABLE_NO != "" ? vzaCode.TABLE_NO : qrcodeInfo.data.Table_No;
                entity.VZA_TYPE = 0;

                //Keep data To AppVariable
                BaseApplication.GlobalVariable.set(masterEntity);
                BaseApplication.GlobalVariable.set(entity);
                BaseApplication.RequestCheckVariable.set(false);

                //New Intent With Clear
                Intent intent = new Intent(QRCodeActivity.this,GuestActivity.class);
                intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }
        });
        masterService.execute();
    }

    public void doScanAgain() {
        ConfirmDialog againDialog = ConfirmDialog.newInstance(QRCodeActivity.this, getResources().getString(R.string.alert_scan_again));
        againDialog.setOnSubmitListener(new ConfirmDialog.OnSubmit() {
            @Override
            public void OnSubmit(View view) {
                //-- Scan QR Code
                ScanQRCode();
            }
        });
        againDialog.setOnCancelListener(new ConfirmDialog.OnCancel() {
            @Override
            public void OnCancel() {
                //-- Go Login
                LoginAgain();
            }
        });
        againDialog.show(QRCodeActivity.this.getFragmentManager(), "tag");
    }

    public void OpenTableAndOpenVZA(final Integer pax) {
        //-- Get AppVariable
        UserInfoEntity userInfo = BaseApplication.GlobalVariable.get(getBaseContext(), UserInfoEntity.class);
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getBaseContext(), HotelInfoEntity.class);
        QRCodeInfoEntity qrcodeInfo = BaseApplication.GlobalVariable.get(getBaseContext(), QRCodeInfoEntity.class);

        //-- VZAOpenTableAndOpenVZAJSON
        List<NameValuePair> param = new ArrayList<NameValuePair>();
        param.add(new BasicNameValuePair("LOCATION_IDPara", qrcodeInfo.data.Location_ID));
        param.add(new BasicNameValuePair("TABLE_NOPara", qrcodeInfo.data.Table_No));
        param.add(new BasicNameValuePair("ACCEPT_COVERPara", pax.toString()));
        param.add(new BasicNameValuePair("TELEPHONE_NOPara", userInfo.Data.TELEPHONE));
        ServiceUtil openTableInfoService = new ServiceUtil(QRCodeActivity.this, hotelInfo.DATA.HOTEL_URL, hotelInfo.DATA.HOTEL_KEYCODE,
                SiteOperate.VZAOpenTableAndOpenVZAJSON, param, OpenTableEntity.class);
        openTableInfoService.setFinishedListener(openTableInfoService_OnFinished);
        openTableInfoService.execute();
    }

    public ServiceUtil.OnFinished openTableInfoService_OnFinished = new ServiceUtil.OnFinished() {
        @Override
        public void OnFinished(Object results) {
            OpenTableEntity openTableEntity = (OpenTableEntity) results;

            if (openTableEntity.ERRORMSG.equals("")) {
                BaseApplication.GlobalVariable.set(openTableEntity);
                doMainMenu();
            } else {
                ChooseDialog dialogOpenTable = ChooseDialog.newInstance(QRCodeActivity.this, openTableEntity.ERRORMSG, false);
                dialogOpenTable.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                    @Override
                    public void OnSubmit(View view) {
                        ScanQRCode();
                    }
                });
                dialogOpenTable.show(getFragmentManager(), "tag");
            }

        }
    };


}
