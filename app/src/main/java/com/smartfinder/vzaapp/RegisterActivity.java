package com.smartfinder.vzaapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import control.Dialog.ChooseDialog;
import emuntype.MainOperate;
import entity.BaseEntity;
import util.ServiceUtil;


public class RegisterActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Init Page
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        (findViewById(R.id.btnClose)).setOnClickListener(btnClose_OnClick);
        (findViewById(R.id.btnRegister)).setOnClickListener(btnRegister_OnClick);
    }

    private Button.OnClickListener btnRegister_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           //Init Control
           Boolean rdoMale = ((RadioButton)findViewById(R.id.rdoMale)).isChecked();
           String txtFirstName = ((EditText)findViewById(R.id.txtFirstName)).getText().toString();
           String txtLastName = ((EditText)findViewById(R.id.txtLastName)).getText().toString();
           String txtEmail = ((EditText)findViewById(R.id.txtEmail)).getText().toString();
           String txtResidence = ((EditText)findViewById(R.id.txtResidence)).getText().toString();
           String txtCountry = ((EditText)findViewById(R.id.txtCountry)).getText().toString();
           String txtPhone = ((EditText)findViewById(R.id.txtPhone)).getText().toString();
           String txtPassword = ((EditText)findViewById(R.id.txtPassword)).getText().toString();
           String txtRePassword = ((EditText)findViewById(R.id.txtRePassword)).getText().toString();
           String txtZipCode = ((EditText)findViewById(R.id.txtZipCode)).getText().toString();
           String txtDOB = ((EditText)findViewById(R.id.txtDOB)).getText().toString();
           String txtNationality = ((EditText)findViewById(R.id.txtNationality)).getText().toString();
           Boolean validDOB;

            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            sdf.setLenient(false);
            try {

                //if not valid, it will throw ParseException
                Date date = sdf.parse(txtDOB);
                validDOB = true;

            } catch (ParseException e) {

                validDOB = false;
            }

            //Validation Check
            if(txtFirstName.length() == 0 || txtLastName.length() == 0 || txtEmail.length() == 0 ||
               txtPhone.length() == 0 || txtPassword.length() == 0 || txtRePassword.length() == 0){
                ChooseDialog dialog = ChooseDialog.newInstance(RegisterActivity.this,getResources().getString(R.string.alert_register_fill),false);
                dialog.show(getFragmentManager(),"tag");
            } else if(txtDOB.length() == 0 || !validDOB){
                ChooseDialog dialog = ChooseDialog.newInstance(RegisterActivity.this,getResources().getString(R.string.alert_DOB_invalid),false);
                dialog.show(getFragmentManager(),"tag");
            }else if(!Patterns.EMAIL_ADDRESS.matcher(txtEmail).matches()){
                ChooseDialog dialog = ChooseDialog.newInstance(RegisterActivity.this,getResources().getString(R.string.alert_email_invalid),false);
                dialog.show(getFragmentManager(),"tag");
            }else if(txtPhone.length() != 10 && txtPhone.length() != 11 ){
                ChooseDialog dialog = ChooseDialog.newInstance(RegisterActivity.this,getResources().getString(R.string.alert_phone_invalid),false);
                dialog.show(getFragmentManager(),"tag");
            }else if(!txtPassword.equals(txtRePassword)){
                ChooseDialog dialog = ChooseDialog.newInstance(RegisterActivity.this,getResources().getString(R.string.alert_password_match),false);
                dialog.show(getFragmentManager(),"tag");
            }else{
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("IDPara","0"));
                if(rdoMale) {
                    param.add(new BasicNameValuePair("TITLENAMEPara", "Male"));
                }else{
                    param.add(new BasicNameValuePair("TITLENAMEPara", "Famale"));
                }
                param.add(new BasicNameValuePair("FIRSTNAMEPara",txtFirstName));
                param.add(new BasicNameValuePair("LASTNAMEPara",txtLastName));
                param.add(new BasicNameValuePair("EMAILPara",txtEmail));
                param.add(new BasicNameValuePair("RESIDENCECOUNTRYPara",txtResidence + " " + txtCountry));
                param.add(new BasicNameValuePair("TELEPHONEPara",txtPhone));
                param.add(new BasicNameValuePair("PASSCODEPara",txtPassword));
                param.add(new BasicNameValuePair("CITYPara",txtResidence));
                param.add(new BasicNameValuePair("ZIPCODEPara",txtZipCode));
                param.add(new BasicNameValuePair("DATEOFBIRTHPara",txtDOB));
                param.add(new BasicNameValuePair("NATIONALITYPara",txtNationality));

                ServiceUtil serviceUtil = new ServiceUtil(RegisterActivity.this, MainOperate.SaveRegisterJSON,param, BaseEntity.class);
                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        ChooseDialog dialog = ChooseDialog.newInstance(RegisterActivity.this,getResources().getString(R.string.alert_register_success),false);
                        dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                            @Override
                            public void OnSubmit(View view) {
                                RegisterActivity.this.finish();
                            }
                        });
                        dialog.show(getFragmentManager(),"tag");
                    }
                });
                serviceUtil.execute();
            }

        }
    };

    private Button.OnClickListener btnClose_OnClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            RegisterActivity.this.finish();
        }
    };

}
