package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;
import com.smartfinder.vzaapp.StaffActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.ExpandableList.ExpandableList;
import control.ExpandableList.MyBillAdapter;
import emuntype.SiteOperate;
import entity.Host.FollowUp.FollowUpData;
import entity.Host.Request.RequestData;
import entity.Host.Served.ServedData;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.BillOrder.BillOrderData;
import entity.Service.BillOrder.BillOrderDataAddition;
import entity.Service.BillOrder.BillOrderEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.FollowUp.FollowUpEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.Request.RequestEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class MyBillActivity extends Activity {

    //Footer View
    private View Footer;
    private BillOrderEntity billEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mybill);

        //Init View
        Footer = new View(MyBillActivity.this);

        //Set Check Button
        if(BaseApplication.RequestCheckVariable.get()){
            ((Button)findViewById(R.id.btnCheck)).setText(getString(R.string.btn_wait));
            findViewById(R.id.btnCheck).setClickable(false);
        }else{
            ((Button)findViewById(R.id.btnCheck)).setText(getString(R.string.btn_checkbill));
            findViewById(R.id.btnCheck).setClickable(true);
        }

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);

    }

    @Override
    protected void onResume() {
        super.onResume();

        try {

            //Init ExpList
            final ExpandableList expListView = ((ExpandableList) (findViewById(R.id.expBill)));
            final Integer SummaryListVisible = findViewById(R.id.frmSummaryList).getVisibility();

            if (expListView.getFooterViewsCount() == 0) {
                findViewById(R.id.frmSummaryList).setVisibility(View.VISIBLE);
            }

            //Init Data
            HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyBillActivity.this, HotelInfoEntity.class);
            Integer locationID = BaseApplication.GlobalVariable.get(MyBillActivity.this, CheckLocationEntity.class).LOCATION_ID;
            String locationName = BaseApplication.GlobalVariable.get(MyBillActivity.this, MasterEntity.class).FindByLocationID(locationID).LOCATION_NAME1;
            String VZACode = BaseApplication.GlobalVariable.get(MyBillActivity.this, VZACodeEntity.class).VZACODE;
            final String currencyDep = BaseApplication.GlobalVariable.get(MyBillActivity.this, MasterEntity.class).FindByLocationID(locationID).CURRENCY_DEP;

            //Init Label
            ((TextView) findViewById(R.id.txtLocation)).setText(locationName);

            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("LOCATION_IDPara", locationID.toString()));
            param.add(new BasicNameValuePair("VZACODEPara", VZACode));
            String site = hotelInfo.DATA.HOTEL_URL;
            String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

            ServiceUtil serviceUtil = new ServiceUtil(MyBillActivity.this, site, keycode,
                    SiteOperate.POSgetTrnOrderingbyVZACODEJSON, param,
                    BillOrderEntity.class);

            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    //Init Adapter
                    billEntity = (BillOrderEntity) results;
                    expListView.setAdapter(new MyBillAdapter(MyBillActivity.this, billEntity, currencyDep));
                    expListView.setOnGroupClickListener(expListView_OnGroupClick);

                    //Init Space
                    if (expListView.getFooterViewsCount() == 0) {

                        //Init frmSummaryList Status
                        findViewById(R.id.frmSummaryList).setVisibility(SummaryListVisible);

                        //Init line Space
                        //Init View
                        View FooterSpace = new View(MyBillActivity.this);
                        final float scale = getResources().getDisplayMetrics().density;
                        FooterSpace.setLayoutParams(new AbsListView.LayoutParams(MATCH_PARENT, (int) (scale + 0.5f)));
                        FooterSpace.setBackgroundResource(R.color.bg);
                        expListView.addFooterView(FooterSpace, null, false);

                        Footer.setLayoutParams(new AbsListView.LayoutParams(MATCH_PARENT, findViewById(R.id.btnSummary).getHeight()));
                        expListView.addFooterView(Footer, null, false);
                    }

                    //Init Remark
                    TextView txtRemark = (TextView) findViewById(R.id.txtSummaryDesc);
                    txtRemark.setText(billEntity.MESSAGE_ONFOOTERBILL);

                    //Init Total Value
                    TextView txtAllPrice = (TextView) findViewById(R.id.txtAllPrice);
                    TextView txtNetPrice = (TextView) findViewById(R.id.txtNet);
                    TextView txtDiscountPrice = (TextView) findViewById(R.id.txtDiscount);
                    TextView txtService = (TextView) findViewById(R.id.txtService);
                    TextView txtVatPrice = (TextView) findViewById(R.id.txtVat);

                    if (billEntity.TOTAL == 0.00) {
                        double allPrice = 0.00;
                        for (BillOrderData orderData : billEntity.ITEMS) {
                            double orderPrice = 0.00;
                            if (!orderData.ADDITIONALDETAIL.isEmpty()) {
                                for (BillOrderDataAddition addition : orderData.ADDITIONALDETAIL) {
                                    orderPrice = orderPrice + addition.ADDITIONAL_UNITPRICE;
                                }
                            }
                            orderPrice = (orderPrice + orderData.MENU_UNITPRICE) * orderData.ORDER_QTY;
                            allPrice = allPrice + orderPrice;
                        }
                        txtAllPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(allPrice));
                        txtNetPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(allPrice));
                        txtDiscountPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(0.00));
                        txtService.setText(currencyDep + new DecimalFormat("#,##0.00").format(0.00));
                        txtVatPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(0.00));
                    } else {
                        txtAllPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.TOTAL));
                        txtNetPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.NET));
                        txtDiscountPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.DISCOUNT));
                        txtService.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.SERVICE));
                        txtVatPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.VAT));
                    }

                    //Init Event
                    findViewById(R.id.btnSummary).setOnClickListener(btnSummary_OnClick);
                    findViewById(R.id.btnFollowUp).setOnClickListener(btnFollowUp_OnClick);
                    findViewById(R.id.btnCheck).setOnClickListener(btnCheck_OnClick);
                    findViewById(R.id.btnServed).setOnClickListener(btnServed_OnClick);
                }
            });
            serviceUtil.execute();
        }catch(Exception ex){
            ChooseDialog dialog = ChooseDialog.newInstance(MyBillActivity.this, getResources().getString(R.string.alert_default), false);
            dialog.show(getFragmentManager(), "tag");
            return;
        }
    }

    public ExpandableList.OnGroupClickListener expListView_OnGroupClick = new ExpandableListView.OnGroupClickListener() {
        @Override
        public boolean onGroupClick(ExpandableListView parent, View view, int groupPosition, long id) {
            if(((MyBillAdapter) parent.getExpandableListAdapter()).getGroup(groupPosition) != null) {
                MyBillAdapter adapter = (MyBillAdapter) parent.getExpandableListAdapter();
                BillOrderData entity = adapter.getGroup(groupPosition);
                if (entity.ORDER_STATUS == 0) {
                    entity.ORDER_STATUS = 100;
                } else if (entity.ORDER_STATUS == 100) {
                    entity.ORDER_STATUS = 0;
                }

                if (entity.ORDER_STATUS == 1) {
                    entity.ORDER_STATUS = 200;
                } else if (entity.ORDER_STATUS == 200) {
                    entity.ORDER_STATUS = 1;
                }
                adapter.notifyDataSetChanged();

                MyBillActivity.this.findViewById(R.id.frmServed).setVisibility(View.GONE);
                for(BillOrderData data : adapter.getEntity().ITEMS){
                    if(data.ORDER_STATUS == 100 || data.ORDER_STATUS == 200){
                        MyBillActivity.this.findViewById(R.id.frmServed).setVisibility(View.VISIBLE);
                        break;
                    }
                }
            }
            return true;
        }
    };

    public ImageButton.OnClickListener btnSummary_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
           final LinearLayout frmSummaryList = (LinearLayout)findViewById(R.id.frmSummaryList);
           final LinearLayout frmBillSummary = (LinearLayout)findViewById(R.id.frmBillSummaryCtrl);
            if(frmSummaryList.getVisibility() == View.VISIBLE){
                //Init Button Animation
                TranslateAnimation frmAnimate = new TranslateAnimation(0,0,0,frmSummaryList.getHeight());
                frmAnimate.setDuration(500);
                frmAnimate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        ((ImageButton)view).setImageResource(R.drawable.pic_arrowup);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        frmSummaryList.setVisibility(View.GONE);
                        Footer.setLayoutParams( new AbsListView.LayoutParams(MATCH_PARENT,view.getHeight()));
                    }
                });
                frmBillSummary.startAnimation(frmAnimate);
            }else{
                //Init Button Animation
                TranslateAnimation frmAnimate = new TranslateAnimation(0,0,frmSummaryList.getHeight(),0);
                frmAnimate.setDuration(500);
                frmAnimate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        ((ImageButton) view).setImageResource(R.drawable.pic_arrowdown);
                        frmSummaryList.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Footer.setLayoutParams( new AbsListView.LayoutParams(MATCH_PARENT,frmBillSummary.getHeight()));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                frmBillSummary.startAnimation(frmAnimate);
            }
        }
    };

    public Button.OnClickListener btnFollowUp_OnClick = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyBillActivity.this,CheckLocationEntity.class);
            final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyBillActivity.this,HotelInfoEntity.class);

            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("LOCATION_IDPata",checkLocation.LOCATION_ID.toString()));
            List<FollowUpData> reqFollowUp = new ArrayList<>();
            for(BillOrderData orderData : billEntity.ITEMS){
                if(orderData.ORDER_STATUS != 2 && orderData.ORDER_STATUS != 3) {
                    reqFollowUp.add(new FollowUpData(orderData.ORDER_ID));
                }
            }
            param.add(new BasicNameValuePair("ORDER_IDPara",new Gson().toJson(reqFollowUp)));
            String site = hotelInfo.DATA.HOTEL_URL;
            String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

            ServiceUtil serviceUtil = new ServiceUtil(MyBillActivity.this,site,keycode,SiteOperate.VZAUpdateOrderFollowUpJSON,param, FollowUpEntity.class);
            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    ChooseDialog dialog = ChooseDialog.newInstance(MyBillActivity.this,getResources().getString(R.string.alert_followup_success), false);
                    dialog.show(getFragmentManager(), "tag");
                    dialog.setOnDismissListener(new ChooseDialog.OnDismiss() {
                        @Override
                        public void OnDismiss() {
                            MyBillActivity.this.finish();
                        }
                    });

                }
            });
            serviceUtil.execute();
        }
    };

    public Button.OnClickListener btnCheck_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!((Button)findViewById(R.id.btnCheck)).getText().equals(getString(R.string.btn_wait))) {
                ChooseDialog dialog = ChooseDialog.newInstance(MyBillActivity.this, getResources().getString(R.string.alert_check_bill), true);
                dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                    @Override
                    public void OnSubmit(View view) {
                        final VZACodeEntity vzaCode = BaseApplication.GlobalVariable.get(MyBillActivity.this, VZACodeEntity.class);
                        final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyBillActivity.this, CheckLocationEntity.class);
                        final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyBillActivity.this, HotelInfoEntity.class);

                        List<NameValuePair> param = new ArrayList<>();
                        param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                        param.add(new BasicNameValuePair("VZA_CODEPara", vzaCode.VZACODE));
                        String site = hotelInfo.DATA.HOTEL_URL;
                        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                        ServiceUtil checkLocationService = new ServiceUtil(MyBillActivity.this, site, keycode, SiteOperate.VZACheckLocationOpenedJSON, param, CheckLocationEntity.class);
                        checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
                            @Override
                            public void OnFinished(Object results) {
                                if (((CheckLocationEntity) results).RESULT) {

                                    //Keep checkLocation To AppVariable
                                    BaseApplication.GlobalVariable.set((CheckLocationEntity) results);
                                    MasterEntity masterEntity = BaseApplication.GlobalVariable.get(MyBillActivity.this, MasterEntity.class);
                                    List<NameValuePair> param = new ArrayList<>();
                                    param.add(new BasicNameValuePair("LOCATIONPara", checkLocation.LOCATION_ID.toString()));
                                    param.add(new BasicNameValuePair("SYSTEMID", masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SYSTEMID.toString()));
                                    param.add(new BasicNameValuePair("VZACODEPara", vzaCode.VZACODE));
                                    param.add(new BasicNameValuePair("REFNO", ""));
                                    param.add(new BasicNameValuePair("TEXTMSG", ""));

                                    ArrayList<RequestData> requestData = new ArrayList<>();
                                    requestData.add(new RequestData(masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SERVICE_ID));
                                    param.add(new BasicNameValuePair("REQTYPEIDJSON", new Gson().toJson(requestData)));

                                    String site = hotelInfo.DATA.HOTEL_URL;
                                    String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                                    ServiceUtil serviceUtil = new ServiceUtil(MyBillActivity.this, site, keycode, SiteOperate.VZASaveRequestJSON, param, RequestEntity.class);
                                    serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                        @Override
                                        public void OnFinished(Object results) {
                                            BaseApplication.RequestCheckVariable.set(true);
                                            MyBillActivity.this.finish();
                                        }
                                    });
                                    serviceUtil.execute();
                                }
                            }
                        });
                        checkLocationService.execute();
                    }
                });
                dialog.show(getFragmentManager(), "tag");
            }
        }
    };

    public Button.OnClickListener btnServed_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyBillActivity.this,CheckLocationEntity.class);
            final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyBillActivity.this,HotelInfoEntity.class);

            //Init ExpList
            final ExpandableList expListView = ((ExpandableList)(findViewById(R.id.expBill)));
            List<ServedData> lisServed = new ArrayList<>();
            for(BillOrderData data : ((MyBillAdapter)expListView.getExpandableListAdapter()).getEntity().ITEMS){
                if(data.ORDER_STATUS == 100 || data.ORDER_STATUS == 200){
                    ServedData served = new ServedData();
                    served.ORDER_ID = data.ORDER_ID;
                    served.ORDER_STATUS = 2;
                    lisServed.add(served);
                }
            }

            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("LOCATION_IDPata",checkLocation.LOCATION_ID.toString()));
            param.add(new BasicNameValuePair("ORDER_IDPara",new Gson().toJson(lisServed)));

            String site = hotelInfo.DATA.HOTEL_URL;
            String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
            ServiceUtil serviceUtil = new ServiceUtil(MyBillActivity.this,site,keycode,SiteOperate.VZAUpdateOrderStatusJSON,param, RequestEntity.class);
            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    MyBillActivity.this.finish();
                }
            });
            serviceUtil.execute();
        }
    };

    public Button.OnClickListener btnClose_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

}
