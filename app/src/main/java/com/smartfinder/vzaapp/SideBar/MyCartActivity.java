package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.ExpandableList.ExpandableList;
import control.ExpandableList.MyCartAdapter;
import emuntype.SiteOperate;
import entity.Host.CanOrder.CanOrderData;
import entity.Host.Cart.CheckOrderData;
import entity.Host.Cart.MenuOrderData;
import entity.Host.Cart.MenuOrderEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CanOrder.CanOrderEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.CheckLogin.CheckLoginEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.Notification.NotificationEntity;
import entity.Service.Ordering.OrderingEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.BroadcastUtil;
import util.ServiceUtil;

public class MyCartActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycart);

        //Init Data
        Integer locationID = BaseApplication.GlobalVariable.get(this, CheckLocationEntity.class).LOCATION_ID;
        String locationName = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(locationID).LOCATION_NAME1;
        String currencyDep = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(locationID).CURRENCY_DEP;

        //Init Label
        ((TextView) findViewById(R.id.txtLocation)).setText(locationName);
        ((TextView) findViewById(R.id.txtAllPrice)).setText(currencyDep + new DecimalFormat("#,##0.00").format(BaseApplication.CartStateVariable.getAllPrice()));

        //Init ExpList
        ExpandableList expListView = ((ExpandableList) (findViewById(R.id.expOrder)));
        expListView.setAdapter(new MyCartAdapter(this, currencyDep));

        //Init Event
        (findViewById(R.id.btnClose)).setOnClickListener(btnClose_OnClick);
        (findViewById(R.id.btnConfirm)).setOnClickListener(btnConfirm_OnClick);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private Button.OnClickListener btnClose_OnClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            MyCartActivity.this.finish();
        }
    };

    private Button.OnClickListener btnConfirm_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Init Order Data
            final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyCartActivity.this, HotelInfoEntity.class);
            final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyCartActivity.this, CheckLocationEntity.class);
            final VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(MyCartActivity.this, VZACodeEntity.class);
            final UserInfoEntity userInfoEntity = BaseApplication.GlobalVariable.get(MyCartActivity.this, UserInfoEntity.class);
            MasterEntity masterEntity = BaseApplication.GlobalVariable.get(MyCartActivity.this, MasterEntity.class);

            final String site = hotelInfo.DATA.HOTEL_URL;
            final String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
            /*String vzacode = "";
            if (vzaCodeEntity.VZACODE != "") {
                vzacode = vzaCodeEntity.VZACODE.toString();
            } else {
                vzacode = BaseApplication.GlobalVariable.get(MyCartActivity.this, CheckLoginEntity.class).VZACODE;
            }*/

            final String vzaCode = vzaCodeEntity.VZACODE == "" ? checkLocation.VZACODE : vzaCodeEntity.VZACODE;

            //-- Check VZA Closed or Not
            List<NameValuePair> closeParam = new ArrayList<>();
            closeParam.add(new BasicNameValuePair("HOTEL_IDPara", hotelInfo.DATA.HOTEL_ID.toString()));
            closeParam.add(new BasicNameValuePair("SYSTEMIDPara", String.valueOf(masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SYSTEMID)));
            closeParam.add(new BasicNameValuePair("SENDORRECEIVECODEPara", vzaCodeEntity.VZACODE == "" ? checkLocation.VZACODE : vzaCodeEntity.VZACODE));
            closeParam.add(new BasicNameValuePair("LOCATIONIDPara", checkLocation.LOCATION_ID.toString()));
            closeParam.add(new BasicNameValuePair("LASTRECORDIDPara", "0"));

            ServiceUtil closeSeviceUtil = new ServiceUtil(MyCartActivity.this, site, keycode,
                    SiteOperate.NTFGetTransactionNotificationJSON, closeParam, NotificationEntity.class);
            closeSeviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    NotificationEntity notificationEntity = (NotificationEntity) results;
                    if (!notificationEntity.VZACLOSE) {

                        //Init CanOrder
                        final List<entity.Host.CanOrder.CanOrderData> canOrderData = new ArrayList<>();
                        for (final MenuOrderEntity orderEntity : BaseApplication.CartStateVariable.getAll()) {
                            for (final MenuOrderData orderData : orderEntity.ORDER) {
                                //canOrderData.add(new CanOrderData(Integer.parseInt(orderData.MENU_CODE)));
                                canOrderData.add(new CanOrderData(orderData.MENU_ID));
                            }
                        }

                        List<NameValuePair> canParam = new ArrayList<>();
                        canParam.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                        canParam.add(new BasicNameValuePair("TABLE_NOPara", checkLocation.TABLE_NO.toString()));
                        canParam.add(new BasicNameValuePair("VZA_CODEPara", vzaCodeEntity.VZACODE == "" ? checkLocation.VZACODE : vzaCodeEntity.VZACODE));
                        canParam.add(new BasicNameValuePair("ACCEPT_COVERPara", "0"));
                        canParam.add(new BasicNameValuePair("MENU_IDPara", new Gson().toJson(canOrderData)));

                        final ServiceUtil canSeviceUtil = new ServiceUtil(MyCartActivity.this, site, keycode,
                                SiteOperate.VZACheckCanOrderJSON, canParam, CanOrderEntity.class);
                        canSeviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                            @Override
                            public void OnFinished(Object results) {
                                CanOrderEntity entity = (CanOrderEntity) results;
                                String vzacode = "";
                                if (entity.BILL_DISCOUNT) {
                                    ChooseDialog dialog = ChooseDialog.newInstance(MyCartActivity.this, getString(R.string.alert_order_discount), false);
                                    dialog.show(MyCartActivity.this.getFragmentManager(), "tag");
                                    return;
                                } else if (!entity.TABLE_OPEN) {
                                    ChooseDialog dialog = ChooseDialog.newInstance(MyCartActivity.this, getString(R.string.alert_table_open), false);
                                    dialog.show(MyCartActivity.this.getFragmentManager(), "tag");
                                    return;
                                } else if (entity.ISTABLE_TF) {
                                    checkLocation.TABLE_NO = entity.TABLE_NO_TF;
                                    vzacode = "T" + entity.TABLE_NO_TF;
                                    vzaCodeEntity.VZACODE = vzacode;
                                    checkLocation.VZACODE = vzacode;

                                    //-- Move Tables and change key notification
                                    String[] arrKey = BaseApplication.NotificationRecordVariable._key.split("_");
                                    String key = arrKey[0]+"_"+arrKey[1]+"_"+arrKey[2]+"_"+vzacode;
                                    BaseApplication.NotificationRecordVariable.setVariable(getBaseContext(), key);

                                    //-- Set value is change to global variable
                                    BaseApplication.GlobalVariable.set(checkLocation);
                                    BaseApplication.GlobalVariable.set(vzaCodeEntity);
                                }else {
                                    vzacode = vzaCode;
                                }
                                final String VZACode = vzacode;
                                if (entity.LOCATION_OPEN) {
                                    //-- Alert Confirm Order
                                    ChooseDialog dialog = ChooseDialog.newInstance(MyCartActivity.this, getResources().getString(R.string.alert_comfirm_mycart), true);
                                    dialog.show(getFragmentManager(), "tag");
                                    dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                                        @Override
                                        public void OnSubmit(View view) {
                                            for (final MenuOrderEntity orderEntity : BaseApplication.CartStateVariable.getAll()) {
                                                List<NameValuePair> param = new ArrayList<>();
                                                param.add(new BasicNameValuePair("VZACODEPara", VZACode /*vzaCodeEntity.VZACODE == "" ? checkLocation.VZACODE : vzaCodeEntity.VZACODE*/));
                                                param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                                                param.add(new BasicNameValuePair("TELPara", userInfoEntity.Data.TELEPHONE));
                                                param.add(new BasicNameValuePair("MENUORDERITEMWITHJSONFORMATPara", new Gson().toJson(orderEntity.ORDER)));
                                                String site = hotelInfo.DATA.HOTEL_URL;
                                                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

                                                ServiceUtil serviceUtil = new ServiceUtil(MyCartActivity.this, site, keycode,
                                                        SiteOperate.POSSaveTrnOrderingJSON, param,
                                                        OrderingEntity.class);
                                                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                                    @Override
                                                    public void OnFinished(Object results) {
                                                        BaseApplication.CartStateVariable.clear(orderEntity);
                                                        if (!(BaseApplication.CartStateVariable.length() > 0)) {
                                                            BaseApplication.RequestCheckVariable.set(false);
                                                            MyCartActivity.this.finish();
                                                        }
                                                    }
                                                });
                                                serviceUtil.execute();
                                            }
                                        }
                                    });
                                } else {
                                    ChooseDialog dialog = ChooseDialog.newInstance(MyCartActivity.this, getString(R.string.alert_vzacode_closed), false);
                                    dialog.show(MyCartActivity.this.getFragmentManager(), "tag");
                                    return;
                                }
                            }
                        });
                        canSeviceUtil.execute();
                    } else {
                        ChooseDialog dialog = ChooseDialog.newInstance(MyCartActivity.this, getString(R.string.alert_vzacode_closed), false);
                        dialog.show(MyCartActivity.this.getFragmentManager(), "tag");
                        return;
                    }
                }
            });
            closeSeviceUtil.execute();
        }
    };
}
