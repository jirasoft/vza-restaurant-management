package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.AdsMenuActivity;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.ExpandableList.ExpandableList;
import control.ExpandableList.MyDocAdapter;
import control.ExpandableList.MyFolioAdapter;
import emuntype.SiteOperate;
import entity.Host.FollowUp.FollowUpData;
import entity.Host.Request.RequestData;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.BillOrder.BillOrderData;
import entity.Service.BillOrder.BillOrderDataAddition;
import entity.Service.BillOrder.BillOrderEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.FolioOrder.FolioOrderData;
import entity.Service.FolioOrder.FolioOrderDataDetail;
import entity.Service.FolioOrder.FolioOrderEntity;
import entity.Service.FollowUp.FollowUpEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.Request.RequestEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class MyDocActivity extends Activity {

    //Footer View
    private View Footer;
    private BillOrderEntity billEntity;
    private FolioOrderEntity folioEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydoc);

        //Init View
        Footer = new View(MyDocActivity.this);

        //Set Check Button
        if(BaseApplication.RequestCheckVariable.get()){
            ((Button)findViewById(R.id.btnCheck)).setText(getString(R.string.btn_wait));
            findViewById(R.id.btnCheck).setClickable(false);
        }else{
            ((Button)findViewById(R.id.btnCheck)).setText(getString(R.string.btn_checkbill));
            findViewById(R.id.btnCheck).setClickable(true);
        }

        //Init Event
        ((RadioGroup)findViewById(R.id.rgpList)).setOnCheckedChangeListener(rgpList_OnCheckedChange);
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);
        findViewById(R.id.btnMyBill).setOnClickListener(grpList_OnClick);
        findViewById(R.id.btnMyFolio).setOnClickListener(grpList_OnClick);
        findViewById(R.id.btnMyBill).performClick();
    }

    public Button.OnClickListener grpList_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toggle Button
            ((RadioGroup)view.getParent()).check(view.getId());

            //Set To Layout
            if(((RadioGroup)view.getParent()).getCheckedRadioButtonId() == R.id.btnMyBill){

                //Init ExpList
                final ExpandableList expListView = ((ExpandableList)(findViewById(R.id.expBill)));
                final Integer SummaryListVisible = findViewById(R.id.frmSummaryList).getVisibility();

                findViewById(R.id.frmMyBill).setVisibility(View.VISIBLE);
                findViewById(R.id.frmMyFolio).setVisibility(View.GONE);

                if(expListView.getFooterViewsCount() == 0) {
                    findViewById(R.id.frmSummaryList).setVisibility(View.VISIBLE);
                }

                //Init Data
                HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyDocActivity.this,HotelInfoEntity.class);
                CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(getBaseContext(), CheckLocationEntity.class);
                VZACodeEntity vzaEntity = BaseApplication.GlobalVariable.get(getBaseContext(), VZACodeEntity.class);
                MasterEntity masterEntity = (MasterEntity) BaseApplication.GlobalVariable.get(MyDocActivity.this, MasterEntity.class);
                Integer locationID = locationEntity.LOCATION_ID;
                String locationName = masterEntity.FindByLocationID(locationID).LOCATION_NAME1;
                String VZACode = vzaEntity.VZACODE == "" ? locationEntity.VZACODE : vzaEntity.VZACODE;
                final String currencyDep = masterEntity.FindByLocationID(locationID).CURRENCY_DEP;

                //Init Label
                ((TextView)findViewById(R.id.txtLocation)).setText(locationName);

                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("LOCATION_IDPara",locationID.toString()));
                param.add(new BasicNameValuePair("VZACODEPara",VZACode));
                String site = hotelInfo.DATA.HOTEL_URL;
                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

                ServiceUtil serviceUtil = new ServiceUtil(MyDocActivity.this, site, keycode,
                        SiteOperate.POSgetTrnOrderingbyVZACODEJSON, param,
                        BillOrderEntity.class);
                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {

                        //Init Adapter
                        billEntity = (BillOrderEntity)results;
                        expListView.setAdapter(new MyDocAdapter(MyDocActivity.this,billEntity,currencyDep));

                        //Init Space
                        if(expListView.getFooterViewsCount() == 0) {
                            //Init frmSummaryList Status
                            findViewById(R.id.frmSummaryList).setVisibility(SummaryListVisible);

                            //Init line Space
                            //Init View
                            View FooterSpace = new View(MyDocActivity.this);
                            final float scale = getResources().getDisplayMetrics().density;
                            FooterSpace.setLayoutParams(new AbsListView.LayoutParams(MATCH_PARENT, (int) (scale + 0.5f)));
                            FooterSpace.setBackgroundResource(R.color.bg);
                            expListView.addFooterView(FooterSpace, null, false);

                            Footer.setLayoutParams(new AbsListView.LayoutParams(MATCH_PARENT, findViewById(R.id.btnSummary).getHeight()));
                            expListView.addFooterView(Footer, null, false);
                        }

                        //Init Remark
                        TextView txtRemark = (TextView)findViewById(R.id.txtSummaryDesc);
                        txtRemark.setText(billEntity.MESSAGE_ONFOOTERBILL);

                        //Init Total Value
                        TextView txtAllPrice = (TextView)findViewById(R.id.txtAllPrice);
                        TextView txtNetPrice = (TextView)findViewById(R.id.txtNet);
                        TextView txtDiscountPrice = (TextView)findViewById(R.id.txtDiscount);
                        TextView txtService = (TextView)findViewById(R.id.txtService);
                        TextView txtVatPrice = (TextView)findViewById(R.id.txtVat);

                        if (billEntity.TOTAL == 0.00){
                            double allPrice = 0.00;
                            for(BillOrderData orderData : billEntity.ITEMS){
                                double orderPrice = 0.00;
                                if(!orderData.ADDITIONALDETAIL.isEmpty()){
                                    for(BillOrderDataAddition addition : orderData.ADDITIONALDETAIL){
                                        orderPrice = orderPrice + addition.ADDITIONAL_UNITPRICE;
                                    }
                                }
                                orderPrice = (orderPrice + orderData.MENU_UNITPRICE) * orderData.ORDER_QTY;
                                allPrice = allPrice + orderPrice;
                            }
                            txtAllPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(allPrice));
                            txtNetPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(allPrice));
                            txtDiscountPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(0.00));
                            txtService.setText(currencyDep + new DecimalFormat("#,##0.00").format(0.00));
                            txtVatPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(0.00));
                        }else{
                            txtAllPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.TOTAL));
                            txtNetPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.NET));
                            txtDiscountPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.DISCOUNT));
                            txtService.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.SERVICE));
                            txtVatPrice.setText(currencyDep + new DecimalFormat("#,##0.00").format(billEntity.VAT));
                        }

                        //Init Event
                        findViewById(R.id.btnSummary).setOnClickListener(btnSummary_OnClick);
                        findViewById(R.id.btnFollowUp).setOnClickListener(btnFollowUp_OnClick);
                        findViewById(R.id.btnCheck).setOnClickListener(btnCheck_OnClick);
                    }
                });
                serviceUtil.execute();
            }else{
                //Init ExpList
                final ExpandableList expListView = ((ExpandableList)(findViewById(R.id.expFolio)));

                findViewById(R.id.frmMyFolio).setVisibility(View.VISIBLE);
                findViewById(R.id.frmMyBill).setVisibility(View.GONE);

                //Init Data
                HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyDocActivity.this,HotelInfoEntity.class);
                Integer locationID = BaseApplication.GlobalVariable.get(MyDocActivity.this, CheckLocationEntity.class).LOCATION_ID;
                String VZACode = BaseApplication.GlobalVariable.get(MyDocActivity.this, VZACodeEntity.class).VZACODE;
                final String currencyDep = BaseApplication.GlobalVariable.get(MyDocActivity.this, MasterEntity.class).FindByLocationID(locationID).CURRENCY_DEP;

                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("VZACODEPara",VZACode));
                String site = hotelInfo.DATA.HOTEL_URL;
                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

                ServiceUtil serviceUtil = new ServiceUtil(MyDocActivity.this, site, keycode,
                        SiteOperate.HMSgetFolioDetailJSON, param,
                        FolioOrderEntity.class);

                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        folioEntity = (FolioOrderEntity)results;
                        expListView.setAdapter(new MyFolioAdapter(MyDocActivity.this,currencyDep,folioEntity));

                        double allPrice = 0.00;
                        for(FolioOrderData orderData : folioEntity.Folio){
                            double orderPrice = 0.00;
                            if(!orderData.Detail.isEmpty()){
                                for(FolioOrderDataDetail addition : orderData.Detail){
                                    orderPrice = orderPrice + addition.AMOUNT;
                                }
                            }
                            allPrice = allPrice + orderPrice;
                        }
                        ((TextView)findViewById(R.id.txtFolioPrice)).setText(currencyDep + new DecimalFormat("#,##0.00").format(allPrice));

                        //Init Event
                        findViewById(R.id.btnCheckOut).setOnClickListener(btnCheckOut_OnClick);

                    }
                });
                serviceUtil.execute();
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener rgpList_OnCheckedChange = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            for (int j = 0; j < group.getChildCount(); j++) {
                final Button view = (Button) group.getChildAt(j);
                view.setSelected(view.getId() == checkedId);
            }
        }
    };

    public ImageButton.OnClickListener btnSummary_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
           final LinearLayout frmSummaryList = (LinearLayout)findViewById(R.id.frmSummaryList);
           final LinearLayout frmBillSummary = (LinearLayout)findViewById(R.id.frmBillSummaryCtrl);
            if(frmSummaryList.getVisibility() == View.VISIBLE){
                //Init Button Animation
                TranslateAnimation frmAnimate = new TranslateAnimation(0,0,0,frmSummaryList.getHeight());
                frmAnimate.setDuration(500);
                frmAnimate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        ((ImageButton)view).setImageResource(R.drawable.pic_arrowup);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        frmSummaryList.setVisibility(View.GONE);
                        Footer.setLayoutParams( new AbsListView.LayoutParams(MATCH_PARENT,view.getHeight()));
                    }
                });
                frmBillSummary.startAnimation(frmAnimate);
            }else{
                //Init Button Animation
                TranslateAnimation frmAnimate = new TranslateAnimation(0,0,frmSummaryList.getHeight(),0);
                frmAnimate.setDuration(500);
                frmAnimate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        ((ImageButton) view).setImageResource(R.drawable.pic_arrowdown);
                        frmSummaryList.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Footer.setLayoutParams( new AbsListView.LayoutParams(MATCH_PARENT,frmBillSummary.getHeight()));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                frmBillSummary.startAnimation(frmAnimate);
            }
        }
    };

    public Button.OnClickListener btnCheckOut_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ChooseDialog dialog = ChooseDialog.newInstance(MyDocActivity.this,getResources().getString(R.string.alert_comfirm_checkout), true);
            dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                @Override
                public void OnSubmit(View view) {
                    final VZACodeEntity vzaCode = BaseApplication.GlobalVariable.get(MyDocActivity.this,VZACodeEntity.class);
                    final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyDocActivity.this,CheckLocationEntity.class);
                    final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyDocActivity.this,HotelInfoEntity.class);
                                List<NameValuePair> param = new ArrayList<>();
                                param.add(new BasicNameValuePair("LOCATIONPara",checkLocation.LOCATION_ID.toString()));
                                param.add(new BasicNameValuePair("SYSTEMID",folioEntity.SYSTEMID.toString()));
                                param.add(new BasicNameValuePair("VZACODEPara",vzaCode.VZACODE));
                                param.add(new BasicNameValuePair("REFNO",""));
                                param.add(new BasicNameValuePair("TEXTMSG",""));

                                ArrayList<RequestData> requestData = new ArrayList<>();
                                requestData.add(new RequestData(folioEntity.SERVICE_ID));
                                param.add(new BasicNameValuePair("REQTYPEIDJSON", new Gson().toJson(requestData)));

                                String site = hotelInfo.DATA.HOTEL_URL;
                                String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                                ServiceUtil serviceUtil = new ServiceUtil(MyDocActivity.this,site,keycode,SiteOperate.VZASaveRequestJSON,param, RequestEntity.class);
                                serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                                    MyDocActivity.this.finish();
                        }
                    });
                    serviceUtil.execute();

                }
            });
            dialog.show(getFragmentManager(), "tag");
        }
    };

    public Button.OnClickListener btnFollowUp_OnClick = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyDocActivity.this,CheckLocationEntity.class);
            final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyDocActivity.this,HotelInfoEntity.class);

            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("LOCATION_IDPata",checkLocation.LOCATION_ID.toString()));
            List<FollowUpData> reqFollowUp = new ArrayList<>();
            for(BillOrderData orderData : billEntity.ITEMS){
                if(orderData.ORDER_STATUS != 2 && orderData.ORDER_STATUS != 3) {
                    reqFollowUp.add(new FollowUpData(orderData.ORDER_ID));
                }
            }
            param.add(new BasicNameValuePair("ORDER_IDPara",new Gson().toJson(reqFollowUp)));
            String site = hotelInfo.DATA.HOTEL_URL;
            String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

            ServiceUtil serviceUtil = new ServiceUtil(MyDocActivity.this,site,keycode,SiteOperate.VZAUpdateOrderFollowUpJSON,param, FollowUpEntity.class);
            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    ChooseDialog dialog = ChooseDialog.newInstance(MyDocActivity.this,getResources().getString(R.string.alert_followup_success), false);
                    dialog.show(getFragmentManager(), "tag");
                    dialog.setOnDismissListener(new ChooseDialog.OnDismiss() {
                        @Override
                        public void OnDismiss() {
                            MyDocActivity.this.finish();
                        }
                    });

                }
            });
            serviceUtil.execute();
        }
    };

    public Button.OnClickListener btnCheck_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //-- If "Please Wait.." not check bill..
            if(!((Button)findViewById(R.id.btnCheck)).getText().equals(getString(R.string.btn_wait))) {
                ChooseDialog dialog = ChooseDialog.newInstance(MyDocActivity.this, getResources().getString(R.string.alert_check_bill), true);
                dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                    @Override
                    public void OnSubmit(View view) {
                        final VZACodeEntity vzaCode = BaseApplication.GlobalVariable.get(MyDocActivity.this, VZACodeEntity.class);
                        final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyDocActivity.this, CheckLocationEntity.class);
                        final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyDocActivity.this, HotelInfoEntity.class);

                        List<NameValuePair> param = new ArrayList<NameValuePair>();
                        param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));
                        param.add(new BasicNameValuePair("VZA_CODEPara", vzaCode.VZACODE));
                        String site = hotelInfo.DATA.HOTEL_URL;
                        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                        ServiceUtil checkLocationService = new ServiceUtil(MyDocActivity.this, site, keycode, SiteOperate.VZACheckLocationOpenedJSON, param, CheckLocationEntity.class);
                        checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
                            @Override
                            public void OnFinished(Object results) {
                                if (((CheckLocationEntity) results).RESULT) {

                                    //Keep checkLocation To AppVariable
                                    BaseApplication.GlobalVariable.set((CheckLocationEntity) results);
                                    MasterEntity masterEntity = BaseApplication.GlobalVariable.get(MyDocActivity.this, MasterEntity.class);
                                    List<NameValuePair> param = new ArrayList<>();
                                    param.add(new BasicNameValuePair("LOCATIONPara", checkLocation.LOCATION_ID.toString()));
                                    param.add(new BasicNameValuePair("SYSTEMID", masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SYSTEMID.toString()));
                                    param.add(new BasicNameValuePair("VZACODEPara", vzaCode.VZACODE));
                                    param.add(new BasicNameValuePair("REFNO", ""));
                                    param.add(new BasicNameValuePair("TEXTMSG", ""));

                                    ArrayList<RequestData> requestData = new ArrayList<>();
                                    requestData.add(new RequestData(masterEntity.FindByLocationID(checkLocation.LOCATION_ID).SERVICE_ID));
                                    param.add(new BasicNameValuePair("REQTYPEIDJSON", new Gson().toJson(requestData)));

                                    String site = hotelInfo.DATA.HOTEL_URL;
                                    String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
                                    ServiceUtil serviceUtil = new ServiceUtil(MyDocActivity.this, site, keycode, SiteOperate.VZASaveRequestJSON, param, RequestEntity.class);
                                    serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                                        @Override
                                        public void OnFinished(Object results) {
                                            BaseApplication.RequestCheckVariable.set(true);
                                            MyDocActivity.this.finish();
                                        }
                                    });
                                    serviceUtil.execute();
                                }
                            }
                        });
                        checkLocationService.execute();
                    }
                });
                dialog.show(getFragmentManager(), "tag");
            }
        }
    };

    public Button.OnClickListener btnClose_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

}
