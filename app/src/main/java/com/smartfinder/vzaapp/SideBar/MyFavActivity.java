package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.Menu.ItemActivity;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.MenuList.LikeUnlikeAdapter;
import control.MenuList.MenuItemAdapter;
import control.MenuList.MenuList;
import emuntype.SiteOperate;
import entity.Host.Cart.CheckOrderData;
import entity.Host.Cart.MenuOrderData;
import entity.Host.Cart.MenuOrderDataAddition;
import entity.Host.Cart.MenuOrderEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.CheckOrder.CheckOrderEntity;
import entity.Service.Favorite.FavoriteEntity;
import entity.Service.Master.MasterDetailList;
import entity.Service.Master.MasterEntity;
import entity.Service.MasterItem.MasterItemData;
import entity.Service.MasterItem.MasterItemEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

public class MyFavActivity  extends Activity {

    private FavoriteEntity entity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myfav);

        //Init Event
        ((RadioGroup)findViewById(R.id.rgpList)).setOnCheckedChangeListener(rgpList_OnCheckedChange);
        findViewById(R.id.btnListView).setOnClickListener(grpList_OnClick);
        findViewById(R.id.btnGridView).setOnClickListener(grpList_OnClick);
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);

        //Init ListView
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(this,HotelInfoEntity.class);
        VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(this,VZACodeEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(this,CheckLocationEntity.class);

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
        param.add(new BasicNameValuePair("VZACODEPara",vzaCodeEntity.VZACODE));
        param.add(new BasicNameValuePair("USER_EMAILPara",""));

        String site = hotelInfo.DATA.HOTEL_URL;
        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

        ServiceUtil serviceUtil = new ServiceUtil(this, site, keycode,
                SiteOperate.VZAGetFavorite_Like_UnLikeDataJSON, param,
                FavoriteEntity.class);

        serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                entity = (FavoriteEntity)results;
                findViewById(R.id.btnListView).performClick();
            }
        });
        serviceUtil.execute();
    }

    private Button.OnClickListener btnClose_OnClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            MyFavActivity.this.finish();
        }
    };

    public Button.OnClickListener grpList_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toggle Button
            ((RadioGroup)view.getParent()).check(view.getId());

            List<MasterItemData> lisItem = new ArrayList<>();

            //Filter True
            for(MasterItemData item : entity.LikeUnlikedataList){
                if(item.IS_FAVORITE){
                    lisItem.add(item);
                }
            }

            MenuItemAdapter serviceAdapter;
            MenuList lisMenu;
            if(((RadioGroup)view.getParent()).getCheckedRadioButtonId() == R.id.btnListView){
                serviceAdapter = new MenuItemAdapter(MyFavActivity.this,lisItem,R.layout.adapter_menu_itemgroup_list);
                lisMenu = (MenuList) MyFavActivity.this.findViewById(R.id.lisItem);
                lisMenu.setNumColumns(getResources().getInteger(R.integer.MENULIST_LISTMODE_COLSIZE));
                lisMenu.setRowSize(getResources().getDimension(R.dimen.MENULIST_LISTMODE_ROWSIZE));
                lisMenu.setAdapter(serviceAdapter);
            }else{
                serviceAdapter = new MenuItemAdapter(MyFavActivity.this,lisItem,R.layout.adapter_menu_itemgroup_grid);
                lisMenu = (MenuList) MyFavActivity.this.findViewById(R.id.lisItem);
                lisMenu.setNumColumns(getResources().getInteger(R.integer.MENULIST_GRIDMODE_COLSIZE));
                lisMenu.setRowSize(getResources().getDimension(R.dimen.MENULIST_GRIDMODE_ROWSIZE));
                lisMenu.setAdapter(serviceAdapter);
            }
            serviceAdapter.setOnItemClickListener(onToCartClick);
            serviceAdapter.setOnItemClickListener(onItemClick);
        }
    };

    private MenuItemAdapter.OnItemClick onItemClick = new MenuItemAdapter.OnItemClick() {
        @Override
        public void OnItemClick(MasterItemData model) {
            //Init Menu
            Intent intent = new Intent(MyFavActivity.this,ItemActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("itemID", model.ID);
            bundle.putBoolean("isMenuSet",false);
            bundle.putString("favItem",new Gson().toJson(model));
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    private MenuItemAdapter.OnToCartClick onToCartClick = new MenuItemAdapter.OnToCartClick() {
        @Override
        public void OnToCartClick(final MasterItemData model) {
            //Init Order Check
            entity.Host.Cart.CheckOrderEntity menuOrders = new entity.Host.Cart.CheckOrderEntity();
            menuOrders.CHECKORDER.add(new CheckOrderData(model.ID));

            //Init Data
            HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyFavActivity.this,HotelInfoEntity.class);
            CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyFavActivity.this,CheckLocationEntity.class);

            //Call Service Check Order
            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
            param.add(new BasicNameValuePair("MENU_IDPara",new Gson().toJson(menuOrders.CHECKORDER)));

            String site = hotelInfo.DATA.HOTEL_URL;
            String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
            final ServiceUtil serviceUtil = new ServiceUtil(MyFavActivity.this,site,keycode, SiteOperate.VZAGetCheckOrderSTOPSOLDJSON,
                    param, CheckOrderEntity.class);

            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    CheckOrderEntity orderEntity = (CheckOrderEntity) results;
                    List<entity.Service.CheckOrder.CheckOrderData> listCheck = new ArrayList<>(orderEntity.MENUList);
                    Iterator<entity.Service.CheckOrder.CheckOrderData> menuList = listCheck.iterator();
                    while (menuList.hasNext()){
                        if(menuList.next().CAN_ORDER)
                            menuList.remove();
                    }

                    if(!listCheck.isEmpty()){
                        ChooseDialog dialog = ChooseDialog.newInstance(MyFavActivity.this,getString(R.string.alert_cant_order), false);
                        dialog.show(MyFavActivity.this.getFragmentManager(), "tag");
                    }else{
                        MenuOrderData OrderItem = new MenuOrderData(model.MENU_MENU_ID, model.MENU_CODE,model.MENU_NAME1,model.MENU_SIZE,1.00,
                                model.MENU_UNITPRICE,"",new ArrayList<MenuOrderDataAddition>());
                        //BaseApplication.CartStateVariable.add(new MenuOrderEntity(model.MENU_NAME1,OrderItem));
                        BaseApplication.CartStateVariable.add(new MenuOrderEntity(model.MENU_NAME1, model.MENU_UNITPRICE, OrderItem));
                    }
                }
            });
            serviceUtil.execute();
        }
    };

    private RadioGroup.OnCheckedChangeListener rgpList_OnCheckedChange = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            for (int j = 0; j < group.getChildCount(); j++) {
                final Button view = (Button) group.getChildAt(j);
                view.setSelected(view.getId() == checkedId);
            }
        }
    };
}
