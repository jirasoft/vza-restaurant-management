package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.MenuList.LikeUnlikeAdapter;
import control.MenuList.MenuList;
import emuntype.SiteOperate;
import entity.BaseEntity;
import entity.Host.Favorite.FavoriteData;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Favorite.FavoriteEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.MasterItem.MasterItemData;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

public class MyLikeActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylike);

        //Init ListView
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(this,HotelInfoEntity.class);
        VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(this,VZACodeEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(this,CheckLocationEntity.class);

        //Init Location Text
        ((TextView)findViewById(R.id.txtLocation)).setText(BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(checkLocation.LOCATION_ID).LOCATION_NAME1);

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
        param.add(new BasicNameValuePair("VZACODEPara",vzaCodeEntity.VZACODE));
        param.add(new BasicNameValuePair("USER_EMAILPara",""));

        String site = hotelInfo.DATA.HOTEL_URL;
        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

        ServiceUtil serviceUtil = new ServiceUtil(this, site, keycode,
                SiteOperate.VZAGetFavorite_Like_UnLikeDataJSON, param,
                FavoriteEntity.class);

        serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                FavoriteEntity entity = (FavoriteEntity)results;
                MenuList menuList = (MenuList)findViewById(R.id.lisItem);
                LikeUnlikeAdapter adapter = new LikeUnlikeAdapter(MyLikeActivity.this,entity.LikeUnlikedataList);
                menuList.setAdapter(adapter);
            }
        });
        serviceUtil.execute();

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);

    }
    private Button.OnClickListener btnClose_OnClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {

            //Check Data Change List
            MenuList menuList = (MenuList)findViewById(R.id.lisItem);
            final List<MasterItemData> results = ((LikeUnlikeAdapter)menuList.getAdapter()).getEntityChange();
            if(results.size() > 0) {
                ChooseDialog dialog = ChooseDialog.newInstance(MyLikeActivity.this, getResources().getString(R.string.alert_comfirm_mylike), true);
                dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                    @Override
                    public void OnSubmit(View view) {

                        //Init ListView
                        UserInfoEntity userInfo = BaseApplication.GlobalVariable.get(MyLikeActivity.this,UserInfoEntity.class);
                        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(MyLikeActivity.this,HotelInfoEntity.class);
                        VZACodeEntity vzaCodeEntity = BaseApplication.GlobalVariable.get(MyLikeActivity.this,VZACodeEntity.class);
                        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(MyLikeActivity.this,CheckLocationEntity.class);
                        //Init Result
                        List<FavoriteData> paramSrc = new ArrayList<>();
                        for(MasterItemData result : results){
                            paramSrc.add(new FavoriteData(result.MENU_MENU_ID,result.COMMENT_DEP,result.IS_LIKE,result.IS_UNLIKE,result.IS_FAVORITE));
                        }

                        List<NameValuePair> param = new ArrayList<>();
                        param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
                        param.add(new BasicNameValuePair("VAZACODEPara",vzaCodeEntity.VZACODE));
                        param.add(new BasicNameValuePair("EMAILPara",userInfo.Data.EMAIL));
                        param.add(new BasicNameValuePair("MENU_IDPara",new Gson().toJson(paramSrc)));

                        String site = hotelInfo.DATA.HOTEL_URL;
                        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

                        ServiceUtil serviceUtil = new ServiceUtil(MyLikeActivity.this, site, keycode,
                                SiteOperate.VZAUpdate_FAVORITE_LIKE_UNLIKEJSON, param,
                                BaseEntity.class);
                        serviceUtil.execute();
                    }
                });
                dialog.setOnDismissListener(new ChooseDialog.OnDismiss() {
                    @Override
                    public void OnDismiss() {
                        finish();
                    }
                });
                dialog.show(getFragmentManager(), "tag");
            }else{
                finish();
            }
        }
    };
}
