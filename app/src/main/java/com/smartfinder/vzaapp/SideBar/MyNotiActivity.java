package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.TextView;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import control.ExpandableList.ExpandableList;
import control.ExpandableList.MyNotiAdapter;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class MyNotiActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mynoti);

        //Init Data
        Integer locationID = BaseApplication.GlobalVariable.get(this, CheckLocationEntity.class).LOCATION_ID;
        String locationName = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(locationID).LOCATION_NAME1;

        //Init Label
        ((TextView)findViewById(R.id.txtLocation)).setText(locationName);

        //Init ExpandableList
        ExpandableList expandableList = (ExpandableList)findViewById(R.id.lisItem);
        MyNotiAdapter adapter = new MyNotiAdapter(this);
        expandableList.setAdapter(adapter);

        //-- Set Selection Last Records
        expandableList.setSelection(expandableList.getCount()-1);

        View Footer = new View(MyNotiActivity.this);
        Footer.setLayoutParams(new AbsListView.LayoutParams(MATCH_PARENT, expandableList.getDividerHeight()));
        expandableList.addFooterView(Footer, null, false);

        //Init Event
        (findViewById(R.id.btnClose)).setOnClickListener(btnClose_OnClick);
    }

    private Button.OnClickListener btnClose_OnClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            int totalRecord = BaseApplication.NotificationRecordVariable.getTotalRecord(getBaseContext());
            BaseApplication.NotificationRecordVariable.setVariable(getBaseContext(),
                    totalRecord,
                    totalRecord);

            MyNotiActivity.this.finish();
        }
    };
}
