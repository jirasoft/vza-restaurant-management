package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.ViewList.TableListAdapter;
import control.ViewList.ViewList;
import emuntype.SiteOperate;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.TableList.TableListData;
import entity.Service.TableList.TableListEntity;
import entity.Service.VZACode.VZACodeEntity;
import util.ServiceUtil;

public class TableListActivity extends Activity {

    // Declare Variables
    EditText txtSearch;
    ViewList listView;
    TableListAdapter tableListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablelist);

        //Init List
        Integer locationID = BaseApplication.GlobalVariable.get(this, CheckLocationEntity.class).LOCATION_ID;
        String locationName = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(locationID).LOCATION_NAME1;

        //Init
        ((TextView) findViewById(R.id.txtLocation)).setText(locationName);
        txtSearch = (EditText) findViewById(R.id.txtSearch);

        //Init Event
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);
        txtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                //String search = txtSearch.getText().toString();
                //tableListAdapter.getFilter().filter(search.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                tableListAdapter.getFilter().filter(arg0);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Integer locationID = BaseApplication.GlobalVariable.get(this, CheckLocationEntity.class).LOCATION_ID;
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(this, HotelInfoEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(this, CheckLocationEntity.class);
        MasterEntity masterEntity = BaseApplication.GlobalVariable.get(this, MasterEntity.class);
        Integer locationID = checkLocation.LOCATION_ID;
        String SystemID = masterEntity.FindByLocationID(locationID).SYSTEMID.toString();
        final String currencyDep = masterEntity.FindByLocationID(locationID).CURRENCY_DEP;

        //String SystemID = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(locationID).SYSTEMID.toString();
        //final String currencyDep = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(locationID).CURRENCY_DEP;

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("SYSTEMIDPara", SystemID));
        param.add(new BasicNameValuePair("LOCATION_IDPara", locationID.toString()));

        String site = hotelInfo.DATA.HOTEL_URL;
        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;
        ServiceUtil serviceUtil = new ServiceUtil(this, site, keycode, SiteOperate.POSgetTableListForStaffJSON, param, TableListEntity.class);
        serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                TableListEntity entity = (TableListEntity) results;

                //-- Global Variable
                BaseApplication.GlobalVariable.set(entity);

                //-- List Adapter
                listView = (ViewList) findViewById(R.id.lisTableList);
                tableListAdapter = new TableListAdapter(TableListActivity.this, entity.TABLEDATALIST, currencyDep);
                listView.setAdapter(tableListAdapter);
                listView.setTextFilterEnabled(true);
            }
        });
        serviceUtil.execute();
    }

    public Button.OnClickListener btnClose_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

}
