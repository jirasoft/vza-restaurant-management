package com.smartfinder.vzaapp.SideBar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.ViewList.VZAListAdapter;
import control.ViewList.VZAListModel;
import control.ViewList.ViewList;
import emuntype.SiteOperate;
import entity.Host.TableList.TableListVZAData;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.TableList.*;
import entity.Service.VZAOpen.VZAOpenEntity;
import util.ServiceUtil;

public class TableVZAListActivity extends Activity {

    //private TableListEntity tableList;
    private ArrayList<TableListDataVZAList> vzaList;
    private String tableNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_vzalist);

        //Init Event
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            tableNo = bundle.getString("TABLE_NO");
            vzaList = (ArrayList<TableListDataVZAList>) bundle.getSerializable("VZA_LIST");
            ((TextView) findViewById(R.id.txtTitle)).setText(getString(R.string.table_header_table_no) + " - " + tableNo);
        }
        findViewById(R.id.btnClose).setOnClickListener(btnClose_OnClick);
        findViewById(R.id.btnCloseVZA).setOnClickListener(btnCloseVZA_OnClick);

        //Set To Adapter
        List<VZAListModel> itemVZA = new ArrayList<>();
        for(TableListDataVZAList vzaItem : vzaList){
            itemVZA.add(new  VZAListModel<TableListDataVZAList>().setItem(
                    vzaItem.TABLE_NO,
                    vzaItem.GUEST_NAME,
                    vzaItem.TELEPHONE_NO,
                    vzaItem.VZACODE,
                    true,
                    false,
                    vzaItem
            ));
        }

        VZAListAdapter serviceAdapter = new VZAListAdapter(this,R.id.txtGuestName,itemVZA);
        //serviceAdapter.setOnSelectedChangeListener(serviceAdapter_onSelectedChange);
        ViewList lisView = (ViewList) findViewById(R.id.listVZA);
        lisView.setAdapter(serviceAdapter);
    }

    /*private VZAListAdapter.OnSelectedChange serviceAdapter_onSelectedChange = new VZAListAdapter.OnSelectedChange() {
        @Override
        public void OnSelectedChange() {
            //onCalculate();
        }
    };*/

    public Button.OnClickListener btnClose_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //finish();
            TableVZAListActivity.this.finish();
        }
    };

    public Button.OnClickListener btnCloseVZA_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                //Init Data
                final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(TableVZAListActivity.this,HotelInfoEntity.class);
                final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(TableVZAListActivity.this,CheckLocationEntity.class);

                final String site = hotelInfo.DATA.HOTEL_URL;
                final String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

                List<TableListVZAData> listVZA = new ArrayList<>();
                List<VZAListModel> itemModels = ((VZAListAdapter) ((ViewList) findViewById(R.id.listVZA)).getAdapter()).getItems();

                //-- Is select vza items
                for (VZAListModel itemVZA : itemModels) {
                    if (itemVZA.getIsCheck()) {
                        TableListVZAData item = new TableListVZAData();
                        item.TABLE_NOORROOM_NO = itemVZA.getTableNo();
                        item.TELEPHONE_NO = itemVZA.getTelephoneNo();
                        listVZA.add(item);
                    }
                }

                //-- Check is select vza items
                if(listVZA.size()>0){
                    //Call Service
                    List<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("SYSTEMIDPara", BaseApplication.GlobalVariable.get(TableVZAListActivity.this,
                            MasterEntity.class).FindByLocationID(checkLocation.LOCATION_ID).SYSTEMID.toString()));
                    param.add(new BasicNameValuePair("LOCATION_IDPara",checkLocation.LOCATION_ID.toString()));
                    param.add(new BasicNameValuePair("TABLEORROOMNOORTELNOPara",new Gson().toJson(listVZA)));

                    ServiceUtil serviceUtil = new ServiceUtil(TableVZAListActivity.this, site, keycode, SiteOperate.VZACloseJSON,
                            param, VZAOpenEntity.class);
                    serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                            VZAOpenEntity entity = (VZAOpenEntity) results;
                            if(entity.ERRORMSG.equals("")){
                                TableVZAListActivity.this.finish();
                            }else{
                                ChooseDialog dialog = ChooseDialog.newInstance(TableVZAListActivity.this, entity.ERRORMSG, false);
                                dialog.show(TableVZAListActivity.this.getFragmentManager(), "tag");
                            }
                        }
                    });
                    serviceUtil.execute();
                }else{
                    ChooseDialog dialog = ChooseDialog.newInstance(TableVZAListActivity.this, getString(R.string.alert_vzalist_no_select_data), false);
                    dialog.show(TableVZAListActivity.this.getFragmentManager(), "tag");
                }
            }catch (Exception ex){
                ChooseDialog dialog = ChooseDialog.newInstance(TableVZAListActivity.this, ex.getMessage(), false);
                dialog.show(TableVZAListActivity.this.getFragmentManager(), "tag");
            }

        }
    };

}
