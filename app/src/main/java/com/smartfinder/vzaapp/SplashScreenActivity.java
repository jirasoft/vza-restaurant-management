package com.smartfinder.vzaapp;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.VideoView;

public class SplashScreenActivity extends Activity {

    private int INIT_TIME = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Init Page
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        onPlayVdo(INIT_TIME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Pause Vdo
        VideoView vdoSplashScreen = (VideoView) findViewById(R.id.vdoSplashScreen);
        vdoSplashScreen.pause();
        //Get Position time
        INIT_TIME = vdoSplashScreen.getCurrentPosition();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Resume Vdo
        onPlayVdo(INIT_TIME);
    }
    private void onFinishScreen(){
        if(isFinishing()){
            return;
        }else{
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }
    private void onPlayVdo(int seekTo){
        VideoView vdoSplashScreen = (VideoView) findViewById(R.id.vdoSplashScreen);
        //Get vdo Source form raw folder and check rotate
        try{
            Uri vdoUri;
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                vdoUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.vza_ssl);
            }else{
                vdoUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.vza_ss);
            }
            vdoSplashScreen.setVideoURI(vdoUri);
            vdoSplashScreen.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    onFinishScreen();
                }
            });
            vdoSplashScreen.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    onFinishScreen();
                    return true;
                }
            });
            vdoSplashScreen.requestFocus();
            vdoSplashScreen.seekTo(seekTo);
            vdoSplashScreen.start();
        }catch (Exception e){
            onFinishScreen();
        }
    }
}
