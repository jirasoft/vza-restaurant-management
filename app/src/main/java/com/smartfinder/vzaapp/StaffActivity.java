package com.smartfinder.vzaapp;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.Menu.MainFragment;
import com.smartfinder.vzaapp.SideBar.MyBillActivity;
import com.smartfinder.vzaapp.SideBar.MyDocActivity;
import com.smartfinder.vzaapp.SideBar.MyCartActivity;
import com.smartfinder.vzaapp.SideBar.MyFavActivity;
import com.smartfinder.vzaapp.SideBar.MyLikeActivity;
import com.smartfinder.vzaapp.SideBar.MyNotiActivity;
import com.smartfinder.vzaapp.SideBar.TableListActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import control.Dialog.OpenTableDialog;
import control.MenuView.MenuView;
import control.MenuView.MenuViewAdapter;
import emuntype.SiteOperate;
import entity.BaseEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Main.UserInfo.UserInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;
import entity.Service.OpenTable.OpenTableEntity;
import entity.Service.TableList.TableListEntity;
import entity.Service.VZACode.VZACodeEntity;
import entity.Service.VZAOpen.VZAOpenEntity;
import util.BroadcastUtil;
import util.ServiceUtil;

public class StaffActivity extends AdsMenuActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);

        //Custom Anim tableChange
        if (!getIntent().getBooleanExtra("tableChange", false)) {
            overridePendingTransition(R.anim.slidein, 0);
        } else {
            overridePendingTransition(0, 0);
        }

        //Get Entity
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getBaseContext(), MasterEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getBaseContext(), CheckLocationEntity.class);

        //Init Top Bar
        ((TextView) findViewById(R.id.txtHeader)).setText(masterData.FindByLocationID(checkLocation.LOCATION_ID).DisplayText +
                " - Table No - " + checkLocation.TABLE_NO);

        //Init Fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        MenuView menuView = (MenuView) findViewById(R.id.frmFragment);
        menuViewAdapter = new MenuViewAdapter(fragmentManager);
        menuViewAdapter.addItem(menuView, MainFragment.newInstance());
        menuView.setAdapter(menuViewAdapter);

        //Init Event
        findViewById(R.id.btnLocation).setOnClickListener(btnLocation_OnClick);
        findViewById(R.id.btnBill).setOnClickListener(btnBill_OnClick);
        findViewById(R.id.btnNewTable).setOnClickListener(btnNewTable_OnClick);
        findViewById(R.id.btnTableList).setOnClickListener(btnTableList_OnClick);

        menuView.setOnPageChangeListener(menuView_OnPageChange);

        //Init Notification
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(this, HotelInfoEntity.class);
        Integer locationID = BaseApplication.GlobalVariable.get(this, CheckLocationEntity.class).LOCATION_ID;
        String SystemID = BaseApplication.GlobalVariable.get(this, MasterEntity.class).FindByLocationID(locationID).SYSTEMID.toString();
        String VZACode = BaseApplication.GlobalVariable.get(this, VZACodeEntity.class).VZACODE;
        String hotelID = hotelInfo.DATA.HOTEL_ID.toString();
        String hotelKeyCode = hotelInfo.DATA.HOTEL_KEYCODE;
        String site = hotelInfo.DATA.HOTEL_URL;


        //-- Set Notification Key
        String key = SystemID + "_" + hotelKeyCode + "_" + locationID + "_" + VZACode;
        BaseApplication.NotificationRecordVariable.setVariable(getBaseContext(), key);

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("KEYCODEPara", hotelKeyCode));
        param.add(new BasicNameValuePair("HOTEL_IDPara", hotelID));
        param.add(new BasicNameValuePair("SYSTEMIDPara", SystemID));
        param.add(new BasicNameValuePair("SENDORRECEIVECODEPara", VZACode));
        param.add(new BasicNameValuePair("LOCATIONIDPara", locationID.toString()));

        broadcastUtil = new BroadcastUtil(this, param, site);
        broadcastUtil.setCastListener(broadcastUtil_OnCast);
        broadcastUtil.Start();

        //-- Call
        VZAMasterStatus();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Get Entity
        MasterEntity masterData = BaseApplication.GlobalVariable.get(getBaseContext(), MasterEntity.class);
        CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getBaseContext(), CheckLocationEntity.class);

        //Init Top Bar
        ((TextView) findViewById(R.id.txtHeader)).setText(masterData.FindByLocationID(checkLocation.LOCATION_ID).DisplayText +
                " - Table No - " + checkLocation.TABLE_NO);

        //-- Open Dialog VZA Code
        if (!getIntent().getStringExtra("VZACODE").equals("")) {
            ChooseDialog vzaDialog = ChooseDialog.newInstance(StaffActivity.this,
                    getResources().getString(R.string.alert_vzacode).replace("{VZACODE}", getIntent().getStringExtra("VZACODE")), false);
            vzaDialog.show(StaffActivity.this.getFragmentManager(), "tag");

            getIntent().putExtra("VZACODE", "");
        }

        //Init Cart Notification
        BaseApplication.CartStateVariable.setOnChangeListener(OnCart_OnChange);
        OnCart_OnChange.OnChange(BaseApplication.CartStateVariable.length());

        //Interrupt Notification
        broadcastUtil.Execute();
    }

    @Override
    protected void onDestroy() {
        //Un-Init Notification
        broadcastUtil.Shutdown();
        super.onDestroy();
    }

    public BroadcastUtil.OnCast broadcastUtil_OnCast = new BroadcastUtil.OnCast() {
        @Override
        public void OnCast(Integer totalRecord) {
            String displayRecord = "";
            if (totalRecord > 0) {
                if (totalRecord >= 100) {
                    displayRecord = "99+";
                } else {
                    displayRecord = totalRecord.toString();
                }
                findViewById(R.id.btnNoti).setBackground(getResources().getDrawable(R.drawable.pic_bar_noti_red));
            } else {
                findViewById(R.id.btnNoti).setBackground(getResources().getDrawable(R.drawable.pic_bar_noti));
            }
            if (BroadcastUtil.NotificationData.size() > 0) {
                findViewById(R.id.btnNoti).setOnClickListener(btnNoti_OnClick);
            } else {
                findViewById(R.id.btnNoti).setOnClickListener(null);
            }
            ((TextView) findViewById(R.id.txtNoti)).setText(displayRecord);
        }
    };

    public MenuView.OnPageChangeListener menuView_OnPageChange = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int form, float value, int to) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        @Override
        public void onPageSelected(final int position) {
            //Set Swipe Page
            StaffActivity.this.setSwipeBackEnable(position == 0 ? true : false);
        }
    };

    public BaseApplication.CartStateVariable.OnChange OnCart_OnChange = new BaseApplication.CartStateVariable.OnChange() {
        @Override
        public void OnChange(Integer length) {
            if (length > 0) {
                (findViewById(R.id.btnCart)).setBackgroundResource(R.drawable.bar_cart_used);
                (findViewById(R.id.btnCart)).setOnClickListener(btnCart_OnClick);
                ((TextView) findViewById(R.id.txtCart)).setText(length.toString());
            } else {
                (findViewById(R.id.btnCart)).setBackgroundResource(R.drawable.bar_cart);
                (findViewById(R.id.btnCart)).setOnClickListener(null);
                ((TextView) findViewById(R.id.txtCart)).setText("");
            }
        }
    };

    public Button.OnClickListener btnCart_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(StaffActivity.this, MyCartActivity.class);
            startActivity(intent);
        }
    };

    public Button.OnClickListener btnBill_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //-- Call VZAMasterStatus
            VZAMasterStatus();

            if (BaseApplication.MyDocRecordVariable.getStatus(StaffActivity.this).equals("Y")) {
                Intent intent = new Intent(StaffActivity.this, MyBillActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                ChooseDialog dialog = ChooseDialog.newInstance(StaffActivity.this, getResources().getString(R.string.alert_mydocument_empty), false);
                dialog.show(getFragmentManager(), "tag");
            }
        }
    };

    public Button.OnClickListener btnNewTable_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (BaseApplication.CartStateVariable.length() > 0) {
                ChooseDialog dialog = ChooseDialog.newInstance(StaffActivity.this, getString(R.string.alert_new_table), false);
                dialog.show(StaffActivity.this.getFragmentManager(), "tag");
                return;
            }

            //-- Get Global Variable
            final CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(StaffActivity.this, CheckLocationEntity.class);
            final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(StaffActivity.this, HotelInfoEntity.class);
            String site = hotelInfo.DATA.HOTEL_URL;
            String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

            List<NameValuePair> param = new ArrayList<>();
            param.add(new BasicNameValuePair("SYSTEMIDPara", BaseApplication.GlobalVariable.get(StaffActivity.this, MasterEntity.class).FindByLocationID(checkLocation.LOCATION_ID).SYSTEMID.toString()));
            param.add(new BasicNameValuePair("LOCATION_IDPara", checkLocation.LOCATION_ID.toString()));

            ServiceUtil serviceUtil = new ServiceUtil(StaffActivity.this, site, keycode, SiteOperate.POSgetTableListForStaffJSON, param, TableListEntity.class);
            serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                @Override
                public void OnFinished(Object results) {
                    OpenTableDialog dialog = OpenTableDialog.newInstance(StaffActivity.this, (TableListEntity) results);
                    dialog.setOnSubmitListener(new OpenTableDialog.OnSubmit() {
                        @Override
                        /*public void OnSubmit(final String table,final Integer pax) {
                            ChangeTable(table, pax);
                        }*/
                        public void OnSubmit(final String table, final Integer pax, final String telephone) {
                            ChangeTable(table, pax, telephone);
                        }
                    });
                    dialog.show(StaffActivity.this.getFragmentManager(), "tag");
                }
            });
            serviceUtil.execute();
        }
    };

    public void ChangeTable(final String table, final Integer pax, final String telephone) {
        //Check Location
        final CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(StaffActivity.this, CheckLocationEntity.class);
        final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(StaffActivity.this, HotelInfoEntity.class);
        String site = hotelInfo.DATA.HOTEL_URL;
        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

        final ServiceUtil checkLocationService = new ServiceUtil(StaffActivity.this, site, keycode, SiteOperate.VZACheckLocationOpenedJSON, CheckLocationEntity.class);
        final ServiceUtil openTableService = new ServiceUtil(StaffActivity.this, site, keycode, SiteOperate.VZACheckTableOpenedJSON, OpenTableEntity.class);
        final ServiceUtil vzaOpenService = new ServiceUtil(StaffActivity.this, site, keycode, SiteOperate.VZAOpenJSON, VZAOpenEntity.class);

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("LOCATION_IDPara", locationEntity.LOCATION_ID.toString()));
        param.add(new BasicNameValuePair("VZA_CODEPara", "T" + table));
        checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                //Keep checkLocation To AppVariable
                CheckLocationEntity entity = (CheckLocationEntity) results;
                entity.VZACODE = "T" + entity.TABLE_NO;
                BaseApplication.GlobalVariable.set(entity);

                //Keep vzaCode To AppVariable
                VZACodeEntity vzaEntity = new VZACodeEntity();
                vzaEntity.VZACODE = "T" + entity.TABLE_NO;
                BaseApplication.GlobalVariable.set(vzaEntity);


                //Call Table Open
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("LOCATION_IDPara", locationEntity.LOCATION_ID.toString()));
                param.add(new BasicNameValuePair("TABLE_NOPara", entity.TABLE_NO));
                param.add(new BasicNameValuePair("ACCEPT_COVERPara", pax.toString()));
                param.add(new BasicNameValuePair("ACTION_PROCRESSPara", "N"));
                openTableService.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        OpenTableEntity entity = (OpenTableEntity) results;
                        if (entity.RESULT) {
                            //-- If telephone is null
                            if (telephone.equals("")) {
                                //-- Set Global Variable Entity
                                Intent intent = new Intent(StaffActivity.this, StaffActivity.class);
                                //-- Put Extra
                                intent.putExtra("tableChange", true);
                                intent.putExtra("VZACODE", "");
                                startActivity(intent);
                                finish();
                            } else {
                                //-- VZAOpenJSON
                                List<NameValuePair> param = new ArrayList<>();
                                param.add(new BasicNameValuePair("LOCATION_IDPara", locationEntity.LOCATION_ID.toString()));
                                param.add(new BasicNameValuePair("TABLE_NOPara", table));
                                param.add(new BasicNameValuePair("TELEPHONE_NOPara", telephone));
                                vzaOpenService.setFinishedListener(new ServiceUtil.OnFinished() {
                                    @Override
                                    public void OnFinished(Object results) {
                                        VZAOpenEntity vzaOpenEntity = (VZAOpenEntity) results;
                                        if (vzaOpenEntity.ERRORMSG.equals("")) {
                                            Intent intent = new Intent(StaffActivity.this, StaffActivity.class);
                                            intent.putExtra("tableChange", true);
                                            intent.putExtra("VZACODE", vzaOpenEntity.VZACODE);
                                            BaseApplication.RequestCheckVariable.set(false);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            //-- ERROR
                                            ChooseDialog errorDialog = ChooseDialog.newInstance(StaffActivity.this, vzaOpenEntity.ERRORMSG, false);
                                            errorDialog.show(StaffActivity.this.getFragmentManager(), "tag");
                                        }
                                    }
                                });
                                vzaOpenService.execute(param);
                            }
                        } else {
                            ChooseDialog chooseDialog = ChooseDialog.newInstance(StaffActivity.this, getResources().getString(R.string.alert_table_not_exist), false);
                            chooseDialog.show(StaffActivity.this.getFragmentManager(), "tag");
                        }
                    }
                });
                openTableService.execute(param);
            }
        });
        checkLocationService.execute(param);
    }

    ;

    public void ChangeTable(final String table, final Integer pax) {
        //Check Location
        final CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(StaffActivity.this, CheckLocationEntity.class);
        final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(StaffActivity.this, HotelInfoEntity.class);
        String site = hotelInfo.DATA.HOTEL_URL;
        String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

        final ServiceUtil checkLocationService = new ServiceUtil(StaffActivity.this, site, keycode, SiteOperate.VZACheckLocationOpenedJSON, CheckLocationEntity.class);
        final ServiceUtil openTableService = new ServiceUtil(StaffActivity.this, site, keycode, SiteOperate.VZACheckTableOpenedJSON, OpenTableEntity.class);

        List<NameValuePair> param = new ArrayList<>();
        param.add(new BasicNameValuePair("LOCATION_IDPara", locationEntity.LOCATION_ID.toString()));
        param.add(new BasicNameValuePair("VZA_CODEPara", "T" + table));
        checkLocationService.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {
                //Keep checkLocation To AppVariable
                CheckLocationEntity entity = (CheckLocationEntity) results;
                entity.VZACODE = "T" + entity.TABLE_NO;
                BaseApplication.GlobalVariable.set(entity);

                //Keep vzaCode To AppVariable
                VZACodeEntity vzaEntity = new VZACodeEntity();
                vzaEntity.VZACODE = "T" + entity.TABLE_NO;
                BaseApplication.GlobalVariable.set(vzaEntity);

                //Call Table Open
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("LOCATION_IDPara", locationEntity.LOCATION_ID.toString()));
                param.add(new BasicNameValuePair("TABLE_NOPara", entity.TABLE_NO));
                param.add(new BasicNameValuePair("ACCEPT_COVERPara", pax.toString()));
                param.add(new BasicNameValuePair("ACTION_PROCRESSPara", "N"));
                openTableService.setFinishedListener(new ServiceUtil.OnFinished() {
                    @Override
                    public void OnFinished(Object results) {
                        OpenTableEntity entity = (OpenTableEntity) results;
                        if (entity.RESULT) {
                            //-- Set Global Variable Entity
                            //BaseApplication.GlobalVariable.set(entity);
                            //onResume();
                            Intent intent = new Intent(StaffActivity.this, StaffActivity.class);
                            intent.putExtra("tableChange", true);
                            startActivity(intent);
                            finish();
                        } else {
                            ChooseDialog chooseDialog = ChooseDialog.newInstance(StaffActivity.this, getResources().getString(R.string.alert_table_not_exist), false);
                            chooseDialog.show(StaffActivity.this.getFragmentManager(), "tag");
                        }
                    }
                });
                openTableService.execute(param);
            }
        });
        checkLocationService.execute(param);
    }

    ;

    public void VZAMasterStatus() {
        //Init
        HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(this, HotelInfoEntity.class);
        Integer locationID = BaseApplication.GlobalVariable.get(this, CheckLocationEntity.class).LOCATION_ID;
        String VZACode = BaseApplication.GlobalVariable.get(this, VZACodeEntity.class).VZACODE;
        String hotelKeyCode = hotelInfo.DATA.HOTEL_KEYCODE;
        String site = hotelInfo.DATA.HOTEL_URL;
        String Email = BaseApplication.GlobalVariable.get(StaffActivity.this, UserInfoEntity.class).Data.EMAIL.toString();

        List<NameValuePair> paramStatus = new ArrayList<>();
        paramStatus.add(new BasicNameValuePair("LOCATIONIDPara", locationID.toString()));
        paramStatus.add(new BasicNameValuePair("VZACODEPara", VZACode));
        paramStatus.add(new BasicNameValuePair("USER_EMAIL", Email));

        ServiceUtil serviceUtil = new ServiceUtil(StaffActivity.this, site, hotelKeyCode,
                SiteOperate.VZAgetStatusMasterDataJSON, paramStatus,
                BaseEntity.class);

        serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
            @Override
            public void OnFinished(Object results) {

                //Init Adapter
                BaseEntity entity = (BaseEntity) results;

                //-- If Data is entry
                if (entity.STATUS_MYBILL) {
                    //-- SET STATUS
                    BaseApplication.MyDocRecordVariable.setVariable(getBaseContext(), "Y");
                    //-- SET Background
                    findViewById(R.id.btnBill).setBackground(getResources().getDrawable(R.drawable.pic_bar_bill_red));
                } else {
                    //-- SET STATUS
                    BaseApplication.MyDocRecordVariable.setVariable(getBaseContext(), "N");
                }
            }
        });
        serviceUtil.execute();
    }

    public Button.OnClickListener btnTableList_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(StaffActivity.this, TableListActivity.class);
            startActivityForResult(intent, getResources().getInteger(R.integer.request_tablelist));
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == getResources().getInteger(R.integer.request_tablelist) && resultCode == Activity.RESULT_OK) {
            //ChangeTable(data.getStringExtra("table"),data.getIntExtra("pax",0));
            ChangeTable(data.getStringExtra("table"), data.getIntExtra("pax", 0), "");
        }
    }

    public Button.OnClickListener btnNoti_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Init Notification
            broadcastUtil.ReadRecord();
            Intent intent = new Intent(StaffActivity.this, MyNotiActivity.class);
            startActivity(intent);
        }
    };

    public Button.OnClickListener btnLocation_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (BaseApplication.CartStateVariable.length() > 0) {
                ChooseDialog dialog = ChooseDialog.newInstance(StaffActivity.this, getString(R.string.alert_check_mycart), false);
                dialog.show(StaffActivity.this.getFragmentManager(), "tag");
                return;
            }

            //-- Dialog Confirm Change Location
            ChooseDialog dialog = ChooseDialog.newInstance(StaffActivity.this, getResources().getString(R.string.alert_change_location), true);
            dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                @Override
                public void OnSubmit(View view) {
                    Intent intent = new Intent(StaffActivity.this, CheckVzaActivity.class);
                    intent.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            });
            dialog.show(getFragmentManager(), "tag");

            //Intent intent = new Intent(StaffActivity.this,CheckVzaActivity.class);
            //startActivity(intent);
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        MenuView menuView = (MenuView) findViewById(R.id.frmFragment);
        if (menuViewAdapter.getCount() > 0) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            menuViewAdapter = new MenuViewAdapter(fragmentManager);
            menuViewAdapter.addItem(menuView, MainFragment.newInstance());
            menuView.setAdapter(menuViewAdapter);
        }
    }

    @Override
    public void onBackPressed() {
        //Custom Anim
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slideout);
    }

}
