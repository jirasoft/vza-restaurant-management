package com.smartfinder.vzaapp.Tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartfinder.vzaapp.R;


public class TutorialFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_tutorial, container, false);
        Integer position = getArguments().getInt("position");
        if(position == 0) {
            rootView.findViewById(R.id.imgTutorial).setBackgroundResource(R.drawable.pic_tutorial_1);
            rootView.findViewById(R.id.btnStart).setVisibility(View.GONE);
            rootView.findViewById(R.id.btnStart).setOnClickListener(null);
        }else if(position == 1){
            rootView.findViewById(R.id.imgTutorial).setBackgroundResource(R.drawable.pic_tutorial_2);
            rootView.findViewById(R.id.btnStart).setVisibility(View.GONE);
            rootView.findViewById(R.id.btnStart).setOnClickListener(null);
        }else if(position == 2){
            rootView.findViewById(R.id.imgTutorial).setBackgroundResource(R.drawable.pic_tutorial_3);
            rootView.findViewById(R.id.btnStart).setVisibility(View.GONE);
            rootView.findViewById(R.id.btnStart).setOnClickListener(null);
        }else if(position == 3){
            rootView.findViewById(R.id.imgTutorial).setBackgroundResource(R.drawable.pic_tutorial_4);
            rootView.findViewById(R.id.btnStart).setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.btnStart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        }
        return rootView;
    }
}
