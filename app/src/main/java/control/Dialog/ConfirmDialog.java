package control.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import util.ViewUtil;

public class ConfirmDialog extends DialogFragment {

    private OnSubmit submitListener;
    private OnDismiss dismissListener;
    private OnClose closeListener;
    private OnCancel cancelListener;

    public interface OnSubmit{
        public void OnSubmit(View view);
    }
    public interface OnDismiss{
        public void OnDismiss();
    }
    public interface OnClose{
        public void OnClose();
    }
    public interface OnCancel{
        public void OnCancel();
    }

    public void setOnSubmitListener(OnSubmit event){
        this.submitListener = event;
    }
    public void setOnDismissListener(OnDismiss event){
        this.dismissListener = event;
    }
    public void setOnCloseListener(OnClose event){
        this.closeListener = event;
    }
    public void setOnCancelListener(OnCancel event){
        this.cancelListener = event;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        cancelListener.OnCancel();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(dismissListener != null){
            dismissListener.OnDismiss();
        }
        if(closeListener != null) {
            closeListener.OnClose();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_msg, null);

        //Init Message
        ((TextView)view.findViewById(R.id.txtMessage)).setText(getArguments().getCharSequence("title"));

        //Init Event
        view.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(submitListener != null) {
                    submitListener.OnSubmit(v);
                    dismiss();
                }
            }
        });

        view.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelListener.OnCancel();
                dismiss();
            }
        });

        return  builder.setView(view).create();
    }

    public static final ConfirmDialog newInstance(final Activity rootView,CharSequence title){

        //get Default Rotate & lock Rotate
        final int getOrientation = rootView.getRequestedOrientation();
        rootView.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        //Disable View
        ViewUtil.isEnableView(rootView.getWindow().getDecorView().findViewById(android.R.id.content),false);

        ConfirmDialog dialog = new ConfirmDialog();
        dialog.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putCharSequence("title", title);
        dialog.setArguments(bundle);

        return dialog;
    }

}
