package control.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.util.List;

import entity.Service.TableList.TableListData;
import entity.Service.TableList.TableListEntity;
import util.ViewUtil;

public class CoverDialog extends DialogFragment {

    private OnSubmit submitListener;
    private OnDismiss dismissListener;
    private OnClose closeListener;

    public interface OnSubmit {
        public void OnSubmit(Integer pax);
    }

    public interface OnDismiss {
        public void OnDismiss();
    }

    public interface OnClose {
        public void OnClose();
    }

    public void setOnSubmitListener(OnSubmit event) {
        this.submitListener = event;
    }
    public void setOnDismissListener(OnDismiss event) {
        this.dismissListener = event;
    }
    public void setOnCloseListener(OnClose event) {
        this.closeListener = event;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (dismissListener != null) {
            dismissListener.OnDismiss();
        }
        if (closeListener != null) {
            closeListener.OnClose();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_cover, null);

        //Init Edit
        final EditText txtPax = (EditText) view.findViewById(R.id.txtPax);

        txtPax.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                view.findViewById(R.id.btnOK).performClick();
                return false;
            }
        });

        view.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (submitListener != null) {
                    if (txtPax.getText().length() > 0) {
                        submitListener.OnSubmit(Integer.parseInt(txtPax.getText().toString()));
                        dismiss();
                    }
                }
            }
        });

        view.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.setView(view).create();
    }

    public static final CoverDialog newInstance(final Activity rootView) {

        //Disable View
        ViewUtil.isEnableView(rootView.getWindow().getDecorView().findViewById(android.R.id.content), false);

        CoverDialog dialog = new CoverDialog();
        dialog.setCancelable(false);
        Bundle bundle = new Bundle();
        //bundle.putSerializable("entity",tableList);
        dialog.setArguments(bundle);

        dialog.setOnCloseListener(new OnClose() {
            @Override
            public void OnClose() {
                ViewUtil.isEnableView(rootView.getWindow().getDecorView().findViewById(android.R.id.content), true);
            }
        });

        return dialog;
    }
}
