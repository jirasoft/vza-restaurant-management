package control.Dialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import com.smartfinder.vzaapp.R;


public class DeferDialog extends ProgressDialog {
    public DeferDialog(Context context) {
        super(context);
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_defer);
    }

}
