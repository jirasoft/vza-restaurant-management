package control.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import util.ViewUtil;

public class EditDialog extends DialogFragment {

    private OnSubmit submitListener;
    private OnDismiss dismissListener;
    private OnClose closeListener;

    public interface OnSubmit{
        public void OnSubmit(String result);
    }
    public interface OnDismiss{
        public void OnDismiss();
    }
    private interface OnClose{
        public void OnClose();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(dismissListener != null) {
            dismissListener.OnDismiss();
        }
        if(closeListener != null) {
            closeListener.OnClose();
        }
    }

    public void setOnSubmitListener(OnSubmit event){
        this.submitListener = event;
    }
    public void setOnDismissListener(OnDismiss event){
        this.dismissListener = event;
    }
    private void setOnCloseListener(OnClose event){
        this.closeListener = event;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_edit,null);

        //Init Title
        final EditText txtMessage = ((EditText)view.findViewById(R.id.txtMessage));
        txtMessage.setText(getArguments().getString("message"));

        //Init Edit
        view.findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(submitListener != null){
                    submitListener.OnSubmit(txtMessage.getText().toString());
                }
                dismiss();
            }
        });
        view.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return builder.setView(view).create();
    }
    public static final EditDialog newInstance(final View view, String message){
        //Disable View
        ViewUtil.isEnableView(view, false);

        EditDialog dialog = new EditDialog();
        Bundle bundle = new Bundle();
        bundle.putString("message", message);
        dialog.setArguments(bundle);
        dialog.setCancelable(false);

        dialog.setOnCloseListener(new OnClose() {
            @Override
            public void OnClose() {
                ViewUtil.isEnableView(view, true);
            }
        });

        return dialog;
    }
}
