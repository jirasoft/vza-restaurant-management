package control.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
//import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.util.List;

import entity.Service.Master.MasterEntity;
import entity.Service.TableList.TableListData;
import entity.Service.TableList.TableListEntity;
import util.ViewUtil;

public class OpenTableDialog extends DialogFragment {

    private OnSubmit submitListener;
    private OnDismiss dismissListener;
    private OnClose closeListener;

    public interface OnSubmit{
        public void OnSubmit(String table,Integer pax, String telephone);
    }
    public interface OnDismiss{
        public void OnDismiss();
    }
    private interface OnClose{
        public void OnClose();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(closeListener != null){
            closeListener.OnClose();
        }
        if(dismissListener != null){
            dismissListener.OnDismiss();
        }
    }
    public void setOnSubmitListener(OnSubmit event){
        this.submitListener = event;
    }
    public void setOnDismissListener(OnDismiss event){
            this.dismissListener = event;
    }
    private void setOnCloseListener(OnClose event){
        this.closeListener = event;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_opentable,null);

        //Init Edit
        final EditText txtTable = (EditText)view.findViewById(R.id.txtTable);
        final EditText txtPax = (EditText)view.findViewById(R.id.txtPax);
        final EditText txtTelephone = (EditText)view.findViewById(R.id.txtTelephoneNo);

        txtPax.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(getArguments() != null) {
                        if (getArguments().getSerializable("entity") != null) {
                            List<TableListData> lisData = ((TableListEntity)getArguments().getSerializable("entity")).TABLEDATALIST;
                            for(TableListData data : lisData){
                                if(txtTable.getText().toString().equals(data.TABLE_NO)){
                                    txtPax.setText(data.COVER.toString());
                                    txtPax.setSelection(txtPax.getText().length());
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        });

        txtPax.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                view.findViewById(R.id.btnOK).performClick();
                return false;
            }
        });

        view.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (submitListener != null) {
                    if (txtTable.getText().length() > 0 && txtPax.getText().length() > 0) {
                        submitListener.OnSubmit(txtTable.getText().toString(), Integer.parseInt(txtPax.getText().toString()),
                                txtTelephone.getText().toString());
                        dismiss();
                    }
                }
            }
        });

        view.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder.setView(view).create();
    }
    public static final OpenTableDialog newInstance(final Activity rootView,final TableListEntity tableList){

        //Disable View
        ViewUtil.isEnableView(rootView.getWindow().getDecorView().findViewById(android.R.id.content),false);

        OpenTableDialog dialog = new OpenTableDialog();
        dialog.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putSerializable("entity",tableList);
        dialog.setArguments(bundle);
        dialog.setOnCloseListener(new OnClose() {
            @Override
            public void OnClose() {
                ViewUtil.isEnableView(rootView.getWindow().getDecorView().findViewById(android.R.id.content),true);
            }
        });

        return dialog;
    }
}
