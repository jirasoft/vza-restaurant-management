package control.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import util.ViewUtil;


public class ProfileDialog extends DialogFragment {

    private OnSubmit submitListener;
    private OnDismiss dismissListener;
    private OnClose closeListener;

    public interface OnSubmit{
        public void OnSubmit(View view);
    }
    public interface OnDismiss{
        public void OnDismiss();
    }
    private interface OnClose{
        public void OnClose();
    }

    public void setOnSubmitListener(OnSubmit event){
        this.submitListener = event;
    }
    public void setOnDismissListener(OnDismiss event){
        this.dismissListener = event;
    }
    private void setOnCloseListener(OnClose event){
        this.closeListener = event;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(closeListener != null) {
            closeListener.OnClose();
        }
        if(dismissListener != null){
            dismissListener.OnDismiss();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_msg, null);

        //Init Title
        ((TextView)view.findViewById(R.id.txtMessage)).setText(getArguments().getCharSequence("title"));

        //Init Event
        view.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(submitListener != null) {
                    submitListener.OnSubmit(v);
                }
                dismiss();
            }
        });

        view.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //Init ChooseMode
        if(!getArguments().getBoolean("choiceMode")){
            view.findViewById(R.id.btnCancel).setVisibility(View.GONE);
        }

        return  builder.setView(view).create();
    }
    public static final ProfileDialog newInstance(final Activity rootView, CharSequence title, boolean choiceMode){

        //get Default Rotate & lock Rotate
        final int getOrientation = rootView.getRequestedOrientation();
        rootView.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        //Disable View
        ViewUtil.isEnableView(rootView.getWindow().getDecorView().findViewById(android.R.id.content),false);

        ProfileDialog dialog = new ProfileDialog();
        Bundle bundle = new Bundle();
        bundle.putCharSequence("title", title);
        bundle.putBoolean("choiceMode", choiceMode);
        dialog.setArguments(bundle);

        return dialog;
    }

}
