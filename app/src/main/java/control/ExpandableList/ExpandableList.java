package control.ExpandableList;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;

import com.smartfinder.vzaapp.R;

public class ExpandableList extends ExpandableListView {

    private float rowLength;
    private Context context;
    public ExpandableList(Context context) {
        super(context);
        this.context = context;
        OnInit();
    }
    public ExpandableList(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpandableList);
        rowLength = typedArray.getFloat(R.styleable.ExpandableList_rowLength,-1);
        typedArray.recycle();
        OnInit();
    }
    public ExpandableList(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpandableList);
        rowLength = typedArray.getFloat(R.styleable.ExpandableList_rowLength,-1);
        typedArray.recycle();
        OnInit();
    }
    private void OnInit(){
        //Set Nullable Indicator
        this.setGroupIndicator(null);
        //Set OnGroup Disable
        this.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
    }
    @Override
    public void setAdapter(ExpandableListAdapter adapter) {
        super.setAdapter(adapter);
        //Set Expend All
        for(int i = 0 ; i < adapter.getGroupCount(); i++){
            this.expandGroup(i);
        }
    }
    public float getRowLength(){
        return rowLength;
    }
}
