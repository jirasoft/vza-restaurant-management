package control.ExpandableList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import java.text.DecimalFormat;

import entity.Host.Cart.MenuOrderData;
import entity.Service.BillOrder.BillOrderData;
import entity.Service.BillOrder.BillOrderDataAddition;
import entity.Service.BillOrder.BillOrderEntity;


public class MyBillAdapter extends BaseExpandableListAdapter {

    Context context;
    String currencyDesc;
    BillOrderEntity entity;
    public MyBillAdapter(Context context,BillOrderEntity entity,String currencyDesc){
        this.context = context;
        this.currencyDesc = currencyDesc;
        this.entity = entity;
        BaseApplication.CartStateVariable.setOnChangeListener(new BaseApplication.CartStateVariable.OnChange() {
            @Override
            public void OnChange(Integer length) {
                notifyDataSetChanged();
            }
        });
    };

    @Override
    public int getGroupCount() {
        return entity.ITEMS.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int ComCount = 0;
        if(!entity.ITEMS.get(groupPosition).CONDIMENT.equals("")) {
            ComCount = entity.ITEMS.get(groupPosition).CONDIMENT.split(",").length;
        }
        return entity.ITEMS.get(groupPosition).ADDITIONALDETAIL.size() + ComCount;
    }

    public BillOrderEntity getEntity(){
        return entity;
    }

    @Override
    public BillOrderData getGroup(int groupPosition) {
        return entity.ITEMS.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
            int ComCount = 0;
            if(!entity.ITEMS.get(groupPosition).CONDIMENT.equals("")) {
                ComCount = entity.ITEMS.get(groupPosition).CONDIMENT.split(",").length;
            }
            if(childPosition <  ComCount) {
                return entity.ITEMS.get(groupPosition).CONDIMENT.split(",")[childPosition];
            }else{
                return entity.ITEMS.get(groupPosition).ADDITIONALDETAIL.get(childPosition - ComCount);
            }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_mybill_item,parent,false);
        }

        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        TextView txtQty = (TextView) convertView.findViewById(R.id.txtQty);
        final FrameLayout frmList = (FrameLayout) convertView.findViewById(R.id.frmList);
        final TextView txtTotalPrice = (TextView) convertView.findViewById(R.id.txtTotalPrice);
        final ImageView imgStat = (ImageView) convertView.findViewById(R.id.imgStat);
        //Set Height
        ExpandableList expandableList = (ExpandableList)parent;
        if(expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) (parent.getHeight() / expandableList.getRowLength());
        }

        if(getGroup(groupPosition).ORDER_STATUS == 1){
            imgStat.setImageResource(R.drawable.pic_stat_printed);
        }else if(getGroup(groupPosition).ORDER_STATUS == 2) {
            imgStat.setImageResource(R.drawable.pic_stat_served);
        }else if(getGroup(groupPosition).ORDER_STATUS == 100) {
            imgStat.setImageResource(R.drawable.pic_stat_check);
        } else if(getGroup(groupPosition).ORDER_STATUS == 200) {
        imgStat.setImageResource(R.drawable.pic_stat_checkprinted);
        }else{
            imgStat.setImageResource(R.color.transparent);
        }


            //Init Data
        txtName.setText(getGroup(groupPosition).MENU_NAME);
        txtQty.setText(getGroup(groupPosition).ORDER_QTY.toString());

        //Addition
        double additionPrice = 0.00;
        for(BillOrderDataAddition orderData : getGroup(groupPosition).ADDITIONALDETAIL){
           additionPrice = additionPrice + orderData.ADDITIONAL_UNITPRICE;
        }

        txtTotalPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format((getGroup(groupPosition).MENU_UNITPRICE + additionPrice) * getGroup(groupPosition).ORDER_QTY));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_mybill_subitem,parent,false);
        }

        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
        FrameLayout frmList = (FrameLayout) convertView.findViewById(R.id.frmList);

        //Set Height
        ExpandableList expandableList = (ExpandableList)parent;
        if(expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) ((parent.getHeight() / expandableList.getRowLength()) / 1.5);
        }

        if(getChild(groupPosition, childPosition) instanceof BillOrderDataAddition){
            txtName.setText((((BillOrderDataAddition) getChild(groupPosition,childPosition)).ADDITIONAL_NAME1));
            if(((BillOrderDataAddition)getChild(groupPosition, childPosition)).ADDITIONAL_UNITPRICE > 0.00){
                txtPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format(((BillOrderDataAddition)getChild(groupPosition, childPosition)).ADDITIONAL_UNITPRICE));
            }else{
                txtPrice.setText(currencyDesc + "0.00");
            }
        }else{
            txtName.setText(((String)getChild(groupPosition,childPosition)));
            txtPrice.setText("");
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
