package control.ExpandableList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

import entity.Host.Cart.MenuOrderData;
import entity.Host.Cart.MenuOrderDataAddition;
import entity.Host.Cart.MenuOrderEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Master.MasterEntity;
import util.ViewUtil;


public class MyCartAdapter extends BaseExpandableListAdapter {

    Context context;
    String currencyDesc;

    public MyCartAdapter(Context context, String currencyDesc) {
        this.context = context;
        this.currencyDesc = currencyDesc;
        BaseApplication.CartStateVariable.setOnChangeListener(new BaseApplication.CartStateVariable.OnChange() {
            @Override
            public void OnChange(Integer length) {
                notifyDataSetChanged();
            }
        });
    }

    ;

    @Override
    public int getGroupCount() {
        return BaseApplication.CartStateVariable.getAll().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (BaseApplication.CartStateVariable.getAll().get(groupPosition).isMenuSet) {
            return BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.size();
        } else {
            int ComCount = 0;
            if (!BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(0).CONDIMENT.equals("")) {
                ComCount = BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(0).CONDIMENT.split(",").length;
            }
            int AddCount = BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(0).ADDITIONLIST.size();
            return ComCount + AddCount;
        }
    }

    @Override
    public MenuOrderEntity getGroup(int groupPosition) {
        return BaseApplication.CartStateVariable.getAll().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if (BaseApplication.CartStateVariable.getAll().get(groupPosition).isMenuSet) {
            return BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(childPosition);
        } else {
            int ComCount = 0;
            if (!BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(0).CONDIMENT.equals("")) {
                ComCount = BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(0).CONDIMENT.split(",").length;
            }
            if (childPosition < ComCount) {
                return BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(0).CONDIMENT.split(",")[childPosition];
            } else {
                return BaseApplication.CartStateVariable.getAll().get(groupPosition).ORDER.get(0).ADDITIONLIST.get(childPosition - ComCount);
            }
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_mycart_item, parent, false);
        }

        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
        TextView txtQty = (TextView) convertView.findViewById(R.id.txtQty);
        LinearLayout frmControl = (LinearLayout) convertView.findViewById(R.id.frmControl);
        final FrameLayout frmList = (FrameLayout) convertView.findViewById(R.id.frmList);
        final TextView txtTotalPrice = (TextView) convertView.findViewById(R.id.txtTotalPrice);
        final Button btnDelete = (Button) convertView.findViewById(R.id.btnDelete);
        btnDelete.setVisibility(View.INVISIBLE);

        //Clear Animation
        frmControl.clearAnimation();
        btnDelete.clearAnimation();

        //Set Height
        ExpandableList expandableList = (ExpandableList) parent;
        if (expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) (parent.getHeight() / expandableList.getRowLength());
        }

        //Init Data
        txtName.setText(getGroup(groupPosition).ORDER_NAME);

        Double unitPrice = getGroup(groupPosition).getUnitPrice();
        Double addPrice = getGroup(groupPosition).getAdditionPrice();
        if (getGroup(groupPosition).isMenuSet) {
            txtPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format(unitPrice));
            txtTotalPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format((unitPrice + getGroup(groupPosition).ORDER_PRICE) * getGroup(groupPosition).ORDER_QRY));
        } else {
            txtPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format(unitPrice));
            txtTotalPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format((unitPrice + addPrice) * getGroup(groupPosition).ORDER_QRY));
        }
        txtQty.setText(getGroup(groupPosition).ORDER_QRY.toString());

        //Init Event
        frmControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnDelete.getVisibility() == View.VISIBLE) {
                    //Init From Animation
                    TranslateAnimation viewAnimate = new TranslateAnimation(-btnDelete.getWidth(), 0, 0, 0);
                    viewAnimate.setDuration(500);
                    view.startAnimation(viewAnimate);

                    //Init Button Animation
                    TranslateAnimation btnAnimate = new TranslateAnimation(0, btnDelete.getWidth(), 0, 0);
                    btnAnimate.setDuration(500);
                    btnDelete.startAnimation(btnAnimate);
                    btnDelete.setVisibility(View.INVISIBLE);
                } else {
                    //Init From Animation
                    TranslateAnimation viewAnimate = new TranslateAnimation(0, -btnDelete.getWidth(), 0, 0);
                    viewAnimate.setDuration(500);
                    viewAnimate.setFillAfter(true);
                    view.startAnimation(viewAnimate);

                    //Init Button Animation
                    TranslateAnimation btnAnimate = new TranslateAnimation(btnDelete.getWidth(), 0, 0, 0);
                    btnAnimate.setDuration(500);
                    btnDelete.startAnimation(btnAnimate);
                    btnDelete.setVisibility(View.VISIBLE);
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Init From Animation
                ViewUtil.isEnableView(parent, false);
                TranslateAnimation frmAnimate = new TranslateAnimation(0, -frmList.getWidth(), 0, 0);
                frmAnimate.setDuration(500);
                frmList.startAnimation(frmAnimate);


                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                BaseApplication.CartStateVariable.clear(groupPosition);
                                notifyDataSetChanged();

                                if (BaseApplication.CartStateVariable.length() > 0) {
                                    Integer locationID = BaseApplication.GlobalVariable.get(context, CheckLocationEntity.class).LOCATION_ID;
                                    String currencyDep = BaseApplication.GlobalVariable.get(context, MasterEntity.class).FindByLocationID(locationID).CURRENCY_DEP;
                                    ((TextView) ((Activity) context).findViewById(R.id.txtAllPrice)).setText(currencyDep + new DecimalFormat("#,##0.00").format(BaseApplication.CartStateVariable.getAllPrice()));
                                    ViewUtil.isEnableView(parent, true);
                                } else {
                                    ((Activity) context).finish();
                                }
                            }
                        });
                    }
                }, 500);
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_mycart_subitem, parent, false);
        }

        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
        FrameLayout frmList = (FrameLayout) convertView.findViewById(R.id.frmList);

        //Set Height
        ExpandableList expandableList = (ExpandableList) parent;
        if (expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) ((parent.getHeight() / expandableList.getRowLength()) / 1.5);
        }

        if (getGroup(groupPosition).isMenuSet) {
            txtName.setText(((MenuOrderData) getChild(groupPosition, childPosition)).MENU_NAME);
            //if(((MenuOrderData)getChild(groupPosition, childPosition)).MENU_UNITPRICE > 0.00){
            txtPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format(((MenuOrderData) getChild(groupPosition, childPosition)).MENU_UNITPRICE));
            //}
        } else {
            if (getChild(groupPosition, childPosition) instanceof MenuOrderDataAddition) {
                txtName.setText(((MenuOrderDataAddition) getChild(groupPosition, childPosition)).ADDITIONAL_NAME);
                txtPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format(((MenuOrderDataAddition) getChild(groupPosition, childPosition)).ADDITIONAL_UNITPRICE));
            } else {
                txtName.setText(((String) getChild(groupPosition, childPosition)));
            }
        }

        //-- Hiden Price
        txtPrice.setVisibility(View.GONE);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
