package control.ExpandableList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import entity.Service.FolioOrder.FolioOrderData;
import entity.Service.FolioOrder.FolioOrderDataDetail;
import entity.Service.FolioOrder.FolioOrderEntity;
import entity.Service.FollowUp.FollowUpEntity;

public class MyFolioAdapter extends BaseExpandableListAdapter {

    Context context;
    String currencyDesc;
    FolioOrderEntity entity;
    public MyFolioAdapter(Context context,String currencyDesc,FolioOrderEntity entity){
        this.context = context;
        this.currencyDesc = currencyDesc;
        this.entity = entity;
        BaseApplication.CartStateVariable.setOnChangeListener(new BaseApplication.CartStateVariable.OnChange() {
            @Override
            public void OnChange(Integer length) {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getGroupCount() {
        return entity.Folio.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return entity.Folio.get(groupPosition).Detail.size();
    }

    @Override
    public FolioOrderData getGroup(int groupPosition) {
        return entity.Folio.get(groupPosition);
    }

    @Override
    public FolioOrderDataDetail getChild(int groupPosition, int childPosition) {
        return entity.Folio.get(groupPosition).Detail.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_myfolio_item,parent,false);
        }

        final FrameLayout frmList = (FrameLayout) convertView.findViewById(R.id.frmList);
        final TextView txtName = (TextView) convertView.findViewById(R.id.txtName);

        //Set Height
        ExpandableList expandableList = (ExpandableList)parent;
        if(expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) (parent.getHeight() / expandableList.getRowLength());
        }

        txtName.setText(getGroup(groupPosition).FOLIO_NAME);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_myfolio_subitem,parent,false);
        }

        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.txtTotalPrice);
        FrameLayout frmList = (FrameLayout) convertView.findViewById(R.id.frmList);

        //Set Height
        ExpandableList expandableList = (ExpandableList)parent;
        if(expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) ((parent.getHeight() / expandableList.getRowLength()) / 1.2);
        }
         txtDate.setText(entity.Folio.get(groupPosition).Detail.get(childPosition).POST_DATE.substring(0,10));
         txtName.setText(entity.Folio.get(groupPosition).Detail.get(childPosition).TRANS_NAME);
         txtPrice.setText(currencyDesc + new DecimalFormat("#,##0.00").format(entity.Folio.get(groupPosition).Detail.get(childPosition).AMOUNT));
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
