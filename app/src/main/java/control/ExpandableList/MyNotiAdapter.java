package control.ExpandableList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entity.Service.Notification.NotificationData;
import util.BroadcastUtil;


public class MyNotiAdapter extends BaseExpandableListAdapter {

    Context context;
    public MyNotiAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return BroadcastUtil.NotificationData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return BroadcastUtil.NotificationData.get(groupPosition).DATA.size();
    }

    @Override
    public String getGroup(int groupPosition) {
        return BroadcastUtil.NotificationData.get(groupPosition).DATE_RECORD;
    }

    @Override
    public NotificationData getChild(int groupPosition, int childPosition) {
        return BroadcastUtil.NotificationData.get(groupPosition).DATA.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_mynoti_date,parent,false);
        }

        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        FrameLayout frmList = (FrameLayout) convertView.findViewById(R.id.frmList);

        //Set Height
        ExpandableList expandableList = (ExpandableList)parent;
        if(expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) (parent.getHeight() / expandableList.getRowLength());
        }

        //Init Data
        SimpleDateFormat formFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat toFormat = new SimpleDateFormat("dd MMMM yyyy");
        try {
            Date date = formFormat.parse(getGroup(groupPosition));
            txtDate.setText(toFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        NotificationData data = getChild(groupPosition,childPosition);

            if(data.SENDER_CODE.contains("VZA") || data.SENDER_CODE.contains("Staff")){
                convertView = LayoutInflater.from(context).inflate(R.layout.adapter_mynoti_guest,parent,false);
            }else{
                convertView = LayoutInflater.from(context).inflate(R.layout.adapter_mynoti_admin,parent,false);
            }

        TextView txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
        TextView txtTime = (TextView) convertView.findViewById(R.id.txtTime);
        TextView txtRead = (TextView) convertView.findViewById(R.id.txtRead);
        RelativeLayout frmList = (RelativeLayout) convertView.findViewById(R.id.frmList);


        //Set Height
        ExpandableList expandableList = (ExpandableList)parent;
        if(expandableList.getRowLength() > 1) {
            frmList.getLayoutParams().height = (int) (parent.getHeight() / expandableList.getRowLength());
        }

        //Init Data
        txtMessage.setText(data.TEXT_NOTIFICATION);
        txtTime.setText(data.SYSTEM_DATE.substring(11,16));
        txtRead.setText("Read");
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
