package control.MenuList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import control.Dialog.EditDialog;
import control.View.AnimateView;
import entity.Service.MasterItem.MasterItemData;

public class LikeUnlikeAdapter extends ArrayAdapter<MasterItemData> {

    private final List<MasterItemData> fixEntity;
    static class ViewHolder{
        AnimateView imgView;
        TextView txtMessage;
        TextView txtPrice;
        FrameLayout frmList;
        CheckBox chkCom;
        CheckBox chkFav;
        CheckBox chkLike;
        CheckBox chkUnlike;
    }
    public LikeUnlikeAdapter(Context context,List<MasterItemData> objects) {
        super(context, R.layout.adapter_mylike_item, objects);
        fixEntity = new ArrayList<>();
        for(MasterItemData item : objects){
            try {
                fixEntity.add((MasterItemData)item.clone());
            } catch (CloneNotSupportedException e) {
                fixEntity.add(item);
                e.printStackTrace();
            }
        }
    }
    public List<MasterItemData> getEntityChange(){
        List<MasterItemData> entity = new ArrayList<>();
        for( int i = 0;i < fixEntity.size() ; i++){
           if(fixEntity.get(i).IS_FAVORITE != getItem(i).IS_FAVORITE) {
               entity.add(getItem(i));
           }else if(!fixEntity.get(i).COMMENT_DEP.equals(getItem(i).COMMENT_DEP)) {
               entity.add(getItem(i));
           }else if(fixEntity.get(i).IS_LIKE != getItem(i).IS_LIKE) {
               entity.add(getItem(i));
           }else if(fixEntity.get(i).IS_UNLIKE != getItem(i).IS_UNLIKE) {
               entity.add(getItem(i));
           }else if(fixEntity.get(i).IS_FAVORITE != getItem(i).IS_FAVORITE) {
               entity.add(getItem(i));
           }
        }
        return entity;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MasterItemData data = getItem(position);
        final ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_mylike_item,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.frmList = (FrameLayout)convertView.findViewById(R.id.frmList);
            viewHolder.imgView = (AnimateView) convertView.findViewById(R.id.imgView);
            viewHolder.txtMessage = (TextView)convertView.findViewById(R.id.txtMessage);
            viewHolder.txtPrice = (TextView)convertView.findViewById(R.id.txtPrice);
            viewHolder.chkCom = (CheckBox)convertView.findViewById(R.id.chkCom);
            viewHolder.chkFav = (CheckBox)convertView.findViewById(R.id.chkFav);
            viewHolder.chkLike = (CheckBox)convertView.findViewById(R.id.chkLike);
            viewHolder.chkUnlike = (CheckBox)convertView.findViewById(R.id.chkUnlike);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        //Set Height
        MenuList viewList = (MenuList)parent;
        if(viewList.getRowSize() > 1) {
            viewHolder.frmList.getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowSize());
        }

        //Set Image
        viewHolder.imgView.setImageService(data.MENU_IMAGENAME);

        //Set Text
        viewHolder.txtMessage.setText(data.DisplayText);
        viewHolder.txtPrice.setText(data.CURRENCY_DEP + new DecimalFormat("#,##0.00").format(data.MENU_UNITPRICE));

        //Set Check Box
        viewHolder.chkFav.setChecked(data.IS_FAVORITE);
        viewHolder.chkLike.setChecked(data.IS_LIKE);
        viewHolder.chkUnlike.setChecked(data.IS_UNLIKE);
        viewHolder.chkCom.setChecked(data.COMMENT_DEP.length() != 0);

        viewHolder.chkCom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                viewHolder.chkCom.setChecked(true);
                EditDialog dialog = EditDialog.newInstance(view,data.COMMENT_DEP);
                dialog.setOnSubmitListener(new EditDialog.OnSubmit() {
                    @Override
                    public void OnSubmit(String result) {
                       data.COMMENT_DEP = result.trim();
                    }
                });
                dialog.setOnDismissListener(new EditDialog.OnDismiss() {
                    @Override
                    public void OnDismiss() {
                        viewHolder.chkCom.setChecked(data.COMMENT_DEP.length() != 0);
                    }
                });
                dialog.show(((Activity)getContext()).getFragmentManager(),"tag");
            }
        });

        viewHolder.chkFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!data.IS_FAVORITE){
                    data.IS_FAVORITE = true;
                }else{
                    if(fixEntity.get(position).IS_FAVORITE){
                        viewHolder.chkFav.setChecked(true);
                    }else{
                        data.IS_FAVORITE = false;
                    }
                }
            }
        });

        viewHolder.chkLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.IS_LIKE = ((CheckBox)view).isChecked();
                if(data.IS_UNLIKE){
                    viewHolder.chkUnlike.setChecked(false);
                    data.IS_UNLIKE = false;
                }
            }
        });

        viewHolder.chkUnlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.IS_UNLIKE = ((CheckBox)view).isChecked();
                if(data.IS_LIKE){
                    viewHolder.chkLike.setChecked(false);
                    data.IS_LIKE = false;
                }
            }
        });

        return convertView;
    }
}
