package control.MenuList;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaActionSound;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationSet;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import control.View.AnimateView;
import entity.Service.MasterItem.MasterItemData;

public class MenuItemAdapter extends ArrayAdapter<MasterItemData> {

    static class  ViewHolder{
        AnimateView imgView;
        TextView txtMessage;
        TextView txtPrice;
        FrameLayout frmList;
        Button btnTocart;
    }

    private int viewMode = R.layout.adapter_menu_itemgroup_list;
    private String lookFor = "";
    private List<MasterItemData> mOriginalValues;   // Original Values
    private List<MasterItemData> mDisplayedValues;  // Values to be displayed

    private OnToCartClick clickToCartListener;
    private OnItemClick itemClickListener;
    public interface OnToCartClick {
        public void OnToCartClick(MasterItemData model);
    };
    public interface OnItemClick {
        public void OnItemClick(MasterItemData model);
    };
    public void setOnItemClickListener(OnToCartClick event){
        this.clickToCartListener = event;
    };
    public void setOnItemClickListener(OnItemClick event){
        this.itemClickListener = event;
    };

    public MenuItemAdapter(Context context, List<MasterItemData> objects,int viewMode,String lookFor) {
        super(context, viewMode, objects);
        this.viewMode = viewMode;
        this.lookFor = lookFor != null ? lookFor: "";

        //-- For Search
        this.mOriginalValues = objects;
        this.mDisplayedValues = objects;
    }
    public MenuItemAdapter(Context context, List<MasterItemData> objects,int viewMode) {
        super(context, viewMode, objects);
        this.viewMode = viewMode;

        //-- For Search
        this.mOriginalValues = objects;
        this.mDisplayedValues = objects;
    }


    @Override
    public int getCount() {
        return mDisplayedValues.size();
    }

    @Override
    public MasterItemData getItem(int position) {
        return mDisplayedValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {
                mDisplayedValues = (ArrayList<MasterItemData>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<MasterItemData> FilteredArrList = new ArrayList<MasterItemData>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<MasterItemData>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                /*
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                */
                if (constraint == null || constraint.length() == 0) {
                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        MasterItemData item = mOriginalValues.get(i);
                        //String data = mOriginalValues.get(i).TABLE_NO;
                        /*if (item.DisplayText.toLowerCase().startsWith(constraint.toString())
                                || item.MENU_CODE.toLowerCase().startsWith(constraint.toString())) {*/
                        if (item.DisplayText.toLowerCase().contains(constraint.toString())
                                //|| item.MENU_CODE.toLowerCase().contains(constraint.toString())
                                || item.MENU_UNITPRICE.toString().contains(constraint.toString())) {
                            //FilteredArrList.add(new TableListData(mOriginalValues.get(i).name,mOriginalValues.get(i).price));
                            FilteredArrList.add(item);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MasterItemData model = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(viewMode, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgView = (AnimateView)convertView.findViewById(R.id.imgView);
            viewHolder.txtMessage = (TextView)convertView.findViewById(R.id.txtMessage);
            viewHolder.txtPrice = (TextView)convertView.findViewById(R.id.txtPrice);
            viewHolder.frmList = (FrameLayout)convertView.findViewById(R.id.frmList);
            viewHolder.btnTocart = (Button)convertView.findViewById(R.id.btnTocart);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        //Set Height
        MenuList viewList = (MenuList)parent;
        if(viewList.getRowSize() > 1) {
            viewHolder.frmList.getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowSize());
        }

        //Set Image Url
        viewHolder.imgView.setImageService(model.MENU_IMAGENAME);

        //Set Text
        viewHolder.txtMessage.setText(model.DisplayText);
        if(lookFor.equals("SubGroup")){
            viewHolder.txtPrice.setText(model.CURRENCY_DEP + new DecimalFormat("#,##0.00").format(model.CHARGE_UNITPIRCE));
        }else {
            viewHolder.txtPrice.setText(model.CURRENCY_DEP + new DecimalFormat("#,##0.00").format(model.MENU_UNITPRICE));
        }
        //Set Btn Event
        viewHolder.btnTocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuItemAdapter.this.clickToCartListener.OnToCartClick(model);
            }
        });

        viewHolder.frmList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuItemAdapter.this.itemClickListener.OnItemClick(model);
            }
        });

        return convertView;
    }
}
