package control.MenuList;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.GridView;

import com.smartfinder.vzaapp.R;

import java.util.Timer;
import java.util.TimerTask;

import control.ViewList.ViewList;

public class MenuList extends GridView {
    private float rowSize;
    private Context context;
    public MenuList(Context context) {
        super(context);
        this.context = context;
    }
    public MenuList(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MenuList);
        rowSize = typedArray.getFloat(R.styleable.MenuList_rowSize,-1);
        typedArray.recycle();
    }
    public MenuList(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MenuList);
        rowSize = typedArray.getFloat(R.styleable.MenuList_rowSize,-1);
        typedArray.recycle();
    }
    public void setRowSize(float size){
        this.rowSize = size;
    }
    public float getRowSize(){
        return rowSize;
    }
    @Override
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.setEnabled(false);
        super.setOnItemClickListener(listener);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                ((Activity)context).runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                //Enable View
                                MenuList.this.setEnabled(true);
                            }
                        }
                );
            }
        }, 500);

    }
}
