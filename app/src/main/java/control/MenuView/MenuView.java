package control.MenuView;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import com.smartfinder.vzaapp.R;

public class MenuView extends ViewPager {
    public MenuView(Context context) {
        super(context);
        setPageTransformer(true, new PageTransformer());
    }
    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPageTransformer(false,new PageTransformer() );
    }
    public class PageTransformer implements ViewPager.PageTransformer {
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) {
                view.setAlpha(0);
            } else if (position <= 0) {
                view.setAlpha(position + 1);
                view.setTranslationX( (pageWidth * - position) + 1 );
            } else if (position <= 1) {
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);
            } else {
                view.setAlpha(0);
            }
        }

    }
}
