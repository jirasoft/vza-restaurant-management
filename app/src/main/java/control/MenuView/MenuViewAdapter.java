package control.MenuView;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import util.ViewUtil;

public class MenuViewAdapter extends FragmentStatePagerAdapter {

    private LinkedList<Fragment> lisFragment;

    public MenuViewAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.lisFragment = new LinkedList<>();
    }

    public void addItem(MenuView menuView,Fragment fragment){

        //Disable MenuView
        ViewUtil.isEnableView(menuView,false);

        //Remove Old Page
        for(int i = lisFragment.size() - 1;i >  menuView.getCurrentItem();i--){
            lisFragment.remove(i);
        }

        //Check Old Fragment
        for(Fragment oldFragment : lisFragment){
            if(((Object)fragment).getClass().getSimpleName().equals(((Object)oldFragment).getClass().getSimpleName())){
                ViewUtil.isEnableView(menuView,true);
                notifyDataSetChanged();
                return;
            }
        }

        //Add New Page
        lisFragment.addLast(fragment);

        //Notify DataSet
        notifyDataSetChanged();

        //Select MenuView
        menuView.setCurrentItem(getCount() - 1, true);

        //Enable MenuView
        ViewUtil.isEnableView(menuView,true);

    }

    @Override
    public int getItemPosition(Object object) {
       return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        return lisFragment.get(position);
    }

    @Override
    public int getCount() {
        return lisFragment.size();
    }

}
