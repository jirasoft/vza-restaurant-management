package control.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.smartfinder.vzaapp.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import util.CacheImageUtil;

public class AnimateView extends ImageView {

    boolean animatedGifImage = false;
    private InputStream is = null;
    private Movie mMovie = null;
    private long mMovieStart = 0;
    private TYPE mType = TYPE.FIT_CENTER;
    public static enum TYPE {
        FIT_CENTER, STREACH_TO_FIT, AS_IS
    };
    public AnimateView(Context context, AttributeSet attrs,int defStyle) {
        super(context, attrs, defStyle);
        set(R.drawable.pic_defer, AnimateView.TYPE.AS_IS);
    }
    public AnimateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        set(R.drawable.pic_defer, AnimateView.TYPE.AS_IS);
    }
    public AnimateView(Context context) {
        super(context);
        set(R.drawable.pic_defer, AnimateView.TYPE.AS_IS);
    }
    private void onInit(){
        set(R.drawable.pic_defer, AnimateView.TYPE.AS_IS);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        animatedGifImage = false;
        mType = TYPE.STREACH_TO_FIT;
        if (bm != null) {
            super.setImageBitmap(null);
            super.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fadein));
            super.setBackground(new BitmapDrawable(getContext().getResources(), bm));
        }
    }

    public void setImageService(String site){
        if (cancelPotentialDownload(this)) {
            this.onInit();
            AnimateServiceUtil task = new AnimateServiceUtil(this);
            DownloadedService downloadedService = new DownloadedService(task);
            this.setBackground(downloadedService);
            if(CacheImageUtil.hasImage(getContext(), site)){
                setImageBitmap(CacheImageUtil.getImage(getContext(),site));
            }else{
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,site);
            }
        }
    }
    public class AnimateServiceUtil extends AsyncTask<String,Void,Bitmap> {
        private final WeakReference<AnimateView> view;
        public AnimateServiceUtil(AnimateView view){
            this.view = new WeakReference<>(view);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                if(CacheImageUtil.hasImage(getContext(), params[0])){
                    return CacheImageUtil.getImage(getContext(),params[0]);
                }

                URL url = new URL(params[0]);
                Log.wtf("ImgList", params[0]);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                //connection.setDoInput(true);
                connection.setRequestProperty("User-Agent", CoreProtocolPNames.USER_AGENT);
                connection.setRequestProperty("Connection", "close");
                connection.setConnectTimeout(100); // timeout is in milliseconds
                connection.connect();

                //URLConnection connection = url.openConnection();
                //connection.setConnectTimeout(R.integer.IMG_CONN_TIMEOUT); //-- Set timeout for load images
                //connection.setReadTimeout(R.integer.IMG_CONN_TIMEOUT);

                Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
                //Add Image To Cache
                CacheImageUtil.addImage(getContext(),params[0],bitmap);
                return bitmap;
            } catch (SocketTimeoutException e) {
                System.out.println("More than " + R.integer.IMG_CONN_TIMEOUT + " elapsed.");
                Log.wtf("ImgList Timeout", params[0]);
                //-- Fix issue no image and path not true
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.no_image);
                //Add Image To Cache
                CacheImageUtil.addImage(getContext(),params[0],bitmap);

                return bitmap;

            }
            catch (IOException e) {
                e.printStackTrace();
                //return null;

                Log.wtf("ImgList IOException", e.getMessage());

                //-- Fix issue no image and path not true
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.no_image);
                //Add Image To Cache
                CacheImageUtil.addImage(getContext(),params[0],bitmap);

                return bitmap;
            }
        }
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            ImageView imageView = view.get();
            AnimateServiceUtil bitmapDownloaderTask = getServiceUtilTask(imageView);
            if (this == bitmapDownloaderTask) {
                 imageView.setImageBitmap(bitmap);
            }
        }
    }
    static class DownloadedService extends ColorDrawable {
        private final WeakReference<AnimateServiceUtil> serviceUtilReference;
        public DownloadedService(AnimateServiceUtil downloaderTask) {
            serviceUtilReference = new WeakReference<>(downloaderTask);
        }
        public AnimateServiceUtil getAnimateServiceUtil() {
            return serviceUtilReference.get();
        }
    }
    private static boolean cancelPotentialDownload(ImageView imageView) {
        AnimateServiceUtil bitmapDownloaderTask = getServiceUtilTask(imageView);
        if (bitmapDownloaderTask != null) {
                bitmapDownloaderTask.cancel(true);
        }
        return true;
    }
    private static AnimateServiceUtil getServiceUtilTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getBackground();
            if (drawable instanceof DownloadedService) {
                DownloadedService downloadedDrawable = (DownloadedService)drawable;
                return downloadedDrawable.getAnimateServiceUtil();
            }
        }
        return null;
    }

    Paint p;
    private float mScaleH = 1f, mScaleW = 1f;
    private int mMeasuredMovieWidth;
    private int mMeasuredMovieHeight;
    private float mLeft;
    private float mTop;
    private void set(int rawResourceId, TYPE streachType) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        mType = streachType;
        animatedGifImage = true;
        is = getContext().getResources().openRawResource(rawResourceId);
        try {
            mMovie = Movie.decodeStream(is);
        } catch (Exception e) {
            e.printStackTrace();
            byte[] array = streamToBytes(is);
            mMovie = Movie.decodeByteArray(array, 0, array.length);
        }
        p = new Paint();
    }
    private static byte[] streamToBytes(InputStream is) {
        ByteArrayOutputStream os = new ByteArrayOutputStream(1024);
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = is.read(buffer)) >= 0) {
                os.write(buffer, 0, len);
            }
        } catch (java.io.IOException e) {
        }
        return os.toByteArray();
    }
    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mMovie != null) {
            int movieWidth = mMovie.width();
            int movieHeight = mMovie.height();

            int measureModeWidth = MeasureSpec.getMode(widthMeasureSpec);
            float scaleW = 1f, scaleH = 1f;
            if (measureModeWidth != MeasureSpec.UNSPECIFIED) {
                int maximumWidth = MeasureSpec.getSize(widthMeasureSpec);
                if (movieWidth > maximumWidth) {
                    scaleW = (float) movieWidth / (float) maximumWidth;
                } else {
                    scaleW = (float) maximumWidth / (float) movieWidth;
                }
            }

            int measureModeHeight = MeasureSpec.getMode(heightMeasureSpec);

            if (measureModeHeight != MeasureSpec.UNSPECIFIED) {
                int maximumHeight = MeasureSpec.getSize(heightMeasureSpec);
                if (movieHeight > maximumHeight) {
                    scaleH = (float) movieHeight / (float) maximumHeight;
                } else {
                    scaleH = (float) maximumHeight / (float) movieHeight;
                }
            }


            switch (mType) {
                case FIT_CENTER:
                    mScaleH = mScaleW = Math.min(scaleH, scaleW);
                    break;
                case AS_IS:
                    mScaleH = mScaleW = 1f;
                    break;
                case STREACH_TO_FIT:
                    mScaleH = scaleH;
                    mScaleW = scaleW;
                    break;
            }

            mMeasuredMovieWidth = (int) (movieWidth * mScaleW);
            mMeasuredMovieHeight = (int) (movieHeight * mScaleH);

            setMeasuredDimension(mMeasuredMovieWidth, mMeasuredMovieHeight);

        } else {
            setMeasuredDimension(getSuggestedMinimumWidth(), getSuggestedMinimumHeight());
        }
    }
    @Override protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        mLeft = (getWidth() - mMeasuredMovieWidth) / 2f;
        mTop = (getHeight() - mMeasuredMovieHeight) / 2f;
    }
    @Override protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (animatedGifImage) {
            long now = android.os.SystemClock.uptimeMillis();
            if (mMovieStart == 0) { // first time
                mMovieStart = now;
            }
            if (mMovie != null) {
                p.setAntiAlias(true);
                p.setDither(true);
                int dur = mMovie.duration();
                int relTime = (int) ((now - mMovieStart) % dur);
                mMovie.setTime(relTime);
                canvas.save(Canvas.MATRIX_SAVE_FLAG);
                canvas.scale(mScaleW, mScaleH);
                mMovie.draw(canvas, mLeft / mScaleW, mTop / mScaleH);
                canvas.restore();
                invalidate();
            }
        }

    }
}
