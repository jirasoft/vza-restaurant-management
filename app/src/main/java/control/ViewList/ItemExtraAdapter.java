package control.ViewList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ItemExtraAdapter extends ArrayAdapter<ItemExtraModel> {

    private List<ItemExtraModel> objects;

    public class ViewHolder {
        public FrameLayout frmList;
        public TextView txtMessage;
        public TextView txtPrice;
        public CheckBox chkSelect;
    }

    private OnSelectedChange selectedChangeListener;
    public interface OnSelectedChange {
        public void OnSelectedChange();
    };

    public void setOnSelectedChangeListener(OnSelectedChange event) {
        this.selectedChangeListener = event;
    };

    public ItemExtraAdapter(Context context, int resource, List<ItemExtraModel> objects) {
        super(context, resource, objects);
        this.objects = new ArrayList<>(objects);
    }

    @Override
    public int getViewTypeCount() {
        return ItemExtraModel.ItemType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return objects.get(position).getType().ordinal();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        int itemExtraType = getItemViewType(position);

        if (convertView == null) {
            if (itemExtraType == ItemExtraModel.ItemType.HEADER.ordinal()) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_menu_extra_header, parent, false);
            } else {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_menu_extra, parent, false);
            }
            viewHolder = new ViewHolder();
            viewHolder.frmList = (FrameLayout) convertView.findViewById(R.id.frmItemList);
            viewHolder.txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
            viewHolder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
            viewHolder.chkSelect = (CheckBox) convertView.findViewById(R.id.chkSelect);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //Init Data
        ItemExtraModel itemExtra = objects.get(position);

        //Set Visible
        if (!itemExtra.getHasCheck()) {
            viewHolder.chkSelect.setVisibility(View.INVISIBLE);
            viewHolder.txtPrice.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.chkSelect.setVisibility(View.VISIBLE);
            viewHolder.txtPrice.setText(itemExtra.getPrice());
            viewHolder.txtPrice.setVisibility(View.VISIBLE);
        }

        //Set Height
        ViewList viewList = (ViewList) parent;
        if (viewList.getRowCount() > 1) {
            viewHolder.frmList.getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowCount());
        }

        viewHolder.txtMessage.setText(itemExtra.getMessage());

        //if (itemExtra.getUnitPrice() > 0.00) {
        //    viewHolder.txtPrice.setText(itemExtra.getPrice());
        //    viewHolder.txtPrice.setVisibility(View.VISIBLE);
        /*} else {
            viewHolder.txtPrice.setVisibility(View.INVISIBLE);
        }*/

        viewHolder.chkSelect.setChecked(itemExtra.getIsCheck());
        viewHolder.chkSelect.setTag(itemExtra);
        viewHolder.chkSelect.setClickable(false);
        viewHolder.frmList.setOnClickListener(chkSelect_OnClick);
        return convertView;
    }

    public List<ItemExtraModel> getItems() {
        return new ArrayList<>(objects);
    }

    public View.OnClickListener chkSelect_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ItemExtraModel itemExtraModel = (ItemExtraModel) view.findViewById(R.id.chkSelect).getTag();

            //Check Model
            List<ItemExtraModel> keepModel = new ArrayList<>();
            for (ItemExtraModel model : objects) {
                if (model.getSupGroupId().equals(itemExtraModel.getSupGroupId()) && model.getIsCheck() == true) {
                    keepModel.add(model);
                }
            }

            switch (itemExtraModel.getGroup()) {
                case SELECT:
                    //Sort Last Check
                    Collections.sort(keepModel, new Comparator<ItemExtraModel>() {
                        @Override
                        public int compare(ItemExtraModel lhs, ItemExtraModel rhs) {
                            return lhs.lastCheck().compareTo(rhs.lastCheck());
                        }
                    });

                    //Check Keep Data
                    List<ItemExtraModel> checkModel = new ArrayList<>();
                    for (int i = keepModel.size() - 1; i >= 0; i--) {
                        if (checkModel.size() < itemExtraModel.getQuota() - 1 && !keepModel.get(i).getId().equals(itemExtraModel.getId())) {
                            checkModel.add(keepModel.get(i));
                        }
                    }

                    //Clean Checker
                    for (ItemExtraModel model : keepModel) {
                        if (model.getSupGroupId().equals(itemExtraModel.getSupGroupId())) {
                            model.setIsCheck(false);
                        }
                    }

                    //Check Data
                    for (ItemExtraModel model : checkModel) {
                        model.setIsCheck(true);
                    }

                    itemExtraModel.setIsCheck(true);
                    break;
                case FIX:
                    itemExtraModel.setIsCheck(true);
                    break;
                case CHANGE:
                    //Keep Quota
                    if (keepModel.size() < itemExtraModel.getQuota()) {
                        itemExtraModel.setIsCheck(!itemExtraModel.getIsCheck());
                    } else {
                        itemExtraModel.setIsCheck(false);
                    }
                    break;
                default:
                    itemExtraModel.setIsCheck(!itemExtraModel.getIsCheck());
                    break;
            }
            notifyDataSetChanged();
            ItemExtraAdapter.this.selectedChangeListener.OnSelectedChange();
        }
    };
}



