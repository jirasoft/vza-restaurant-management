package control.ViewList;

import java.text.DecimalFormat;
import java.util.Date;

public class ItemExtraModel<T> {

    private Integer id = 0;
    private Integer quota = 0;
    private String message = "";
    private String currency = "";
    private Double price = 0.00;
    private Integer supgroupid = 0;

    private ItemType type = ItemType.NONE;
    private GroupType group = GroupType.NONE;

    private Boolean isCheck = false;
    private Boolean hasCheck = false;
    private Long lastCheck = System.nanoTime();

    private T entity;

    public enum ItemType{
        NONE,HEADER,DETAIL
    }
    public enum GroupType{
        NONE,CONDIMENT,ADDITIONAL,SELECT,FIX,CHANGE,UPSIZE
    }

    public ItemExtraModel(GroupType group){
        this.group = group;
    }
    public ItemExtraModel(GroupType group,Integer supgroupid,Integer quota){
        this.group = group;
        this.quota = quota;
        this.supgroupid = supgroupid;
    }
    public ItemExtraModel setHeader(String message,Boolean hasCheck){
        this.type = ItemType.HEADER;
        this.message = message;
        this.hasCheck = hasCheck;
        return this;
    }
    public ItemExtraModel setHeader(String message,String currency,Double price,Boolean hasCheck){
       this.type = ItemType.HEADER;
       this.message = message;
       this.currency = currency;
       this.price = price;
       this.hasCheck = hasCheck;
       return this;
    }
    public ItemExtraModel setDetail(Integer id,String message,Boolean hasCheck,Boolean isCheck,T entity){
        this.type = ItemType.DETAIL;
        this.id = id;
        this.message = message;
        this.hasCheck = hasCheck;
        this.isCheck = isCheck;
        this.entity = entity;
        return this;
    }
    public ItemExtraModel setDetail(Integer id,String message,String currency,Double price,Boolean hasCheck,Boolean isCheck,T entity){
        this.type = ItemType.DETAIL;
        this.id = id;
        this.message = message;
        this.currency = currency;
        this.price = price;
        this.hasCheck = hasCheck;
        this.isCheck = isCheck;
        this.entity = entity;
        return this;
    }
    public Integer getId(){ return this.id; }
    public Integer getQuota() { return this.quota; }
    public Integer getSupGroupId(){
        return this.supgroupid;
    }
    public String getMessage(){ return this.message; }
    public String getPrice(){ return this.currency + new DecimalFormat("#,##0.00").format(this.price); }
    public Double getUnitPrice(){ return this.price; };
    public GroupType getGroup(){return this.group; };
    public ItemType getType(){
      return this.type;
    };
    public Boolean getIsCheck(){
        return this.isCheck;
    };
    public Boolean getHasCheck(){
        return this.hasCheck;
    };
    public T getEntity(){ return this.entity; };
    public void setIsCheck(Boolean isCheck){this.isCheck = isCheck;
                                            this.lastCheck = System.nanoTime();};
    public Long lastCheck(){return this.lastCheck;};
}
