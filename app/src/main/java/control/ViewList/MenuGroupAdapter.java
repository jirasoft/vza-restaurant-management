package control.ViewList;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;

import control.View.AnimateView;

public class MenuGroupAdapter extends ArrayAdapter<MenuGroupModel> {

    static class  MenuGroupViewHolder{
        AnimateView imgView;
        TextView txtMessage;
        FrameLayout frmList;
    }

    public MenuGroupAdapter(Context context, List<MenuGroupModel> objects) {
        super(context, R.layout.adapter_menu_group, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MenuGroupModel model = getItem(position);
        final MenuGroupViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_menu_group, parent, false);
            viewHolder = new MenuGroupViewHolder();
            viewHolder.imgView = (AnimateView)convertView.findViewById(R.id.imgView);
            viewHolder.txtMessage = (TextView)convertView.findViewById(R.id.txtMessage);
            viewHolder.frmList = (FrameLayout)convertView.findViewById(R.id.frmList);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (MenuGroupViewHolder)convertView.getTag();
        }

        //Set Height
        ViewList viewList = (ViewList)parent;
        if(viewList.getRowCount() > 1) {
            viewHolder.frmList.getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowCount());
        }

        //-- Edit 18/02/2015
        /*AnimationSet set = new AnimationSet(false);
        set.setDuration(500);
        set.start();
        viewHolder.imgView.setAnimation(set);*/
        //-- End Edit

        Log.wtf("Image Url", model.imageUrl);

        //Set Image Url
        viewHolder.imgView.setImageService(model.imageUrl);

        //Set Text
        viewHolder.txtMessage.setText(model.message);

        //Set Tag
        viewHolder.txtMessage.setTag(model.tag);

        return convertView;
    }
}
