package control.ViewList;

public class MenuGroupModel {
    public String imageUrl;
    public String message;
    public String tag;

    public MenuGroupModel(String imageUrl, String message,String tag){
        this.imageUrl = imageUrl;
        this.message = message;
        this.tag = tag;
    }
}
