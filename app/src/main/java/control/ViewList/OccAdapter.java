package control.ViewList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import entity.Main.OccInfo.OccInfoData;

public class OccAdapter extends ArrayAdapter<OccInfoData> {
    public OccAdapter(Context context, List<OccInfoData> objects) {
        super(context, R.layout.adapter_mastervza_occ, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OccInfoData item = getItem(position);
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_mastervza_occ,parent, false);

        //Set Height
        ViewList viewList = (ViewList)parent;
        if(viewList.getRowCount() > 1) {
            convertView.findViewById(R.id.frmList).getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowCount());
        }

        //Set BG
        if(item.IS_TYPE == 1){
            convertView.findViewById(R.id.frmList).setBackgroundColor(convertView.getResources().getColor(R.color.red));
        }else if(item.IS_TYPE == 2){
            convertView.findViewById(R.id.frmList).setBackgroundColor(convertView.getResources().getColor(R.color.bg));
        }else{
            convertView.findViewById(R.id.frmList).setBackgroundColor(convertView.getResources().getColor(R.color.white));
        }

        ((TextView)convertView.findViewById(R.id.txtDD)).setText(item.DD);
        ((TextView)convertView.findViewById(R.id.txtRoom)).setText(item.TOTAL_ROOM_SOLD.toString());
        ((TextView)convertView.findViewById(R.id.txtOCC)).setText(new DecimalFormat("#0.00").format(item.OCC_PERCENT) + "%");
        ((TextView)convertView.findViewById(R.id.txtADR)).setText(new DecimalFormat("#,###0.00").format(item.ADR));
        ((TextView)convertView.findViewById(R.id.txtRoomRev)).setText(new DecimalFormat("#,###0.00").format(item.REVENUE));
        return convertView;
    }
}
