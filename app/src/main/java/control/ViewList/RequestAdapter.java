package control.ViewList;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.util.ArrayList;
import java.util.List;

import control.View.AnimateView;

public class RequestAdapter extends ArrayAdapter<RequestModel> {

    private List<RequestModel> objects;

    public class ViewHolder{
        public FrameLayout frmList;
        public AnimateView imgView;
        public TextView txtMessage;
    }

    public RequestAdapter(Context context, List<RequestModel> objects) {
        super(context, R.layout.fragment_menu_request, objects);
        this.objects = new ArrayList<>(objects);
    }

    @Override
    public int getViewTypeCount() {
        return ItemExtraModel.ItemType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return objects.get(position).getType().ordinal();
    }

    public List<RequestModel> getItems() {
        return new ArrayList<>(objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            if(getItemViewType(position) == RequestModel.ItemType.HEADER.ordinal()){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_menu_request_header,parent, false);
                viewHolder = new ViewHolder();
                viewHolder.frmList = (FrameLayout) convertView.findViewById(R.id.frmList);
                viewHolder.txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
                convertView.setTag(viewHolder);
            }else{
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_menu_request,parent, false);
                viewHolder = new ViewHolder();
                viewHolder.frmList = (FrameLayout) convertView.findViewById(R.id.frmList);
                viewHolder.txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
                viewHolder.imgView = (AnimateView) convertView.findViewById(R.id.imgView);
                convertView.setTag(viewHolder);
            }
        }else{
           viewHolder = (ViewHolder)convertView.getTag();
        }

        ViewList viewList = (ViewList)parent;

        //Init Data
        RequestModel request = objects.get(position);
        viewHolder.txtMessage.setText(request.getMessage());

        if(getItemViewType(position) == RequestModel.ItemType.DETAIL.ordinal()){
            //Set Height
            if(viewList.getRowCount() > 1) {
                viewHolder.frmList.getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowCount());
            }

            //Set Image Url
            viewHolder.imgView.setImageService(request.getImageUrl());
        }

        //Set Tag
        viewHolder.txtMessage.setTag(request);

        return convertView;
    }
}
