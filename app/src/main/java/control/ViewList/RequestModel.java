package control.ViewList;

public class RequestModel {

    private Integer id = 0;
    private Integer systemid = 0;
    private String message = "";
    private String imageUrl = "";

    private ItemType type = ItemType.NONE;
    public enum ItemType{
        NONE,HEADER,DETAIL
    }
    public RequestModel setHeader(Integer id,Integer systemid,String message){
        this.type = ItemType.HEADER;
        this.id = id;
        this.message = message;
        this.systemid = systemid;
        return this;
    }
    public RequestModel setDetail(Integer id,Integer systemid,String message,String imageUrl){
        this.type = ItemType.DETAIL;
        this.id = id;
        this.message = message;
        this.systemid = systemid;
        this.imageUrl = imageUrl;
        return this;
    }
    public String getMessage(){
        return this.message;
    }
    public Integer getSystemid(){
        return this.systemid;
    }
    public ItemType getType(){return this.type;}
    public Integer getId(){
        return this.id;
    }
    public String getImageUrl(){
        return this.imageUrl;
    }
}
