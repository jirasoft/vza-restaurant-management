package control.ViewList;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.util.List;

import control.View.AnimateView;

public class ServiceAdapter extends ArrayAdapter<ServiceModel> {

    static class ViewHolder{
        AnimateView imgView;
        TextView txtMessage;
        LinearLayout frmList;
    }

    public ServiceAdapter(Context context, List<ServiceModel> objects) {
        super(context, R.layout.adapter_mastervza_service, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ServiceModel model = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_mastervza_service, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgView = (AnimateView)convertView.findViewById(R.id.imgView);
            viewHolder.txtMessage = (TextView)convertView.findViewById(R.id.txtMessage);
            viewHolder.frmList = (LinearLayout)convertView.findViewById(R.id.frmList);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        //Set Height
        ViewList viewList = (ViewList)parent;
        if(viewList.getRowCount() > 1) {
            viewHolder.frmList.getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowCount());
        }
        //Set Image Url
        viewHolder.imgView.setImageService(model.imageUrl);

        //Set Text
        viewHolder.txtMessage.setText(model.message);

        return convertView;
    }
}
