package control.ViewList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;
import com.smartfinder.vzaapp.SideBar.TableListActivity;
import com.smartfinder.vzaapp.SideBar.TableVZAListActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import control.Dialog.ChooseDialog;
import emuntype.SiteOperate;
import entity.BaseEntity;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.TableList.TableListData;
import entity.Service.TableList.TableListEntity;
import util.ServiceUtil;
import util.ViewUtil;

import static android.support.v4.app.ActivityCompat.startActivity;
import static android.support.v4.app.ActivityCompat.startActivityForResult;

public class TableListAdapter extends ArrayAdapter<TableListData> {

    private String currencyDep;
    private Context context;
    private List<TableListData> mOriginalValues;    // Original Values
    private List<TableListData> mDisplayedValues;   // Values to be displayed
    LayoutInflater inflater;

    public TableListAdapter(Context context, List<TableListData> objects, String currencyDep) {
        super(context, R.layout.adapter_tablelist, objects);
        this.currencyDep = currencyDep;

        //-- New Code for Search
        this.context = context;
        //this.tableList = objects;
        this.mOriginalValues = objects;
        this.mDisplayedValues = objects;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return mDisplayedValues.size();
    }

    @Override
    public TableListData getItem(int position) {
        return mDisplayedValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        LinearLayout llContainer;
        TextView txtTableNo, txtPax, txtAmount;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDisplayedValues = (ArrayList<TableListData>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<TableListData> FilteredArrList = new ArrayList<TableListData>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<TableListData>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                /*
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                */
                if (constraint == null || constraint.length() == 0) {
                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        TableListData item = mOriginalValues.get(i);
                        //String data = mOriginalValues.get(i).TABLE_NO;
                        //if (data.toLowerCase().startsWith(constraint.toString())) {
                        if (item.TABLE_NO.toLowerCase().startsWith(constraint.toString())) {
                            //FilteredArrList.add(new TableListData(mOriginalValues.get(i).name,mOriginalValues.get(i).price));
                            FilteredArrList.add(item);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        TableListData item = getItem(position);
        //convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_tablelist,parent, false);
        convertView = inflater.inflate(R.layout.adapter_tablelist, parent, false);

        //Set Height
        ViewList viewList = (ViewList) parent;
        if (viewList.getRowCount() > 1) {
            convertView.findViewById(R.id.frmList).getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowCount());
        }

        ((TextView) convertView.findViewById(R.id.txtTableNo)).setText(item.TABLE_NO);
        ((TextView) convertView.findViewById(R.id.txtPax)).setText(item.COVER.toString());
        if (item.TOTAL > 0) {
            ((TextView) convertView.findViewById(R.id.txtAmount)).setText(currencyDep + new DecimalFormat("#,##0.00").format(item.TOTAL));
        } else {
            ((TextView) convertView.findViewById(R.id.txtAmount)).setText(currencyDep + new DecimalFormat("#,##0.00").format(item.getAmount()));
        }

        //Init Event
        final View setView = convertView;
        final String setTableNo = item.TABLE_NO;
        final Integer setCover = item.COVER;

        final NumberPicker pikPax = (NumberPicker) setView.findViewById(R.id.pikPax);
        pikPax.setMinValue(0);
        pikPax.setMaxValue(99);
        pikPax.setWrapSelectorWheel(true);
        ((Button) setView.findViewById(R.id.btnPixSubmit)).setText(getContext().getString(R.string.btn_cancel));
        pikPax.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (pikPax.getValue() == setCover) {
                    ((Button) setView.findViewById(R.id.btnPixSubmit)).setText(getContext().getString(R.string.btn_cancel));
                } else {
                    ((Button) setView.findViewById(R.id.btnPixSubmit)).setText(getContext().getString(R.string.btn_submit));

                    //-- Change Pax
                    final CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(getContext(), CheckLocationEntity.class);
                    final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getContext(), HotelInfoEntity.class);
                    String site = hotelInfo.DATA.HOTEL_URL;
                    String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

                    //Cover Param
                    entity.Host.TableList.TableListData cover = new entity.Host.TableList.TableListData();
                    cover.TABLE_NO = getItem(position).TABLE_NO;
                    cover.BILL_NO = getItem(position).BILL_NO;
                    cover.COVER = pikPax.getValue();
                    List<entity.Host.TableList.TableListData> lisCover = new ArrayList<>();
                    lisCover.add(cover);

                    //Call Table Open
                    List<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("LOCATION_IDPata", locationEntity.LOCATION_ID.toString()));
                    param.add(new BasicNameValuePair("COVERPara", new Gson().toJson(lisCover)));

                    ServiceUtil serviceUtil = new ServiceUtil(getContext(), site, keycode, SiteOperate.VZAUpdateCoverTablelistJSON, BaseEntity.class);
                    serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                            getItem(position).COVER = pikPax.getValue();
                            ((TextView) setView.findViewById(R.id.txtPax)).setText(Integer.toString(pikPax.getValue()));
                            notifyDataSetChanged();
                        }
                    });
                    serviceUtil.execute(param);
                }
            }
        });

        convertView.findViewById(R.id.txtTableNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckLocationEntity checkLocation = BaseApplication.GlobalVariable.get(getContext(), CheckLocationEntity.class);
                if (checkLocation.TABLE_NO != setTableNo) {
                    ChooseDialog dialog = ChooseDialog.newInstance((Activity) getContext(), getContext().getString(R.string.alert_comfirm_changetable), true);
                    dialog.setOnSubmitListener(new ChooseDialog.OnSubmit() {
                        @Override
                        public void OnSubmit(View view) {
                            Intent intent = new Intent();
                            intent.putExtra("table", setTableNo);
                            intent.putExtra("pax", setCover);
                            ((Activity) getContext()).setResult(Activity.RESULT_OK, intent);
                            ((Activity) getContext()).finish();
                        }
                    });
                    dialog.show(((Activity) getContext()).getFragmentManager(), "tag");
                }
            }
        });

        convertView.findViewById(R.id.txtPax).setFocusableInTouchMode(false);  // Controls Key board
        convertView.findViewById(R.id.txtPax).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Init PaxView
                pikPax.setValue(setCover);

                setView.findViewById(R.id.txtPax).setVisibility(View.GONE);
                setView.findViewById(R.id.txtAmount).setVisibility(View.GONE);
                setView.findViewById(R.id.pikPax).setVisibility(View.VISIBLE);
                setView.findViewById(R.id.btnPixSubmit).setVisibility(View.VISIBLE);
            }
        });

        convertView.findViewById(R.id.btnPixSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Change Value
                if (pikPax.getValue() != setCover) {
                    final CheckLocationEntity locationEntity = BaseApplication.GlobalVariable.get(getContext(), CheckLocationEntity.class);
                    final HotelInfoEntity hotelInfo = BaseApplication.GlobalVariable.get(getContext(), HotelInfoEntity.class);
                    String site = hotelInfo.DATA.HOTEL_URL;
                    String keycode = hotelInfo.DATA.HOTEL_KEYCODE;

                    //Cover Param
                    entity.Host.TableList.TableListData cover = new entity.Host.TableList.TableListData();
                    cover.TABLE_NO = getItem(position).TABLE_NO;
                    cover.BILL_NO = getItem(position).BILL_NO;
                    cover.COVER = pikPax.getValue();
                    List<entity.Host.TableList.TableListData> lisCover = new ArrayList<>();
                    lisCover.add(cover);

                    //Call Table Open
                    List<NameValuePair> param = new ArrayList<>();
                    param.add(new BasicNameValuePair("LOCATION_IDPata", locationEntity.LOCATION_ID.toString()));
                    param.add(new BasicNameValuePair("COVERPara", new Gson().toJson(lisCover)));

                    ServiceUtil serviceUtil = new ServiceUtil(getContext(), site, keycode, SiteOperate.VZAUpdateCoverTablelistJSON, BaseEntity.class);
                    serviceUtil.setFinishedListener(new ServiceUtil.OnFinished() {
                        @Override
                        public void OnFinished(Object results) {
                            getItem(position).COVER = pikPax.getValue();
                            ((TextView) setView.findViewById(R.id.txtPax)).setText(Integer.toString(pikPax.getValue()));
                            notifyDataSetChanged();
                        }
                    });
                    serviceUtil.execute(param);
                }
                //Init View
                setView.findViewById(R.id.txtPax).setVisibility(View.VISIBLE);
                setView.findViewById(R.id.txtAmount).setVisibility(View.VISIBLE);
                setView.findViewById(R.id.pikPax).setVisibility(View.GONE);
                setView.findViewById(R.id.btnPixSubmit).setVisibility(View.GONE);
            }
        });

        final View finalConvertView = convertView;
        convertView.findViewById(R.id.btnVZACode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TableListEntity entity = BaseApplication.GlobalVariable.get(getContext(), TableListEntity.class);
                for(int i=0;i< entity.TABLEDATALIST.size();i++){
                    if(entity.TABLEDATALIST.get(i).TABLE_NO.equals(setTableNo)){
                        if(entity.TABLEDATALIST.get(i).VZA_LIST.size()>0){
                            ViewUtil.isEnableView(finalConvertView.getRootView(), false);

                            Intent intent = new Intent(getContext(), TableVZAListActivity.class);
                            intent.putExtra("TABLE_NO", setTableNo);
                            intent.putExtra("VZA_LIST", (Serializable) entity.TABLEDATALIST.get(i).VZA_LIST);
                            startActivity((Activity) getContext(),intent, Bundle.EMPTY);
                        }else{
                            ChooseDialog dialog = ChooseDialog.newInstance((Activity) getContext(),
                                    getContext().getString(R.string.alert_vzalist_empty), false);
                            dialog.show(((Activity) getContext()).getFragmentManager(), "tag");
                        }
                        break;
                    }
                }
            }
        });

        return convertView;
    }
}
