package control.ViewList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.smartfinder.vzaapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VZAListAdapter extends ArrayAdapter<VZAListModel> {

    private List<VZAListModel> objects;

    public class ViewHolder {
        public FrameLayout frmList;
        public CheckBox chkSelect;
        public TextView txtGuestName;
        public TextView txtTelephoneNo;
        public TextView txtVZACode;
    }

    private OnSelectedChange selectedChangeListener;

    public interface OnSelectedChange {
        public void OnSelectedChange();
    }

    ;

    public void setOnSelectedChangeListener(OnSelectedChange event) {
        this.selectedChangeListener = event;
    }

    ;

    public VZAListAdapter(Context context, int resource, List<VZAListModel> objects) {
        super(context, resource, objects);
        this.objects = new ArrayList<>(objects);
    }

    /*@Override
    public int getViewTypeCount() {
        return VZAListModel.ItemType.values().length;
    }*/

    /*@Override
    public int getItemViewType(int position) {
        return objects.get(position).getType().ordinal();
    }*/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_table_vza_list, parent, false);

            viewHolder.frmList = (FrameLayout) convertView.findViewById(R.id.frmItemList);
            viewHolder.chkSelect = (CheckBox) convertView.findViewById(R.id.chkSelect);
            viewHolder.txtGuestName = (TextView) convertView.findViewById(R.id.txtGuestName);
            viewHolder.txtTelephoneNo = (TextView) convertView.findViewById(R.id.txtTelephoneNo);
            viewHolder.txtVZACode = (TextView) convertView.findViewById(R.id.txtVZACode);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //Init Data
        VZAListModel itemVZA = objects.get(position);

        //Set Visible
        if (!itemVZA.getHasCheck()) {
            viewHolder.chkSelect.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.chkSelect.setVisibility(View.VISIBLE);
        }

        //Set Height
        ViewList viewList = (ViewList) parent;
        if (viewList.getRowCount() > 1) {

            viewHolder.frmList.getLayoutParams().height = (int) (parent.getHeight() / viewList.getRowCount());
        }

        viewHolder.txtGuestName.setText(itemVZA.getGuestName());
        viewHolder.txtTelephoneNo.setText(itemVZA.getTelephoneNo());
        viewHolder.txtVZACode.setText(itemVZA.getVZACode());

        viewHolder.chkSelect.setChecked(itemVZA.getIsCheck());
        viewHolder.chkSelect.setTag(itemVZA);
        viewHolder.chkSelect.setClickable(false);
        viewHolder.frmList.setOnClickListener(chkSelect_OnClick);

        return convertView;
    }

    public List<VZAListModel> getItems() {
        return new ArrayList<>(objects);
    }

    public View.OnClickListener chkSelect_OnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            VZAListModel vzaListModel = (VZAListModel) view.findViewById(R.id.chkSelect).getTag();

            //Check Model
            List<VZAListModel> keepModel = new ArrayList<>();
            for (VZAListModel model : objects) {
                if (model.getIsCheck() == true) {
                    keepModel.add(model);
                }
            }

            vzaListModel.setIsCheck(!vzaListModel.getIsCheck());

            notifyDataSetChanged();
            //VZAListAdapter.this.selectedChangeListener.OnSelectedChange();
        }
    };
}



