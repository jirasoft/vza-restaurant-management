package control.ViewList;

public class VZAListModel<T> {

    private String table_no = "";
    private String guest_name = "";
    private String telephone_no = "";
    private String vza_code = "";

    private Boolean isCheck = false;
    private Boolean hasCheck = false;
    private Long lastCheck = System.nanoTime();
    private T entity;

    public VZAListModel setItem(String  tableNo,String guestName, String telephoneNo,
                                String vzaCode, Boolean hasCheck, Boolean isCheck,
                                T entity){
        this.table_no = tableNo;
        this.guest_name = guestName;
        this.telephone_no = telephoneNo;
        this.vza_code = vzaCode;
        this.hasCheck = hasCheck;
        this.isCheck = isCheck;
        this.entity = entity;
        return this;
    }

    public String getTableNo(){ return this.table_no; }
    public String getGuestName(){ return this.guest_name; }
    public String getTelephoneNo(){ return this.telephone_no; }
    public String getVZACode(){ return this.vza_code; }
    public Boolean getIsCheck(){
        return this.isCheck;
    };
    public Boolean getHasCheck(){
        return this.hasCheck;
    };
    public T getEntity(){ return this.entity; };
    public void setIsCheck(Boolean isCheck){this.isCheck = isCheck;
                                            this.lastCheck = System.nanoTime();};
    public Long lastCheck(){return this.lastCheck;};
}
