package control.ViewList;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.ListView;

import com.smartfinder.vzaapp.R;

import java.util.Timer;
import java.util.TimerTask;

import util.ViewUtil;

public class ViewList extends ListView {
    private float rowCount;
    private Context context;
    public ViewList(Context context) {
        super(context);
        this.context = context;
    }
    public ViewList(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ViewList);
        rowCount = typedArray.getFloat(R.styleable.ViewList_rowCount,-1);
        typedArray.recycle();
    }
    public ViewList(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ViewList);
        rowCount = typedArray.getFloat(R.styleable.ViewList_rowCount,-1);
        typedArray.recycle();
    }
    public float getRowCount(){
        return rowCount;
    }

    @Override
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.setEnabled(false);
        super.setOnItemClickListener(listener);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                ((Activity)context).runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                //Enable View
                                ViewList.this.setEnabled(true);
                            }
                        }
                );
            }
        }, 500);

    }
}
