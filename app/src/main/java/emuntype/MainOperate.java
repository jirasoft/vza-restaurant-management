package emuntype;

public enum MainOperate {
    //Main
    VerifyUserJSON,
    UserInfoJSON,
    HotelInformationbyCodeJSON,
    SaveRegisterJSON,
    UpdateRegisterJSON,
    UserInfoforgotpasswordJSON,
    HotelHmsOccListListJSON
}
