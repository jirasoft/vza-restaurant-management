package emuntype;

public enum MenuLookType {
    NORMAL("Normal"),MENU_ITEM("Menu Item"),SUBGROUP("SubGroup"), OPENFOOD("Open Food");
    private final String value;
    private MenuLookType(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
