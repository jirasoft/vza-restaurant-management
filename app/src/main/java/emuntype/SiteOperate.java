package emuntype;

public enum SiteOperate {
    //Site
    VZAgetMasterDataJSON,
    VZAGetShowVZACODEbyTelephonJSON,
    VZACheckLocationOpenedJSON,
    VZAgetMasterItemDataJSON,
    VZAGetCheckOrderSTOPSOLDJSON,
    POSSaveTrnOrderingJSON,
    VZASaveRequestJSON,
    POSgetTrnOrderingbyVZACODEJSON,
    VZAUpdateOrderFollowUpJSON,
    HMSgetFolioDetailJSON,
    VZAGetFavorite_Like_UnLikeDataJSON,
    VZAUpdate_FAVORITE_LIKE_UNLIKEJSON,
    VZACheckTableOpenedJSON,
    POSgetTableListForStaffJSON,
    VZAUpdateCoverTablelistJSON,
    VZAUpdateOrderStatusJSON,
    VZACheckCanOrderJSON,
    VZAgetStatusMasterDataJSON,
    VZACheckLoginJSON,
    VZAOpenTableAndOpenVZAJSON,
    VZAOpenJSON,
    VZACloseJSON,
    NTFGetTransactionNotificationJSON
}
