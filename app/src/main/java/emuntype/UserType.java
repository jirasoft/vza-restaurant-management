package emuntype;

public enum UserType {
    None(0),Staff(1);
    private final int value;
    private UserType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
