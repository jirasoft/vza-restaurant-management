package entity;

import java.io.Serializable;

public class BaseEntity implements Serializable {
    public Boolean STATUS_MYBILL;
    public Boolean STATUS_REQUEST;
    public Boolean STATUS_FOLIO;
    public Boolean FAVORITE;
    public Boolean LIKE_AND_UNLIKE;
    public String GETDATETIME = new String();
    public String FINISHDATETIME = new String();
    public String ERRORMSG = new String();
    public String smartscan = new String();
}
