package entity.Host.Cart;

import java.util.ArrayList;
import java.util.List;

public class MenuOrderData {
    public Integer MENU_ID = 0;
    public String MENU_CODE = "";
    public String MENU_NAME = "";
    public String MENU_SIZE = "";
    public Double ORDER_QTY = 0.00;
    public Double MENU_UNITPRICE = 0.00;
    public String CONDIMENT = "";
    public List<MenuOrderDataAddition> ADDITIONLIST = new ArrayList<>();
    public MenuOrderData(Integer MENU_ID,String MENU_CODE, String MENU_NAME,String MENU_SIZE, Double ORDER_QTY, Double MENU_UNITPRICE, String CONDIMENT, List<MenuOrderDataAddition> ADDITIONLIST){
        this.MENU_ID = MENU_ID;
        this.MENU_CODE = MENU_CODE;
        this.MENU_NAME = MENU_NAME;
        this.ORDER_QTY = ORDER_QTY;
        this.MENU_SIZE = MENU_SIZE;
        this.MENU_UNITPRICE = MENU_UNITPRICE;
        this.CONDIMENT = CONDIMENT;
        this.ADDITIONLIST = ADDITIONLIST;
    }
}