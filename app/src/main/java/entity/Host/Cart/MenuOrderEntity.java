package entity.Host.Cart;

import java.util.ArrayList;
import java.util.List;

public class MenuOrderEntity {
    public String ORDER_NAME = "";
    public Double ORDER_PRICE = 0.0;
    public Integer ORDER_QRY = 0;
    public Boolean isMenuSet = false;
    public List<MenuOrderData> ORDER = new ArrayList<>();
    public MenuOrderEntity(String ORDER_NAME, Double ORDER_PRICE,MenuOrderData ORDER){
        this.ORDER_NAME = ORDER_NAME;
        this.ORDER_PRICE = ORDER_PRICE;
        List<MenuOrderData> lisMenuOrder = new ArrayList<>();
        lisMenuOrder.add(ORDER);
        this.ORDER = lisMenuOrder;
        this.ORDER_QRY = ORDER.ORDER_QTY.intValue();

        this.isMenuSet = false;
    }
    public MenuOrderEntity(String ORDER_NAME, Double ORDER_PRICE,List<MenuOrderData> ORDER,Boolean isMenuSet){
        this.ORDER_NAME = ORDER_NAME;
        this.ORDER_PRICE = ORDER_PRICE;
        this.ORDER = ORDER;
        for(MenuOrderData orderData : ORDER){
            ORDER_QRY = orderData.ORDER_QTY.intValue();
        }
       this.isMenuSet = isMenuSet;
   }
    public Double getUnitPrice(){
        Double unitPrice = 0.00;
        if(isMenuSet){
            for(MenuOrderData orderData : ORDER){
                unitPrice = unitPrice + orderData.MENU_UNITPRICE;
            }
        }else{
            MenuOrderData orderData = ORDER.get(0);
            unitPrice = unitPrice + orderData.MENU_UNITPRICE;
            /*for(MenuOrderDataAddition addition : orderData.ADDITIONLIST){
                unitPrice = unitPrice + addition.ADDITIONAL_UNITPRICE;
            }*/
        }
        return unitPrice;
    }
    public Double getAdditionPrice(){
        Double addPrice = 0.00;
        MenuOrderData orderData = ORDER.get(0);
        for(MenuOrderDataAddition addition : orderData.ADDITIONLIST){
            addPrice = addPrice + addition.ADDITIONAL_UNITPRICE;
        }
        return addPrice;
    }
}
