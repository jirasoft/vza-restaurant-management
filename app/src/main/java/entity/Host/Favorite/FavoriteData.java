package entity.Host.Favorite;

public class FavoriteData {
   public Integer MENU_ID = 0;
   public String COMMENT = "";
   public String LIKE = "";
   public String FAVORITE = "";
   public String UNLIKE = "";
   public FavoriteData(Integer MENU_ID, String COMMENT, Boolean LIKE, Boolean UNLIKE, Boolean FAVORITE){
       this.MENU_ID = MENU_ID;
       this.COMMENT = COMMENT;
       this.LIKE = boolConv(LIKE);
       this.UNLIKE = boolConv(UNLIKE);
       this.FAVORITE = boolConv(FAVORITE);
   }
   private String boolConv(Boolean value){
       return value ? "Y" : "N";
   }
}
