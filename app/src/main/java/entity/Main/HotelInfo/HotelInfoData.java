package entity.Main.HotelInfo;

import java.io.Serializable;

public class HotelInfoData implements Serializable {
            public Integer HOTEL_ID = 0;
            public String HOTEL_CODE = new String();
            public String HOTEL_NAME1 = new String();
            public String HOTEL_NAME2 = new String();
            public String HOTEL_ADDRESS1 = new String();
            public String HOTEL_ADDRESS2 = new String();
            public String HOTEL_PROVINCE = new String();
            public String HOTEL_CITY = new String();
            public String HOTEL_TEL = new String();
            public String HOTEL_FAX = new String();
            public String HOTEL_IMAGE = new String();
            public String HOTEL_URL = new String();
            public String HOTEL_KEYCODE = new String();
            public String HOTEL_LATITUDE = new String();
            public String HOTEL_LONGITUDE = new String();
            public String COUNTRY_CODE = new String();
            public String LOCAL_CODE = new String();
            public Integer HOTEL_TYPE = 0;
            public String HOTEL_LOGO = new String();
            public Boolean HOTEL_USEVZA = false;
            public String HOTEL_DESC = new String();
            public String HOTEL_DISTICT = new String();
            public String HOTEL_SUBDISTICT  = new String();
            public Integer COUNTRY_ID = 0;
            public Integer HOTEL_SECTOR_ID = 0;
            public Integer PROVINCE_ID = 0;
            public String HOTEL_ZIPCODE = new String();
            public String HOTEL_WEBSITE = new String();
            public Integer HOTEL_STAR = 0;
            public Integer DbStatus = 0;
            public String ErrorMsg = new String();
}
