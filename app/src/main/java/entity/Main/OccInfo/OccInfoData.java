package entity.Main.OccInfo;

import java.io.Serializable;

public class OccInfoData implements Serializable {
    public Integer HOTEL_ID = 0;
    public String DD = "";
    public Integer TOTAL_ROOM_SOLD = 0;
    public Double OCC_PERCENT = 0.00;
    public Double ADR = 0.00;
    public Double REVENUE = 0.00;
    public Integer IS_TYPE = 0;
    public String  LAST_UPDATE_DATE = "";
    public Integer SEQ_NO = 0;
    public Integer DbStatus = 0;
    public String ErrorMsg = "";
}
