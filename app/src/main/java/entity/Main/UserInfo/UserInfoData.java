package entity.Main.UserInfo;


import java.io.Serializable;

public class UserInfoData implements Serializable {
   public Integer ID = 0;
   public String TITLE = new String();
   public String FIRSTNAME = new String();
   public String LASTNAME = new String();
   public String EMAIL = new String();
   public String RESIDENCE_COUNTRY = new String();
   public String TELEPHONE = new String();
   public String PASSCODE = new String();
   public String CONFIRM_PASSCODE = new String();
   public boolean ACTIVE = false;
   public Integer GUEST_TYPE = 0;
   public Integer HOTEL_ID = 0;
   public String STAFF_CODE = new String();
   public boolean GET_HMSOCC = false;
   public Integer LEVEL_ID = 0;
   public String COMPANY_NAME  = new String();
   public String ADDRESS1 = new String();
   public String ADDRESS2 = new String();
   public String NATIONALITY = new String();
   public String CITY = new String();
   public Integer COUNTRY_ID = 0;
   public String ZIPCODE = new String();
   public String DATEOFBIRTH = new String();
   public Integer DbStatus = 0;
   public String ErrorMsg = new String();
}
