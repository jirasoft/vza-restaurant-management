package entity.Service.BillOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BillOrderData implements Serializable {
    public Integer ORDER_ID = 0;
    public Integer LOCATION_ID = 0;
    public String LOCATION_NAME1 = "";
    public  String LOCATION_NAME2 = "";
    public  String TABLE_NO = "";
    public  String ORDER_DATETIME = "";
    public  String MENU_CODE = "";
    public  String MENU_NAME = "";
    public Double ORDER_QTY = 0.00; // public Integer ORDER_QTY = 0;
    public Double MENU_UNITPRICE = 0.00;
    public String CONDIMENT = "";
    public Integer ORDER_STATUS = 0;
    public String ORDER_STATUS_NAME = "";
    public Integer VZAID = 0;
    public String VZACODE = "";
    public String ORDER_NO = "";
    public String STAFF_CODE = "";
    public String STAFF_NAME1 = "";
    public String STAFF_NAME2 = "";
    public String REF_NO = "";
    public Boolean DINEIN = false;
    public Boolean VOID_STATUS = false;
    public String VOID_STAFFBY = "";
    public String VOID_DATE = "";
    public Integer AMOUNT = 0;
    public String MENU_SIZE = "";
    public Boolean FOLLOWUP = false;
    public Integer STATUS = 0;
    public Integer SYSTEMID = 0;
    public String BILL_NO = "";
    public String REMARK = "";
    public Integer STATUS_LIKE_UNLIKE = 0;
    public String LIKE_UNLIKE_DEP = "";
    public List<BillOrderDataAddition> ADDITIONALDETAIL = new ArrayList<>();
    public Integer DbStatus = 0;
    public Boolean RDSELECT = false;
    public String ErrorMsg = "";
    public Integer COMPANY_ID = 0;
    public String DisplayText = "";
}
