package entity.Service.BillOrder;

import java.io.Serializable;

public class BillOrderDataAddition implements Serializable {
    public Integer ORDER_D_ID = 0;
    public Integer ADDITIONAL_ID = 0;
    public String ADDITIONAL_CODE = "";
    public String ADDITIONAL_NAME1 = "";
    public String ADDITIONAL_NAME2 = "";
    public Double AMOUNT = 0.00;
    public Double ORDER_ADDITIONAL_QTY = 0.00;
    public Double ADDITIONAL_UNITPRICE = 0.00;
    public Integer DbStatus = 0;
    public Boolean RDSELECT = false;
    public String ErrorMsg = "";
    public Integer COMPANY_ID = 0;
    public String DisplayText = "";
}
