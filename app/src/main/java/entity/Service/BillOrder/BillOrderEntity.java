package entity.Service.BillOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import entity.BaseEntity;

public class BillOrderEntity extends BaseEntity implements Serializable {
    public List<BillOrderData> ITEMS = new ArrayList<>();
    public Double NET = 0.00;
    public Double DISCOUNT = 0.00;
    public Double SERVICE = 0.00;
    public Double VAT = 0.00;
    public Double TOTAL = 0.00;
    public String MESSAGE_ONFOOTERBILL = "";
    public Boolean CONTROL_FOLLOWUP = false;
    public Boolean CHECKDATA = false;
    public Integer SERVICE_ID = 0;
    public Integer SYSTEMID = 0;
}
