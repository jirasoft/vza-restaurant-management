package entity.Service.CanOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import entity.BaseEntity;
public class CanOrderEntity extends BaseEntity implements Serializable {
     public Boolean BILL_DISCOUNT = false;
     public Boolean ISTABLE_TF = false;
     public String TABLE_NO_TF = "";
     public Boolean LOCATION_OPEN = false;
     public Boolean TABLE_OPEN = false;
     public List<CanOrderData> ITEM_SOLDOUTList = new ArrayList<>();
}
