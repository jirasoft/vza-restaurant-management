package entity.Service.CheckLocation;

import java.io.Serializable;
import entity.BaseEntity;

public class CheckLocationEntity  extends BaseEntity implements Serializable {
    public Boolean RESULT = false;
    public Integer VZAID = 0;
    public String VZACODE = new String();
    public Integer LOCATION_ID= 0;
    public String ROOM_ID = new String();
    public String TABLE_NO = new String();
    public Integer VZA_TYPE = 0;
}
