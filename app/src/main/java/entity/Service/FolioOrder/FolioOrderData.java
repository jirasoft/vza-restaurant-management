package entity.Service.FolioOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FolioOrderData implements Serializable {
        public String FOLIO_DATE = "";
        public String FOLIO_CODE = "";
        public String FOLIO_NAME = "";
        public Double AMOUNT = 0.00;
        public Double VATAMT = 0.00;
        public Double SERVAMT = 0.00;
        public Double TAXAMT = 0.00;
        public Double NETAMT = 0.00;
        public List<FolioOrderDataDetail> Detail = new ArrayList<>();
    }
