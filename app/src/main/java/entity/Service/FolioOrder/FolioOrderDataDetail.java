package entity.Service.FolioOrder;

import java.io.Serializable;

public class FolioOrderDataDetail implements Serializable {
           public String POST_DATE;
           public String BILL_ITEM;
           public String TRANS_CODE;
           public String TRANS_NAME;
           public Boolean ITEM_PAYMENT = false;
           public Double AMOUNT = 0.00;
           public Double VATAMT = 0.00;
           public Double SERVAMT = 0.00;
           public Double TAXAMT = 0.00;
           public Double NETAMT = 0.00;
}
