package entity.Service.FolioOrder;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import entity.BaseEntity;

public class FolioOrderEntity extends BaseEntity implements Serializable {
    public List<FolioOrderData> Folio = new ArrayList<>();
    public Integer SERVICE_ID = 0;
    public Integer SYSTEMID = 0;
}
