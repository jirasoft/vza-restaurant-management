package entity.Service.Master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MasterData implements Serializable {
    public List<MasterPrivary> PrivacyPolicy = new ArrayList<>();
    public MasterDetail MenuDetail = new MasterDetail();
    public ServiceDetail ServiceDetail = new ServiceDetail();
    public Integer SERVICE_ID = 0;
    public Integer LOCATION_ID = 0;
    public String LOCATION_CODE = new String();
    public String LOCATION_NAME1 = new String();
    public String LOCATION_NAME2 = new String();
    public Integer STATUS = 0;
    public String STATUS_NAME = new String();
    public String LOCATON_MESSAGEONBILL = new String();
    public Integer SYSTEMID = 0;
    public String SYSTEM_NAME = new String();
    public String IMAGENAME = new String();
    public String LOCATION_DESP = new String();
    public Boolean CAN_ORDER = false;
    public Boolean CONTROL_FOLLOWUP = false;
    public String CURRENCY_DEP = new String();
    public Integer DbStatus = 0;
    public Boolean RDSELECT = false;
    public String ErrorMsg = new String();
    public Integer COMPANY_ID = 0;
    public String DisplayText = new String();

}
