package entity.Service.Master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MasterDetail implements Serializable {
    public String GroupAllImagUrl = new String();
    public List<MasterDetailList> MenugroupList = new ArrayList<>();
    public MasterDetailList FindByID(Integer id,String lookFor){
        try {
            if(MenugroupList == null){
                return MasterDetailList.class.newInstance();
            }else if(MenugroupList.size() == 0) {
                return MasterDetailList.class.newInstance();
            }else{
                for(MasterDetailList menuData : MenugroupList){
                    if(menuData.MENU_GROUP_ID.equals(id) && menuData.GROUP_LOOKFOR.equals(lookFor)){
                        return menuData;
                    }
                }
                return MasterDetailList.class.newInstance();
            }
        } catch (InstantiationException e) {
            throw new NullPointerException();
        } catch (IllegalAccessException e) {
            throw new NullPointerException();
        }
    }
}
