package entity.Service.Master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MasterDetailList implements Serializable {
    public Integer LOCATION_ID = 0;
    public Integer MENU_GROUP_ID = 0;
    public String MENU_GROUP_CODE = new String();
    public String MENU_GROUP_NAME1 = new String();
    public String MENU_GROUP_NAME2 = new String();
    public Boolean ISSPECIAL = false;
    public Integer MENU_GROUP_SEQ = 0;
    public String MENU_GROUP_IMAGENAME = new String();
    public Boolean SCRGROUP_PK= false;
    public String GROUP_LOOKFOR = new String();
    public List<MasterDetailSubList> MENUSUBGROEP_DETAIL = new ArrayList<>();
    public MasterDetailSubList FindByID(Integer id){
        try {
            if(MENUSUBGROEP_DETAIL == null){
                return MasterDetailSubList.class.newInstance();
            }else if(MENUSUBGROEP_DETAIL.size() == 0) {
                return MasterDetailSubList.class.newInstance();
            }else{
                for(MasterDetailSubList menuData : MENUSUBGROEP_DETAIL){
                    if(menuData.MENU_SUBMENU_ID.equals(id)){
                        return menuData;
                    }
                }
                return MasterDetailSubList.class.newInstance();
            }
        } catch (InstantiationException e) {
            throw new NullPointerException();
        } catch (IllegalAccessException e) {
            throw new NullPointerException();
        }
    }
    public Integer DbStatus = 0;
    public Boolean RDSELECT = false;
    public String ErrorMsg  = new String();
    public Integer COMPANY_ID = 0;
    public String DisplayText = new String();
}
