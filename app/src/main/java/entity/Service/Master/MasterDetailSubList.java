package entity.Service.Master;

import java.io.Serializable;

public class MasterDetailSubList implements Serializable {
    public Integer LOCATION_ID = 0;
    public Integer MENU_GROUP_ID = 0;
    public Integer MENU_SUBMENU_ID = 0;
    public String MENU_SUBGROUP_NAME1 = new String();
    public String MENU_SUBGROUP_NAME2 = new String();
    public String MENU_SUBGROUP_CODE = new String();
    public String MENU_GROUP_CODE = new String();
    public String MENU_SUBGROUP_IMAGENAME = new String();
    public Integer PROMOTIONSET_LOOKFOR_ID = 0;
    //CONDITMENTGROUPDETAIL Miss
    public Integer DbStatus = 0;
    public Boolean RDSELECT = false;
    public String ErrorMsg = new String();
    public Integer COMPANY_ID = 0;
    public String DisplayText = new String();
}
