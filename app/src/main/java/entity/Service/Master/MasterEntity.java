package entity.Service.Master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import entity.BaseEntity;

public class MasterEntity extends BaseEntity implements Serializable {
    public List<MasterData> LocationList = new ArrayList<>();
    public MasterData FindByLocationID(int LocationID) {
        try {
            if (LocationList == null) {
                return MasterData.class.newInstance();
            } else if (LocationList.size() == 0) {
                return MasterData.class.newInstance();
            } else {
                for (MasterData location : LocationList) {
                    if (location.LOCATION_ID == LocationID) {
                        return location;
                    }
                }
                return MasterData.class.newInstance();
            }
        } catch (InstantiationException e) {
            throw new NullPointerException();
        } catch (IllegalAccessException e) {
            throw new NullPointerException();
        }
    }
}
