package entity.Service.Master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ServiceDetailList implements Serializable {
     public Integer SERVICEGRP_ID = 0;
     public Integer SYSTEMID = 0;
     public String SYSTEM_NAME = new String();
     public String SERVICEGRP_NAME = new String();
     public Integer SERVICE_GROUP_SEQ = 0;
     public String SERVICE_GROUP_IMAGENAME = new String();
     public List<ServiceDetailSubList> SERVICE_DETAIL = new ArrayList<>();
}
