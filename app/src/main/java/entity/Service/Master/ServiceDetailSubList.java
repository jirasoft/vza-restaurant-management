package entity.Service.Master;

import java.io.Serializable;

public class ServiceDetailSubList implements Serializable {
    public Integer SERVICE_ID = 0;
    public Integer SYSTEMID = 0;
    public String SERVICE_NAME1 = new String();
    public String SERVICE_NAME2 = new String();
    public String SYSTEM_NAME = new String();
    public Integer DEPARTMENT_ID = 0;
    public String DEPARTMENT_NAME = new String();
    public Integer SERVICEGRP_ID = 0;
    public String IMAGENAME = new String();
    public Integer SYSTEM_SPECIAL_ID = 0;
    public String IMAGENAME2 = new String();
    public String IMAGENAME3 = new String();
    public Integer DbStatus = 0;
    public Boolean RDSELECT = false;
    public String ErrorMsg = new String();
    public Integer COMPANY_ID = 0;
    public String DisplayText = new String();
}
