package entity.Service.MasterItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MasterItemData implements Serializable,Cloneable {
            public Integer ID = 0;
            public Integer MENU_GROUP_ID = 0;
            public Integer MENU_MENU_ID = 0;
            public Integer MENU_SEQNO = 0;
            public String MENU_CODE = "";
            public String MENU_NAME1 = "";
            public String MENU_NAME2 = "";
            public Double MENU_UNITPRICE = 0.00;
            public String CURRENCY_DEP = "";
            public String MENU_SIZE = "";
            public String MENU_IMAGENAME = "";
            public Integer MENU_SUBGROUP_ID = 0;
            public Integer LOCATION_ID = 0;
            public String MENU_DESP = "";
            public Boolean MENU_PK = false;
            public String MENU_IMAGENAME2 = "";
            public String MENU_IMAGENAME3 = "";
            public String MENU_GROUP_CODE = "";
            public Boolean IS_FAVORITE = false;
            public Boolean IS_LIKE = false;
            public Boolean IS_UNLIKE = false;
            public String COMMENT_DEP = "";
            public Double CHARGE_UNITPIRCE = 0.00;
            public List<AdditionalDetail> ADDITIONAL_DETAIL = new ArrayList<>();
            public List<CondimentDetail> CONDIMENT_DETAIL = new ArrayList<>();
            public List<SelectDetail> SELECT_DETAIL = new ArrayList<>();
            public List<FixDetail> FIX_DETAIL = new ArrayList<>();
            public List<ChangeDetail> CHANGE_DETAIL = new ArrayList<>();
            public List<UpSizeDetail> UPSIZE_DETAIL = new ArrayList<>();
            public Integer DbStatus = 0;
            public Boolean RDSELECT = false;
            public String ErrorMsg = "";
            public Integer COMPANY_ID =  0;
            public String DisplayText = "";

            public class AdditionalDetail{
                public Integer MENU_ID = 0;
                public Integer ADDITIONAL_ID = 0;
                public Integer LOCATION_ID = 0;
                public String ADDITIONAL_CODE = "";
                public String ADDITIONAL_NAME1 = "";
                public String ADDITIONAL_NAME2 = "";
                public String ADDITIONAL_IMAGENAME = "";
                public Double ADDITIONAL_UNITPRICE = 0.00;
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
            }

            public class CondimentDetail{
                public Integer LOCATION_ID = 0;
                public Integer CONDIMENT_ID = 0;
                public String CONDIMENT_CODE_REF = "";
                public String CONDIMENT_NAME1 = "";
                public String CONDIMENT_NAME2 = "";
                public Boolean ACTIVE = false;
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
            }

            public class SelectDetail{
                public Integer ID = 0;
                public Integer LOCATION_ID = 0;
                public String PROMOTIONSET_CODE = "";
                public String GROUPSELECT_NAME = "";
                public Double ITEM_LIMITQTY = 0.00;
                public Double UPSIZE_CHARGE = 0.00;
                public Boolean CANUP_SIZE = false;
                public Integer ITEM_TYPE = 0;
                public List<MenuPromotionDetail> MENUPROMOTION_DETAIL = new ArrayList<>();
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
                public MenuPromotionDetail findMenuDetail(Integer MENU_MENU_ID){
                   for(MenuPromotionDetail promotionDetail : MENUPROMOTION_DETAIL){
                       if(promotionDetail.MENU_MENU_ID.equals(MENU_MENU_ID)){
                           return promotionDetail;
                       }
                   }
                    return null;
                }
            }

            public class FixDetail{
                public Integer ID = 0;
                public Integer LOCATION_ID = 0;
                public String PROMOTIONSET_CODE = "";
                public String GROUPSELECT_NAME = "";
                public Double ITEM_LIMITQTY = 0.00;
                public Double UPSIZE_CHARGE = 0.00;
                public Boolean CANUP_SIZE = false;
                public Integer ITEM_TYPE = 0;
                public List<MenuPromotionDetail> MENUPROMOTION_DETAIL = new ArrayList<>();
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
                public MenuPromotionDetail findMenuDetail(Integer MENU_MENU_ID){
                    for(MenuPromotionDetail promotionDetail : MENUPROMOTION_DETAIL){
                        if(promotionDetail.MENU_MENU_ID.equals(MENU_MENU_ID)){
                            return promotionDetail;
                        }
                    }
                    return null;
                }
            }

            public class ChangeDetail{
                public Integer ID = 0;
                public Integer LOCATION_ID = 0;
                public String PROMOTIONSET_CODE = "";
                public String GROUPSELECT_NAME = "";
                public Double ITEM_LIMITQTY = 0.00;
                public Double UPSIZE_CHARGE = 0.00;
                public Boolean CANUP_SIZE = false;
                public Integer ITEM_TYPE = 0;
                public List<MenuPromotionDetail> MENUPROMOTION_DETAIL = new ArrayList<>();
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
                public MenuPromotionDetail findMenuDetail(Integer MENU_MENU_ID){
                    for(MenuPromotionDetail promotionDetail : MENUPROMOTION_DETAIL){
                        if(promotionDetail.MENU_MENU_ID.equals(MENU_MENU_ID)){
                            return promotionDetail;
                        }
                    }
                    return null;
                }
            }

            public class UpSizeDetail{
                public Integer ID = 0;
                public Integer LOCATION_ID = 0;
                public String PROMOTIONSET_CODE = "";
                public Double ITEM_LIMITQTY = 0.00;
                public Double UPSIZE_CHARGE = 0.00;
                public boolean CANUP_SIZE = false;
                public Integer ITEM_TYPE = 0;
                public List<ItemUpSizeDetail> ITEMUPSIZE_DETAIL = new ArrayList<>();
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
                public ItemUpSizeDetail findUpSideDetail(Integer ITEM_UPSIZE_ID){
                    for(ItemUpSizeDetail promotionDetail : ITEMUPSIZE_DETAIL){
                        if(promotionDetail.ITEM_UPSIZE_ID.equals(ITEM_UPSIZE_ID)){
                            return promotionDetail;
                        }
                    }
                    return null;
                }
            }

            public class MenuPromotionDetail{
                public Integer ID = 0;
                public Integer PROMOTIONSET_ID = 0;
                public Integer MENU_MENU_ID = 0;
                public Integer MENU_SEQNO = 0;
                public String MENU_CODE = "";
                public String MENU_NAME1 = "";
                public String MENU_NAME2 = "";
                public Double MENU_UNITPRICE = 0.00;
                public String CURRENCY_DEP = "";
                public String MENU_SIZE = "";
                public Integer MENU_SUBGROUP_ID = 0;
                public Integer LOCATION_ID = 0;
                public String MENU_DESP = "";
                public String MENU_GROUP_CODE = "";
                public Double CHARGE_UNITPIRCE = 0.00;
                public Integer ITEM_UPSIZE_ID = 0;
                public String ITEM_UPSIZE_CODE = "";
                public String ITEM_UPSIZE_NAME1 = "";
                public String ITEM_UPSIZE_NAME2 = "";
                public Integer ITEM_TYPE = 0;
                public String GROUPSELECT_NAME = "";
                public boolean CANUP_SIZE = false;
                public Integer ITEM_LIMITQTY = 0;
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
            }

            public class ItemUpSizeDetail{
                public Integer PROMOTIONSET_ID = 0;
                public Integer ITEM_UPSIZE_ID = 0;
                public String ITEM_UPSIZE_CODE = "";
                public String ITEM_UPSIZE_NAME1 = "";
                public String ITEM_UPSIZE_NAME2 = "";
                public Integer DbStatus = 0;
                public Boolean RDSELECT = false;
                public String ErrorMsg = "";
                public Integer COMPANY_ID = 0;
                public String DisplayText = "";
            }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }
}
