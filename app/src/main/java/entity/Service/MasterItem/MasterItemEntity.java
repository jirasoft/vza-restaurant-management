package entity.Service.MasterItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import entity.BaseEntity;
import entity.Service.Master.MasterDetailList;

public class MasterItemEntity extends BaseEntity implements Serializable {
    public List<MasterItemData> MenuList = new ArrayList<>();
    public MasterItemData FindByID(Integer id){
        try {
            if(MenuList == null){
                return MasterItemData.class.newInstance();
            }else if(MenuList.size() == 0) {
                return MasterItemData.class.newInstance();
            }else{
                for(MasterItemData menuData : MenuList){
                    if(menuData.ID.equals(id)){
                        return menuData;
                    }
                }
                return MasterItemData.class.newInstance();
            }
        } catch (InstantiationException e) {
            throw new NullPointerException();
        } catch (IllegalAccessException e) {
            throw new NullPointerException();
        }
    }
}
