package entity.Service.Notification;

import java.io.Serializable;

public class NotificationData implements Serializable {
    public Integer ID = 0;
    public String TEXT_NOTIFICATION = "";
    public String SYSTEM_DATE = "";
    public String SENDER_CODE = "";
    public Boolean STATUS_READ = false;
}
