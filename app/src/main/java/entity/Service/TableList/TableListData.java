package entity.Service.TableList;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class TableListData implements Serializable {
    public Integer VZAID = 0;
    public Integer SYSTEMID = 0;
    public Integer LOCATION_ID = 0;
    public String BILL_NO = "";
    public String TABLE_NO = "";
    public Double TOTAL = 0.00;
    public Integer COVER = 0;
    public List<TableListDataOrderList> ORDER_DETAIL = new ArrayList<>();
    public Double getAmount() {
        Double TotalAmount = 0.00;
        for (TableListDataOrderList listDataOrderList : ORDER_DETAIL) {
            Double ItemAmount = 0.00;
            ItemAmount += (listDataOrderList.MENU_UNITPRICE * listDataOrderList.ORDER_QTY);
            for (TableListDataOrderListAddition addition : listDataOrderList.ADDITIONALDETAIL) {
                ItemAmount += (addition.ADDITIONAL_UNITPRICE * addition.ADDITIONAL_UNITPRICE);
            }
            TotalAmount += ItemAmount;
        }
        return TotalAmount;
    }
    public List<TableListDataVZAList> VZA_LIST = new ArrayList<>();

}
