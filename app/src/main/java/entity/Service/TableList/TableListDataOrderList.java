package entity.Service.TableList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nutchaitat_t on 18/1/2558.
 */
public class TableListDataOrderList implements Serializable {
    public Integer ORDER_ID = 0;
    public Double ORDER_QTY = 0.00;
    public Double MENU_UNITPRICE = 0.00;
    public List<TableListDataOrderListAddition> ADDITIONALDETAIL = new ArrayList<>();
}
