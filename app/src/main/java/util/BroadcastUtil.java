package util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import control.ShortcutBadger.ShortcutBadgeException;
import control.ShortcutBadger.ShortcutBadger;
import entity.Main.HotelInfo.HotelInfoEntity;
import entity.Service.CheckLocation.CheckLocationEntity;
import entity.Service.Notification.NotificationData;
import entity.Service.Notification.NotificationEntity;
import entity.Service.VZACode.VZACodeEntity;

public class BroadcastUtil {

    private Context context;
    private List<NameValuePair> param = new ArrayList<>();
    public static List<entity.Host.Notification.NotificationData> NotificationData = new ArrayList<>();
    private Integer readRecord = 0;
    private Integer lastRecord = 0;
    private String site = "";

    private OnCast castListener;
    public interface OnCast{
        public void OnCast(Integer totalRecord);
    }
    public void setCastListener(OnCast event){
        this.castListener = event;
    }

    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private AsyncTask processTask;

    public BroadcastUtil(Context context,List<NameValuePair> param,String site){
        NotificationData = new ArrayList<>();
        this.context = context;
        this.param = param;
        this.site = site;
    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!((Activity) context).isFinishing()) {
                //Cell HTTP Post
                HttpParams httpParams = new BasicHttpParams();
                HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
                HttpProtocolParams.setContentCharset(httpParams, HTTP.DEFAULT_CONTENT_CHARSET);
                HttpConnectionParams.setConnectionTimeout(httpParams, context.getResources().getInteger(R.integer.CONN_TIMEOUT));
                HttpConnectionParams.setSoTimeout(httpParams, context.getResources().getInteger(R.integer.CONN_TIMEOUT));

                //Edit Param
                List<NameValuePair> param = new ArrayList<>(BroadcastUtil.this.param);
                param.add(new BasicNameValuePair("LASTRECORDIDPara", lastRecord.toString()));

                //Edit Param after change table
                String VZACode = BaseApplication.GlobalVariable.get(context, CheckLocationEntity.class).VZACODE;
                if(!param.get(3).getValue().equals(VZACode)) {
                    param.set(3,new BasicNameValuePair("SENDORRECEIVECODEPara", VZACode));
                }

                final HttpClient httpClient = new DefaultHttpClient(httpParams);
                final HttpPost httpPost = new HttpPost(site + "/NTFGetTransactionNotificationJSON");

                //Check Internet Connection
                if (((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                    try {
                        //Add Read Record
                        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                        httpPost.setEntity(new UrlEncodedFormEntity(param, "UTF-8"));
                        processTask = new AsyncTask<Void, Void, NotificationEntity>() {
                            @Override
                            protected NotificationEntity doInBackground(Void... params) {
                                try {
                                    HttpResponse httpResponse = httpClient.execute(httpPost);
                                    if (httpResponse != null) {
                                        if (httpResponse.getStatusLine().getStatusCode() == 200) {
                                            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
                                            return new Gson().fromJson(reader, NotificationEntity.class);
                                        } else {
                                            //throw new IOException();
                                            return null;
                                        }
                                    } else {
                                        //throw new IOException();
                                        return null;
                                    }
                                } catch (SocketTimeoutException e) {
                                    e.printStackTrace();
                                    return null;
                                } catch (ConnectTimeoutException e) {
                                    e.printStackTrace();
                                    return null;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            }

                            @Override
                            protected void onPostExecute(NotificationEntity results) {
                                NotificationData = new ArrayList<>(); //-- New
                                if (results != null) {
                                    for (NotificationData data : results.ITEMS) {
                                        Boolean hasKeep = false;
                                        for (entity.Host.Notification.NotificationData lisEntity : NotificationData) {
                                            if (lisEntity.DATE_RECORD.contains(data.SYSTEM_DATE.substring(0, 10))) {
                                                lisEntity.DATA.add(data);
                                                hasKeep = true;
                                                break;
                                            }
                                        }
                                        if (!hasKeep) {
                                            entity.Host.Notification.NotificationData newData = new entity.Host.Notification.NotificationData();
                                            newData.DATE_RECORD = data.SYSTEM_DATE.substring(0, 10);
                                            newData.DATA.add(data);
                                            NotificationData.add(newData);
                                        }
                                        //Keep Last Record
                                        //lastRecord = data.ID;
                                    }
                                }

                                //Count Record
                                Integer countRecord = 0;
                                for (entity.Host.Notification.NotificationData lisRecord : NotificationData) {
                                /*for(entity.Service.Notification.NotificationData record : lisRecord.DATA){
                                    if(readRecord < record.ID){
                                        countRecord++;
                                    }
                                }*/
                                    countRecord = countRecord + lisRecord.DATA.size();
                                }

                                //Init Cast Event
                                int countRecords = 0;
                                if (castListener != null) {
                                    if (countRecord > 0) {
                                        int totalRecords = BaseApplication.NotificationRecordVariable.getTotalRecord(context);
                                        int lastRecords = BaseApplication.NotificationRecordVariable.getLastRecord(context);

                                        Log.wtf("Notification_" + BaseApplication.NotificationRecordVariable._key,
                                                countRecord + " - " + totalRecords + " - " + lastRecords);

                                        if (lastRecords > 0) {
                                            //-- Check
                                            if (totalRecords > countRecord) {
                                                if ((countRecord == lastRecords)
                                                        && ((totalRecords > countRecord)
                                                        || (totalRecords == 0)))
                                                    totalRecords = countRecord;

                                                if ((totalRecords == lastRecords)
                                                        && (totalRecords != countRecord)
                                                        && (countRecord > 0))
                                                    totalRecords = totalRecords + countRecord;

                                                BaseApplication.NotificationRecordVariable.setVariable(context,
                                                        totalRecords,
                                                        lastRecords);
                                                countRecords = totalRecords;
                                            } else {
                                                BaseApplication.NotificationRecordVariable.setVariable(context,
                                                        countRecord,
                                                        lastRecords);
                                                countRecords = countRecord;
                                            }
                                            countRecords = countRecords - lastRecords;
                                        } else {
                                            BaseApplication.NotificationRecordVariable.setVariable(context,
                                                    countRecord,
                                                    0);
                                            countRecords = countRecord;
                                        }
                                    }
                                    castListener.OnCast(countRecords);
                                }
                                //Init Cast To Badge
                                try {
                                    ShortcutBadger.setBadge(context.getApplicationContext(), countRecords);
                                } catch (ShortcutBadgeException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.execute();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
    };
    public void Start() {
        scheduler.scheduleWithFixedDelay(runnable, context.getResources().getInteger(R.integer.NOTIFICATION_TIME_SLEEP), context.getResources().getInteger(R.integer.NOTIFICATION_TIME_SLEEP), TimeUnit.SECONDS);
    }
    public void ReadRecord(){
        readRecord = lastRecord;
        Execute();
    }
    public void ChangeTable(List<NameValuePair> param,String site){
        this.param = param;
        this.site = site;
    }
    public void Execute() {
        runnable.run();
    }
    public void Shutdown(){
        //Shutdown Process
        if(processTask != null){
            processTask.cancel(true);
        }
        scheduler.shutdown();

        //Clear Notification
        try {
            ShortcutBadger.setBadge(context.getApplicationContext(),0);
        } catch (ShortcutBadgeException e) {
            e.printStackTrace();
        }
    };
}
