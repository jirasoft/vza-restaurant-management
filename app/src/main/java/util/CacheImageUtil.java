package util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.smartfinder.vzaapp.R;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;

public class CacheImageUtil {
    private static HashMap<String,Bitmap> CacheImage = new HashMap<>();
    public static void addImage(Context context,String path,Bitmap source){
        SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.SHAREDPREF_IMGCACHE),Context.MODE_PRIVATE);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        source.compress(Bitmap.CompressFormat.PNG,40,stream); // Default format: webp, quality: 70
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(path, Base64.encodeToString(stream.toByteArray(),Base64.DEFAULT));
        if(!preferences.contains(context.getString(R.string.SHAREDPREF_IMGCACHE_TIMEOUT))){
            editor.putInt(context.getString(R.string.SHAREDPREF_IMGCACHE_TIMEOUT), Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
        }
        editor.commit();
    }
    public static boolean hasImage(Context context,String path){
        SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.SHAREDPREF_IMGCACHE),Context.MODE_PRIVATE);
        if(preferences.contains(context.getString(R.string.SHAREDPREF_IMGCACHE_TIMEOUT))){
           Integer cache_timeout  = preferences.getInt(context.getString(R.string.SHAREDPREF_IMGCACHE_TIMEOUT),0);
           if(cache_timeout != Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
             SharedPreferences.Editor edit = preferences.edit();
               edit.clear().commit();
           }
        }
        return preferences.contains(path);
    }
    public static Bitmap getImage(Context context,String path){
        if(CacheImage.containsKey(path)){
            return CacheImage.get(path);
        }

        Log.wtf("Img Path", path);

        Bitmap bitmap = null;

        SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.SHAREDPREF_IMGCACHE),Context.MODE_PRIVATE);
        String sourceBase = preferences.getString(path,null);
        if(sourceBase != null){
            try {
                byte[] source = Base64.decode(sourceBase, Base64.DEFAULT);

                //-- Start : Issue Out Of Memory
                Bitmap sourceImage = BitmapFactory.decodeByteArray(source, 0, source.length);
                final int maxSize = context.getResources().getInteger(R.integer.IMAGE_WIDTH);
                int outWidth;
                int outHeight;
                int inWidth = sourceImage.getWidth();
                int inHeight = sourceImage.getHeight();
                if(inWidth > inHeight){
                    outWidth = maxSize;
                    outHeight = (inHeight * maxSize) / inWidth;
                } else {
                    outHeight = maxSize;
                    outWidth = (inWidth * maxSize) / inHeight;
                }

                if(sourceImage.getWidth()> maxSize) {
                    bitmap = Bitmap.createScaledBitmap(sourceImage, outWidth, outHeight, true);
                }
                else
                {
                    bitmap = sourceImage;
                }
                //-- Cache Image Put
                CacheImage.put(path, bitmap);
                //-- End : Issue Out Of Memory

                //CacheImage.put(path, BitmapFactory.decodeByteArray(source, 0, source.length));
                if (!(CacheImage.size() < context.getResources().getInteger(R.integer.IMG_CACHE))) {
                    CacheImage.remove(CacheImage.keySet().toArray()[0].toString());
                }
                //return BitmapFactory.decodeByteArray(source, 0, source.length);
            }catch (OutOfMemoryError ex){
                ex.printStackTrace();
                //return null;

                bitmap = CacheImage.get(path);
            }
            catch (Exception ex){
                ex.printStackTrace();
                //return null;

                bitmap = CacheImage.get(path);
            }
        }else{
            //return null;
            //-- Fix issue no image and path not true
            bitmap = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.no_image);
        }

        return bitmap;
    }
}
