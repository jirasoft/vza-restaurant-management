package util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import control.Dialog.ChooseDialog;
import control.Dialog.DeferDialog;

import com.google.gson.Gson;
import com.smartfinder.vzaapp.BaseApplication;
import com.smartfinder.vzaapp.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import emuntype.MainOperate;
import entity.BaseEntity;
import entity.Service.Master.MasterEntity;

public class ServiceUtil extends AsyncTask<List<NameValuePair>, Void, ServiceUtil.ServiceResponse> {

    private Context context;
    private View frmMain;
    private Enum operate;
    private Class entityClass;
    private OnFinished finishedListener;
    private OnFailed failedListener;
    private DeferDialog deferDialog;
    private String site;
    private String keycode;
    final private int srcOrientation;
    private List<NameValuePair> param = new ArrayList<>();

    public interface OnFinished {
        public void OnFinished(Object results);
    }

    public interface OnFailed {
        public void OnFailed();
    };

    public class ServiceResponse {
        public HttpResponse httpResponse;
        public Object OBJResults;
    }

    public void setFinishedListener(OnFinished event) {
        this.finishedListener = event;
    }

    public void setFailedListener(OnFailed event) {
        this.failedListener = event;
    }

    public ServiceUtil(Context context, MainOperate operate, List<NameValuePair> param, Class entityClass) {
        this.context = context;
        this.srcOrientation = ((Activity) context).getRequestedOrientation();
        this.frmMain = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        this.operate = operate;
        this.param = param;
        this.entityClass = entityClass;
        //this.site = context.getResources().getString(R.string.MAIN_SITE);
        if (BaseApplication.ConfigRecordVariable.getVariable(context) == "") {
            this.site = context.getResources().getString(R.string.MAIN_SITE);
        } else {
            this.site = BaseApplication.ConfigRecordVariable.getVariable(context);
        }
        this.keycode = context.getResources().getString(R.string.KEYCODE);
    }

    public ServiceUtil(Context context, String site, String keycode, Enum operate, Class entityClass) {
        this.context = context;
        this.srcOrientation = ((Activity) context).getRequestedOrientation();
        this.frmMain = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        this.operate = operate;
        this.entityClass = entityClass;
        this.site = site;
        this.keycode = keycode;
    }

    public ServiceUtil(Context context, String site, String keycode, Enum operate, List<NameValuePair> param, Class entityClass) {
        this.context = context;
        this.srcOrientation = ((Activity) context).getRequestedOrientation();
        this.frmMain = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        this.operate = operate;
        this.param = param;
        this.entityClass = entityClass;
        this.site = site;
        this.keycode = keycode;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //Log.wtf("ServiceUtil onPreExecute", "Start...");

        try {
            //lock Rotate
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

            //isEnable View
            ViewUtil.isEnableView(frmMain, false);

            //Show Dialog
            deferDialog = new DeferDialog(context);
            deferDialog.setIndeterminate(true);
            deferDialog.setCancelable(false);
            deferDialog.show();
        } catch(Exception e) {
            //e.printStackTrace();
            Log.wtf("ServiceUtil onPreExecute", e.getMessage());
        }
    }

    @Override
    protected ServiceResponse doInBackground(List<NameValuePair>... params) {

        Log.wtf("Service Response Start", operate.toString());

        //Check Outer Param
        for (List<NameValuePair> inParams : params) {
            this.param.addAll(inParams);
        }

        try {


            //Cell HTTP Post
            HttpParams httpParams = new BasicHttpParams();
            HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(httpParams, HTTP.DEFAULT_CONTENT_CHARSET);
            HttpConnectionParams.setConnectionTimeout(httpParams, context.getResources().getInteger(R.integer.CONN_TIMEOUT));
            HttpConnectionParams.setSoTimeout(httpParams, context.getResources().getInteger(R.integer.CONN_TIMEOUT));

            HttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpPost httpPost = new HttpPost(this.site + "/" + operate.toString());
            if (!operate.toString().equals("VZAgetStatusMasterDataJSON"))
                param.add(new BasicNameValuePair("KEYCODEPara", keycode));

            //Check Internet Connection
            if (((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
                try {
                    httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
                    httpPost.setEntity(new UrlEncodedFormEntity(param, "UTF-8"));

                    ServiceUtil.ServiceResponse serviceResponse = new ServiceResponse();
                    serviceResponse.httpResponse = httpClient.execute(httpPost);

                    //Confirm Status Data
                    if (serviceResponse.httpResponse != null) {
                        if (serviceResponse.httpResponse.getStatusLine().getStatusCode() == 200) {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(serviceResponse.httpResponse.getEntity().getContent()));
                            serviceResponse.OBJResults = new Gson().fromJson(reader, entityClass);
                        }
                    }

                    Log.wtf("Service Response End", operate.toString());

                    return serviceResponse;
                } catch (SocketTimeoutException e) {
                    //Dismiss Dialog
                    if (deferDialog != null && deferDialog.isShowing()) {
                        deferDialog.dismiss();
                        deferDialog = null;
                    }
                    e.printStackTrace();
                    Log.wtf("Service Response SocketTimeoutException", e.getMessage());
                    return null;
                } catch (ConnectTimeoutException e) {
                    //Dismiss Dialog
                    if (deferDialog != null && deferDialog.isShowing()) {
                        deferDialog.dismiss();
                        deferDialog = null;
                    }
                    e.printStackTrace();
                    Log.wtf("Service Response ConnectTimeoutException", e.getMessage());
                    return null;
                } catch (IOException e) {
                    //Dismiss Dialog
                    if (deferDialog != null && deferDialog.isShowing()) {
                        deferDialog.dismiss();
                        deferDialog = null;
                    }
                    e.printStackTrace();
                    Log.wtf("Service Response IOException", e.getMessage());
                    return null;
                } catch (RuntimeException e) {
                    //Dismiss Dialog
                    if (deferDialog != null && deferDialog.isShowing()) {
                        deferDialog.dismiss();
                        deferDialog = null;
                    }
                    e.printStackTrace();
                    Log.wtf("Service Response RuntimeException", e.getMessage());
                    return null;
                }
            } else {
                //Dismiss Dialog
            /*if (deferDialog!=null && deferDialog.isShowing()){
                deferDialog.dismiss();
                deferDialog = null;
            }*/
                return null;
            }
        }catch(Exception ex){
            ex.printStackTrace();
            Log.wtf("Service Response RuntimeException", ex.getMessage());
            return null;
        }
    }

    @Override
    protected void onPostExecute(ServiceResponse httpResponse) {

        //Log.wtf("ServiceUtil onPostExecute", "Start...");

        //Dismiss Dialog
        if (deferDialog!=null && deferDialog.isShowing()){
            deferDialog.dismiss();
            deferDialog = null;
        }

        if (httpResponse == null) {
            ChooseDialog dialog = ChooseDialog.newInstance((Activity) context, context.getResources().getString(R.string.alert_no_internet), false);
            dialog.setOnDismissListener(new ChooseDialog.OnDismiss() {
                @Override
                public void OnDismiss() {
                    if (ServiceUtil.this.failedListener != null) {
                        ServiceUtil.this.failedListener.OnFailed();
                    }
                }
            });
            dialog.show(((Activity) context).getFragmentManager(), "tag");
        } else if (httpResponse.httpResponse.getStatusLine().getStatusCode() != 200) {
            ChooseDialog dialog = ChooseDialog.newInstance((Activity) context, context.getResources().getString(R.string.alert_service_error) +
                    "(" + httpResponse.httpResponse.getStatusLine().getStatusCode() + ")", false);
            dialog.setOnDismissListener(new ChooseDialog.OnDismiss() {
                @Override
                public void OnDismiss() {
                    if (ServiceUtil.this.failedListener != null) {
                        ServiceUtil.this.failedListener.OnFailed();
                    }
                }
            });
            dialog.show(((Activity) context).getFragmentManager(), "tag");
        } else if (((BaseEntity) httpResponse.OBJResults).ERRORMSG.length() > 0) {
            ChooseDialog dialog = ChooseDialog.newInstance((Activity) context, ((BaseEntity) httpResponse.OBJResults).ERRORMSG, false);
            dialog.setOnDismissListener(new ChooseDialog.OnDismiss() {
                @Override
                public void OnDismiss() {
                    if (ServiceUtil.this.failedListener != null) {
                        ServiceUtil.this.failedListener.OnFailed();
                    }
                }
            });
            dialog.show(((Activity) context).getFragmentManager(), "tag");
        } else {
            //return httpResponse data
            if (this.finishedListener != null) {
                this.finishedListener.OnFinished(httpResponse.OBJResults);
            }
        }

        //Delay Close Dialog
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                ((Activity) context).runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                if (!((Activity) context).isFinishing()) {
                                    //unlock Rotate
                                    if (srcOrientation != ActivityInfo.SCREEN_ORIENTATION_LOCKED) {
                                        ((Activity) context).setRequestedOrientation(srcOrientation);
                                    }

                                    //Enable View
                                    if (!operate.equals("UserInfoJSON"))
                                        ViewUtil.isEnableView(frmMain, true);
                                }
                            }
                        }
                );
            }
        }, 1500);
    }

}


